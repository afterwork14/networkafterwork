function populateVenues(data) {
  var $element = $('#venue');
  $element.empty();
  $.each(data, function(key, value) {
    var option = $('<option/>').val(key).text(value);
    $element.append(option)
  });
  $element.removeAttr('disabled');
}