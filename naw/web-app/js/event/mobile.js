function checkin(rsvpId) {
	$.mobile.loading("show", {
		  text: "Checking in..",
		  textVisible: true,
		  theme: "z",
		  html: ""
	});
	$.ajax({
		  url: checkinURL+"?id="+rsvpId,
		  type: 'PUT',
		  data: "id="+rsvpId,
		  success: function(data) {
				$("#rsvp-"+rsvpId).css("color", "black");
				$("#rsvp-"+rsvpId).css("text-decoration", "line-through");
				$("#rsvp-"+rsvpId).append("<p class='ui-li-aside'>"+data.entryTime+"</p>");
				$("#button-checkin-"+rsvpId).attr("href", "undocheckin("+rsvpId+")");
				$("#button-checkin-"+rsvpId).text("Undo");
				$("#"+rsvpId).panel("close");
				$.mobile.loading("hide");
		  }
	});
}
function undocheckin(rsvpId) {
	$.ajax({
		  url: undocheckinURL+"id="+rsvpId,
		  type: 'PUT',
		  data: "id="+rsvpId,
		  success: function(data) {
				$("#rsvp-"+rsvpId).css("text-decoration", "none");
				$("#rsvp-"+rsvpId).append("<p class='ui-li-aside'>"+data.entryTime+"</p>");
				$("#button-checkin-"+rsvpId).attr("href", "checkin("+rsvpId+")");
				$("#button-checkin-"+rsvpId).text("Check-in");
				$("#"+rsvpId).panel("close");
		  }
	});
}