if (typeof jQuery !== 'undefined') {
	(function($) {
		$('#spinner').ajaxStart(function() {
			$(this).fadeIn();
		}).ajaxStop(function() {
			$(this).fadeOut();
		});
	})(jQuery);
}
$(document).ready(function(){
	var str=window.location.pathname.toLowerCase();
	var s = str.split('/');
	$(".nav-list li a").each(function() {
		var atr = this.pathname.toLowerCase();
		var a = atr.split('/');
		if (s[1] == a[2] || s[2] == a[2]) {
			$("li.active").removeClass("active");
			$(this).closest("li").addClass("active");
			return false;
		}
	 });
})
