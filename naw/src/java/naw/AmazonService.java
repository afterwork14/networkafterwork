package naw;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.codehaus.groovy.grails.commons.GrailsApplication;
import org.codehaus.groovy.grails.plugins.support.aware.GrailsApplicationAware;
import org.codehaus.groovy.grails.web.json.JSONArray;
import org.codehaus.groovy.grails.web.json.JSONObject;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;

public class AmazonService implements GrailsApplicationAware {

	private static Logger log = Logger.getLogger(AmazonService.class);

	private GrailsApplication grailsApplication;

	public JSONArray getImages(ListObjectsRequest lor)
			throws IOException {
		Properties pros = grailsApplication.getConfig().toProperties();
		String access_key_id = (String) pros.get("grails.awssdk.accessKey");
		String secret_access_key = (String) pros.get("grails.awssdk.secretKey");
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(access_key_id, secret_access_key);
		AmazonS3 s3client = new AmazonS3Client(awsCreds);
		
		JSONArray images = new JSONArray();
		try {
			System.out.println("Listing objects");
			ObjectListing objectListing;
			do {
				objectListing = s3client.listObjects(lor);
				List<S3ObjectSummary> objectSummaries = objectListing.getObjectSummaries();
				if(objectSummaries != null && objectSummaries.size() != 0) {
					for (S3ObjectSummary objectSummary : objectSummaries.subList(1, objectSummaries.size())) {
						java.util.Date expiration = new java.util.Date();
						long msec = expiration.getTime();
						msec += 1000 * 60 * 60; // 1 hour.
						expiration.setTime(msec);
						GeneratePresignedUrlRequest generatePresignedUrlRequest = 
						              new GeneratePresignedUrlRequest(lor.getBucketName(), objectSummary.getKey());
						generatePresignedUrlRequest.setMethod(HttpMethod.GET); // Default.
						generatePresignedUrlRequest.setExpiration(expiration);
						             
						URL s = s3client.generatePresignedUrl(generatePresignedUrlRequest); 
						JSONObject image = new JSONObject();
						image.accumulate("url", s);
						images.add(image);
					}
				}
			} while (objectListing.isTruncated());
			log.debug("Object Listings:    " + objectListing);
		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, "
					+ "which means your request made it "
					+ "to Amazon S3, but was rejected with an error response "
					+ "for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, "
					+ "which means the client encountered "
					+ "an internal error while trying to communicate"
					+ " with S3, "
					+ "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
		return images;
	}

	public GrailsApplication getGrailsApplication() {
		return grailsApplication;
	}

	public void setGrailsApplication(GrailsApplication grailsApplication) {
		this.grailsApplication = grailsApplication;
	}
}
