package naw;

import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.codehaus.groovy.grails.commons.GrailsApplication;
import org.codehaus.groovy.grails.plugins.support.aware.GrailsApplicationAware;

import paypal.payflow.PayflowAPI;
import paypal.payflow.PayflowConstants;
import paypal.payflow.SDKProperties;

public class PaypalService implements GrailsApplicationAware{
	
	private GrailsApplication grailsApplication;
	
	private static Logger log = Logger.getLogger(PaypalService.class); 

	public String submitTransaction(Map<String, String> payment){
		
		Properties pros = grailsApplication.getConfig().toProperties();
		
		// Payflow Pro Host Name. This is the host name for the PayPal Payment Gateway.
		// For testing: 	 pilot-payflowpro.paypal.com
		// For production:   payflowpro.paypal.com
		// DO NOT use payflow.verisign.com or test-payflow.verisign.com!
		SDKProperties.setHostAddress((String) pros.get("grails.paypal.host"));
		SDKProperties.setHostPort(443);
		SDKProperties.setTimeOut(45);

		// Uncomment the lines below and set the proxy address and port, if a proxy has to be used.
		//SDKProperties.setProxyAddress("");
		//SDKProperties.setProxyPort(0);
		//SDKProperties.setProxyLogin("");
		//SDKProperties.setProxyPassword("");

		// Logging is by default off. To turn logging on uncomment the following lines:
		SDKProperties.setLogFileName("payflow_java.log");
		SDKProperties.setLoggingLevel(PayflowConstants.SEVERITY_DEBUG);
		SDKProperties.setMaxLogFileSize(1000000);
		// Log Stack Traces (boolean)
		SDKProperties.setStackTraceOn(true);

		
		String user = (String) pros.get("grails.paypal.user");
		String password = (String) pros.get("grails.paypal.password");
		String vendor = (String) pros.get("grails.paypal.vendor");
		String partner = (String) pros.get("grails.paypal.partner");
		
		String request = 
				  "USER="+ user +"&"
				+ "VENDOR="+ vendor +"&"
				+ "PARTNER="+ partner +"&"
				+ "PWD="+ password +"&"
				+ "TRXTYPE=S&"
				+ "ACCT="+ payment.get("number") +"&"
				+ "EXPDATE="+ payment.get("expiration") +"&"
				+ "TENDER=C&"
				+ "INVNUM="+ payment.get("orderId") +"&"
				+ "PONUM="+ payment.get("postboxnum") +"&"
				+ "STREET="+ payment.get("street") +"&"
				+ "ZIP="+ payment.get("zip") +"&"
				+ "AMT="+ payment.get("amount") +"&"
				+ "CVV2="+ payment.get("cvv") +"&"
				+ "VERBOSITY=HIGH";

		PayflowAPI pa = new PayflowAPI();
		// RequestId is a unique string that is required for each & every transaction.
		// The merchant can use her/his own algorithm to generate this unique request id or
		// use the SDK provided API to generate this as shown below (PayflowAPI.generateRequestId).
		String requestId = pa.generateRequestId();

		String response = pa.submitTransaction(request, requestId);

		log.debug("Transaction Request :\n-------------------- \n" + request);
		log.debug("Transaction Response :\n-------------------- \n" + response);

		// Following lines of code are optional.
		// Begin optional code for displaying SDK errors ...
		// It is used to read any errors that might have occured in the SDK.
		// Get the transaction errors.

		String transErrors = pa.getTransactionContext().toString();
		if (transErrors != null && transErrors.length() > 0) {
			log.debug("Transaction Errors from SDK = \n" + transErrors);
			return transErrors;
		}
		
		return response;
	}

	public GrailsApplication getGrailsApplication() {
		return grailsApplication;
	}

	public void setGrailsApplication(GrailsApplication grailsApplication) {
		this.grailsApplication = grailsApplication;
	}
}
