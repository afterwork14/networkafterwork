package naw

class NawConstants {
	final static ENABLED = "enabled"
	final static DISABLED = "disabled"
	final static AUTO_CHECKIN = "AUTO"
	final static MANUAL_CHECKIN = "MANUAL"
	final static PAYPAL_PRO = "PAYPAL PRO"
	final static PAYPAL_EXPRESS = "PAYPAL EXPRESS"
	final static CASH = "CASH"
	final static ORDER_WEBSITE = "WEBSITE"
	final static ORDER_MEETUP = "MEETUP"
	final static ORDER_EVENTBRITE = "EVENTBRITE"
	final static ORDER_AT_DOOR = "AT DOOR"
	final static MEETUP_ORDER_PAID_STATUS = "PAID"
	final static ORDER_FULLY_PAID = "PAID"
	final static ORDER_NOT_PAID = "UNPAID"
	final static ORDER_PARTIALLY_PAID = "PARTIAL"
}
