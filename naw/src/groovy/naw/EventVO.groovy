package naw

class EventVO {
	int daysToGo
	boolean upcoming = false
	boolean past = false
	boolean today = false
	int totalOrders
	double totalAmount = 0.00
	double totalPaidAmount = 0.00
	double totalUnpaidAmount = 0.00
	int totalWebsiteOrders
	double totalWebsiteAmount = 0.00
	double totalWebsitePaidAmount = 0.00
	double totalWebsiteUnpaidAmount = 0.00
	int totalMeetupOrders
	double totalMeetupAmount = 0.00
	double totalMeetupPaidAmount = 0.00
	double totalMeetupUnpaidAmount = 0.00
	int totalEventbriteOrders
	double totalEventbriteAmount = 0.00
	double totalEventbritePaidAmount = 0.00
	double totalEventbriteUnpaidAmount = 0.00
	int totalAtDoorOrders
	double totalAtDoorAmount = 0.00
	double totalAtDoorPaidAmount = 0.00
	double totalAtDoorUnpaidAmount = 0.00
	int totalRsvps = 0;
	int newCustomers = 0
	int attendees = 0
	def imageURLs
}
