class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		group "/ui", {
			"/industry/list"(controller:"UI", action:"getIndustries")
			"/city/list"(controller:"UI", action:"getCities")
			"/user/exists/$email"(controller:"UI", action:"checkUserExists")
			"/user/save"(controller:"UI", action:"saveUser")
			"/user/update"(controller:"UI", action:"updateUser")
			"/user/lead/source"(controller:"UI", action:"getUserLeadSources")
			"/user/password/forgot"(controller:"UI", action:"forgotPassword")
			"/user/profile/$id"(controller:"UI", action:"getUserProfile")
			"/user/orders/$id"(controller:"UI", action:"getUserOrders")
			"/event/upcoming/geolocation/$latitude/$longitude"(controller:"UI", action:"upcomingEventsByGeoLocation")
			"/event/future/geolocation/$latitude/$longitude"(controller:"UI", action:"futureEventsByGeoLocation")
			"/event/past/geolocation/$latitude/$longitude"(controller:"UI", action:"pastEventsByGeoLocation")
			"/event/upcoming/city/$id"(controller:"UI", action:"upcomingEventsByCity")
			"/event/future/city/$id"(controller:"UI", action:"futureEventsByCity")
			"/event/past/city/$id"(controller:"UI", action:"pastEventsByCity")
			"/venue/images/$id"(controller:"UI", action:"getVenueImages")
			"/city/images/$id"(controller:"UI", action:"getCityImages")
			"/country/images/$id"(controller:"UI", action:"getCountryImages")
		}
		"/api/contactus"(resource: "mailboxRest")
		"/api/forgotpwd"(resource:"forgotPassword")
		
		"/"(controller: "dashboard")
		"500"(view:'/error')
	}
}
