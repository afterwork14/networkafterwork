// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = naw // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
	pdf:           'application/pdf',
	rtf: 		   'application/rtf',
	excel: 		   'application/vnd.ms-excel',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml']
]

naw.mime.file.extensions = [
	pdf:           'pdf',
	excel: 		   'xls'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*', '/assets/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

plugin.platformCore.events.catchFlushExceptions=true

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    debug  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate',
		   'grails.app.services.naw',
		   'grails.app.services.com.grailsrocks.emailconfirmation',
		   'com.amazonaws',
		   'naw',
		   'com.the6hours', 
		   'grails.app.taglib.com.the6hours',
		   'com.odobo',
		   'grails.app.controllers.com.odobo',
		   'grails.app.services.com.odobo',
		   'org.pac4j'
		   
	trace  'org.hibernate.type.descriptor.sql.BasicBinder'
		   
}

// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.userLookup.userDomainClassName = 'naw.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'naw.UserRole'
grails.plugin.springsecurity.authority.className = 'naw.Role'
grails.plugin.springsecurity.successHandler.defaultTargetUrl = '/dashboard'
grails.plugin.springsecurity.successHandler.ajaxSuccessUrl = '/'
grails.plugin.springsecurity.failureHandler.ajaxAuthFailUrl = '/'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	'/':                              ['permitAll'],
	'/index':                         ['permitAll'],
	'/index.gsp':                     ['permitAll'],
	'/**/js/**':                      ['permitAll'],
	'/**/css/**':                     ['permitAll'],
	'/**/images/**':                  ['permitAll'],
	'/**/assets/**':                  ['permitAll'],
	'/**/favicon.ico':                ['permitAll']
]
//grails.plugin.springsecurity.facebook.domain.classname = 'naw.FacebookUser'
//grails.plugin.springsecurity.facebook.domain.appUserConnectionPropertyName = 'user'
//grails.plugin.springsecurity.facebook.filter.processUrl = 'j_spring_security_facebook_json'
//grails.plugin.springsecurity.facebook.filter.type =	'json'

grails.plugin.springsecurity.rest.login.endpointUrl	= "/ui/login"
grails.plugin.springsecurity.rest.logout.endpointUrl = "/ui/logout"
grails.plugin.springsecurity.rest.token.generation.useUUID = true
grails.plugin.springsecurity.rest.token.storage.useGorm	= true
grails.plugin.springsecurity.rest.token.storage.gorm.tokenDomainClassName = "naw.AuthenticationToken"
grails.plugin.springsecurity.filterChain.chainMap = [
	'/ui/**': 'JOINED_FILTERS,-exceptionTranslationFilter,-authenticationProcessingFilter,-securityContextPersistenceFilter',  // Stateless chain
	'/**': 'JOINED_FILTERS,-restTokenValidationFilter,-restExceptionTranslationFilter'                                          // Traditional chain
]

grails {
	plugin {
		springsecurity {
			rest {
				oauth {
					frontendCallbackUrl = { String tokenValue -> "http://localhost:8080/naw/dashboard?code=${tokenValue}" }
					facebook {
						client = org.pac4j.oauth.client.FacebookClient
						key = '735981529745433'
						secret = '8562d5785a119a59913bc573f229cafe'
						scope = 'email,user_location'
						fields = 'id,name,first_name,middle_name,last_name,username'
						defaultRoles = ['ROLE_USER']
					}
				}
			}
		}
	}
}

//CORS
cors.url.pattern = '/ui/*'

grails {
	mail {
		host = "relay.edsmtp.net"
		port = 465
		username = System.getProperty("EMAIL_DIRECT_SMTP_USER")
		password = System.getProperty("EMAIL_DIRECT_SMTP_PSWD")
		props = ["mail.smtp.auth":"true",
				 "mail.smtp.socketFactory.port":"465",
				 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
				 "mail.smtp.socketFactory.fallback":"false"]
	}
}

environments {
    development {
        grails.logging.jul.usebridge = true
		grails.plugin.springsecurity.rejectIfNoRule = false
		grails.plugin.springsecurity.fii.rejectPublicInvocations = false
		rest.providers.emaildirect.key="162bacff-532e-4f5d-ba2f-6288357b4ac0"
		grails.awssdk.accessKey = "AKIAIVP467NYNGNE6VZA"
		grails.awssdk.secretKey = "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"
		grails.aws.s3.bucket="nawdev"
		grails.paypal.host = "pilot-payflowpro.paypal.com"
		grails.paypal.user = "website"
		grails.paypal.password = "Afterwork78"
		grails.paypal.vendor = "NetworkAfterWork"
		grails.paypal.partner = "PayPal"
		grails {
			mail {
				host = "relay.edsmtp.net"
				port = 465
				username = "nawuser@NWAWS"
				password = "nawpwd"
				props = ["mail.smtp.auth":"true",
						 "mail.smtp.socketFactory.port":"465",
						 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
						 "mail.smtp.socketFactory.fallback":"false"]
			 }
		}
//		grails.plugin.springsecurity.facebook.secret = '8562d5785a119a59913bc573f229cafe'
//		grails.plugin.springsecurity.facebook.appId =	'735981529745433'
//		grails.plugin.springsecurity.facebook.appToken =	'735981529745433|n_MBUYwaPTqcjnjfuPlZu6u-5bc'
		grails.serverURL = "http://localhost:8080/naw"
		grails.google.geolocation.key= 'AIzaSyCzccr5hzJaQTAZANTH-zRX3_YtNTJ1FzI'
    }
	amzdevelopment {
		grails.logging.jul.usebridge = true
		grails.plugin.springsecurity.rejectIfNoRule = false
		grails.plugin.springsecurity.fii.rejectPublicInvocations = false
		rest.providers.emaildirect.key=System.getProperty("EMAILDIRECT_KEY");
		grails.awssdk.accessKey = System.getProperty("AWS_ACCESS_KEY_ID");
		grails.awssdk.secretKey = System.getProperty("AWS_SECRET_KEY");
		grails.aws.s3.bucket = System.getProperty("S3_BUCKET");
		grails.paypal.host = System.getProperty("PAYPAL_HOST");
		grails.paypal.user = System.getProperty("PAYPAL_USER");
		grails.paypal.password = System.getProperty("PAYPAL_PASSWORD");
		grails.paypal.vendor = System.getProperty("PAYPAL_VENDOR");
		grails.paypal.partner = System.getProperty("PAYPAL_PARTNER");
//		grails.plugin.springsecurity.facebook.secret = System.getProperty("FB_SECRET");
//		grails.plugin.springsecurity.facebook.appId =	System.getProperty("FB_APPID");
		grails.serverURL = System.getProperty("SERVER_URL")
		grails.google.geolocation.key= System.getProperty("GOOGLE_GEO_KEY")
	}
	test {
		grails.logging.jul.usebridge = true
		grails.plugin.springsecurity.rejectIfNoRule = true
		grails.plugin.springsecurity.fii.rejectPublicInvocations = false
		rest.providers.emaildirect.key=System.getProperty("EMAILDIRECT_KEY");
		grails.awssdk.accessKey = System.getProperty("AWS_ACCESS_KEY_ID");
		grails.awssdk.secretKey = System.getProperty("AWS_SECRET_KEY");
		grails.aws.s3.bucket = System.getProperty("S3_BUCKET");
		grails.paypal.host = System.getProperty("PAYPAL_HOST");
		grails.paypal.user = System.getProperty("PAYPAL_USER");
		grails.paypal.password = System.getProperty("PAYPAL_PASSWORD");
		grails.paypal.vendor = System.getProperty("PAYPAL_VENDOR");
		grails.paypal.partner = System.getProperty("PAYPAL_PARTNER");
//		grails.plugin.springsecurity.facebook.secret = System.getProperty("FB_SECRET");
//		grails.plugin.springsecurity.facebook.appId =	System.getProperty("FB_APPID");
		grails.serverURL = System.getProperty("SERVER_URL")
		grails.google.geolocation.key= System.getProperty("GOOGLE_GEO_KEY")
	}
    production {
        grails.logging.jul.usebridge = false
		grails.plugin.springsecurity.rejectIfNoRule = true
		grails.plugin.springsecurity.fii.rejectPublicInvocations = false
		rest.providers.emaildirect.key=System.getProperty("EMAILDIRECT_KEY");
		grails.awssdk.accessKey = System.getProperty("AWS_ACCESS_KEY_ID");
		grails.awssdk.secretKey = System.getProperty("AWS_SECRET_KEY");
		grails.aws.s3.bucket = System.getProperty("S3_BUCKET");
		grails.paypal.host = System.getProperty("PAYPAL_HOST");
		grails.paypal.user = System.getProperty("PAYPAL_USER");
		grails.paypal.password = System.getProperty("PAYPAL_PASSWORD");
		grails.paypal.vendor = System.getProperty("PAYPAL_VENDOR");
		grails.paypal.partner = System.getProperty("PAYPAL_PARTNER");
//		grails.plugin.springsecurity.facebook.secret = System.getProperty("FB_SECRET");
//		grails.plugin.springsecurity.facebook.appId =	System.getProperty("FB_APPID");
		grails.serverURL = System.getProperty("SERVER_URL")
		grails.google.geolocation.key= System.getProperty("GOOGLE_GEO_KEY")
    }
}
 
// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}