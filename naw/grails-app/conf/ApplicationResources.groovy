modules = {
    application {
        resource url:'js/application.js'
    }
	core{
		dependsOn 'jquery'
		resource url:'/assets/bootstrap/js/bootstrap.js'
		resource url:'/assets/jquery-slimscroll/jquery.slimscroll.js'
		resource url:'/assets/jquery-cookie/jquery.cookie.js'
		resource url:'/assets/bootstrap/css/bootstrap.css'
		resource url:'/assets/font-awesome/css/font-awesome.css'
	}
	flaty{
		dependsOn 'jquery, core'
		resource url:'/js/flaty.js'
		resource url:'/js/flaty-demo-codes.js'
		resource url:'/css/flaty.css'
		resource url:'/css/flaty-responsive.css'
	}
	fileupload{
		dependsOn 'jquery, core, flaty'
		resource url:'/assets/bootstrap-fileupload/bootstrap-fileupload.css'
		resource url:'/assets/bootstrap-fileupload/bootstrap-fileupload.js'
	}
	charts{
		dependsOn 'jquery, core, flaty'
		resource url:'/assets/flot/jquery.flot.js'
		resource url:'/assets/flot/jquery.flot.resize.js'
		resource url:'/assets/flot/jquery.flot.pie.js'
		resource url:'/assets/flot/jquery.flot.stack.js'
		resource url:'/assets/flot/jquery.flot.crosshair.js'
		resource url:'/assets/sparkline/jquery.sparkline.min.js'
	}
	maps{
		dependsOn 'jquery, core, flaty'
		resource url:'/assets/flot/jquery.flot.js', linkOverride:'http://maps.google.com/maps/api/js?sensor=true'
		resource url:'/assets/gmaps/gmaps.js'
	}
	userprofile{
		dependsOn 'jquery, core, flaty, fileupload'
		resource url:'/assets/chosen-bootstrap/chosen.css'
		resource url:'/assets/bootstrap-datepicker/css/datepicker.css'
		resource url:'/assets/chosen-bootstrap/chosen.jquery.js'
		resource url:'/assets/bootstrap-datepicker/js/bootstrap-datepicker.js'
	}
	event{
		dependsOn 'jquery, core, flaty'
		resource url:'/assets/bootstrap-datepicker/css/datepicker.css'
		resource url:'/assets/bootstrap-timepicker/compiled/timepicker.css'
		resource url:'/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css'
		resource url:'/assets/prettyPhoto/css/prettyPhoto.css'
		resource url:'/assets/chosen-bootstrap/chosen.css'
		resource url:'/assets/bootstrap-timepicker/js/bootstrap-timepicker.js'
		resource url:'/assets/bootstrap-datepicker/js/bootstrap-datepicker.js'
		resource url:'/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js'
		resource url:'/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js'
		resource url:'/js/event/new.js'
		resource url:'/assets/prettyPhoto/js/jquery.prettyPhoto.js'
		resource url:'/assets/chosen-bootstrap/chosen.jquery.js'
	}
	venue{
		dependsOn 'jquery, core, flaty, fileupload'
	}
	prod{
		resource url:'/js/prod/tracking.js'
	}
	test{
		resource url:'/js/test/tracking.js'
	}
	mobile{
		resource url:'/js/jquery.mobile-1.4.2.js'
		resource url:'/js/event/mobile.js'
		resource url:'/css/jquery.mobile-1.4.2.css'
		resource url:'/css/jquery.mobile.external-png-1.4.2.css'
		resource url:'/css/jquery.mobile.icons-1.4.2.css'
		resource url:'/css/jquery.mobile.inline-png-1.4.2.css'
		resource url:'/css/jquery.mobile.inline-svg-1.4.2.css'
		resource url:'/css/jquery.mobile.structure-1.4.2.css'
		resource url:'/css/jquery.mobile.theme-1.4.2.css'
	}
}