// Place your Spring DSL code here
import naw.SpringSecurityServiceHolder
import naw.PaypalService
import naw.AmazonService

beans = {
	System.setProperty('org.hibernate.envers.audit_table_suffix', '_audit')
	springSecurityServiceHolder(SpringSecurityServiceHolder, ref('springSecurityService'))
	rest(grails.plugins.rest.client.RestBuilder)
	paypalService(PaypalService)
	amazonService(AmazonService)
	oauthUserDetailsService(naw.CustomOauthUserDetailsService) {
		userDetailsService = ref("userDetailsService")
		facebookService = ref("facebookService")
		userService = ref("userService")
	}
}
