import com.amazonaws.util.json.JSONObject;
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.converters.JSON
import groovy.json.JsonBuilder
import naw.City
import naw.Country
import naw.CreditCard
import naw.Event
import naw.Industry
import naw.NawConstants;
import naw.Order
import naw.PriceModifier
import naw.Role
import naw.Rsvp
import naw.Source
import naw.State
import naw.User
import naw.UserRole
import naw.Venue
import naw.Reference
import naw.NawConstants
import groovy.time.TimeCategory

class BootStrap {
	def grailsApplication
	def emailDirectService
	def userService
	def orderService
	def venueService
	def cityService
	def geometryFactory = new GeometryFactory()
	
	def init = { servletContext ->

		environments {
			development {
				populateLocalReference()
				populateCountries()
				populateStates()
				populateCities()
				populateVenues()
				populateIndustries()
				populateRoles()
				populatePriceModifiers()
				populateSources()
				populateUsers()
				populateEvents()
				populateWebsiteOrders()
				populateMeetupOrders()
				populateDoorOrders()
			}
			amzdevelopment {
				populateTestReference()
				populateCountries()
				populateStates()
				populateCities()
				populateVenues()
				populateIndustries()
				populateRoles()
				populatePriceModifiers()
				populateSources()
				populateUsers()
				populateEvents()
				populateWebsiteOrders()
				populateMeetupOrders()
				populateDoorOrders()
			}
			test {
				populateTestReference()
				populateCountries()
				populateStates()
				populateCities()
				populateVenues()
				populateIndustries()
				populateRoles()
				populatePriceModifiers()
				populateUsers()
			}
		}
	}

	def destroy = {
	}


	void populateRoles(){
		def filePath = "bootstrap/roles.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Role.withTransaction {
			for (role in json) {
				def c = new Role(
						authority: role ["name"],
						description: role["description"]
						).save(flush: true, failOnError: true);
			}
		}
	}

	void populateUsers(){
		def filePath = "bootstrap/users.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		User.withTransaction {
			for (user in json) {
				def c = new User(
						username: user ["username"],
						password: user ["password"],
						firstname: user ["firstname"],
						lastname: user ["lastname"],
						email: user ["email"],
						phone: user ["phone"],
						gender: user ["gender"],
						company: user["company"],
						source: Source.get(user["sourceId"]),
						enabled: true
						).addToCities(City.get(1))
				def imagePath = "bootstrap"+user["picture"];
				def picture = grailsApplication.getParentContext().getResource("classpath:$imagePath").getInputStream().getBytes();
				c.picture = picture;
				c.save(flush: true, failOnError: true);
				UserRole.create(c, Role.findByAuthority(user["role"])).save(flush: true, failOnError: true);
			}
		}
	}

	void populateCountries(){
		def filePath = "bootstrap/countries.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Country.withTransaction {
			for (country in json) {
				def c = new Country(
						name: country ["name"],
						abbreviation: country ["code"],
						active: country ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}

	void populateStates(){
		def filePath = "bootstrap/states.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		State.withTransaction {
			for (state in json) {
				def c = new State(
						name: state ["name"],
						abbreviation: state ["abbreviation"],
						country: Country.findByAbbreviation(state ["country"]),
						active: state ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}

	void populateEvents(){
		def filePath = "bootstrap/events.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Event.withTransaction {
			def today = new Date()
			for (event in json) {
				def _startTime = today + event["dateAdjust"]
				def _endTime
				use(TimeCategory) {
					_endTime = _startTime + 3.hours
				} 
				def c = new Event(
						name: event ["name"],
						capacity: event ["capacity"],
						venue: Venue.findByName(event["venue"]),
						price: event ["price"],
						meetupId: event["meetupId"],
						manager: User.findByUsername(event["manager"]),
						startTime: Date.parse("yyyy-MM-dd hh:mm:ss", _startTime.format("yyyy-MM-dd HH:mm:ss")),
						endTime: Date.parse("yyyy-MM-dd hh:mm:ss", _endTime.format("yyyy-MM-dd HH:mm:ss")),
						active: true
						).save(flush: true, failOnError: true);
			}
		}
	}

	void populateVenues(){
		def filePath = "bootstrap/venues.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Venue.withTransaction {
			for (venue in json) {
				def params = [:]
				params["name"] = venue ["name"]
				params["address1"] = venue ["address1"]
				params["address2"] = venue ["address2"]
				params["city"] = City.findByName(venue["city"]).id
				params["postalcode"] = venue ["postalcode"]
				params["state"] = State.findByAbbreviation(venue["state"]).id
				params["country"] = Country.findByAbbreviation(venue ["country"]).id
				venueService.save(params);
			}
		}
	}

	void populateCities(){
		def filePath = "bootstrap/cities.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		City.withTransaction {
			for (city in json) {
				cityService.save(city)
//				def params = [:]
//				params["name"] = city ["name"]
//				params["meetupGroupUrlName"] = city ["meetupGroupUrlName"]
//				params["meetupGroupId"] = city ["meetupGroupId"]
//				params["city"] = City.findByName(venue["city"]).id
//				params["postalcode"] = venue ["postalcode"]
//				params["state"] = State.findByAbbreviation(venue["state"]).id
//				params["country"] = Country.findByAbbreviation(venue ["country"]).id
//				def imagePath = "bootstrap"+venue["picture"];
//				params["picture"] = grailsApplication.getParentContext().getResource("classpath:$imagePath")
//				venueService.save(params);
//				def c = new City(
//						name: city ["name"],
//						meetupGroupUrlName: city ["meetupGroupUrlName"],
//						meetupGroupId: city ["meetupGroupId"],
//						meetupKey: city ["meetupKey"],
//						active: city ["active"],
//						state: State.findByAbbreviation(city ["state"])
//						)
//				if(c.name.equals("Chicago") || c.name.equals("Boston") || c.name.equals("San Francisco")){
//					try{
//						int listId = emailDirectService.addList(c.name)
//						c.setEmailDirectListId(listId);
//					}catch(Exception e){
//						e.printStackTrace()
//					}
//				}
//				c.save(flush: true, failOnError: true);
			}
		}
	}

	void populateIndustries(){
		def filePath = "bootstrap/industries.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Industry.withTransaction {
			for (industry in json) {
				def c = new Industry(
						name: industry ["name"],
						active: industry ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}
	
	void populateSources(){
		def filePath = "bootstrap/sources.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Source.withTransaction {
			for (source in json) {
				def c = new Source(
						name: source ["name"],
						description: source ["description"],
						active: source ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}
	
	void populateLocalReference(){
		def filePath = "bootstrap/local/reference.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Reference.withTransaction {
			for (reference in json) {
				def c = new Reference(
						name: reference ["name"],
						value: reference ["value"],
						active: reference ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}
	
	void populateTestReference(){
		def filePath = "bootstrap/test/reference.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Reference.withTransaction {
			for (reference in json) {
				def c = new Reference(
						name: reference ["name"],
						value: reference ["value"],
						active: reference ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}
	
	void populateProdReference(){
		def filePath = "bootstrap/prod/reference.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Reference.withTransaction {
			for (reference in json) {
				def c = new Reference(
						name: reference ["name"],
						value: reference ["value"],
						active: reference ["active"]
						).save(flush: true, failOnError: true);
			}
		}
	}
	
	void populatePriceModifiers(){
		def filePath = "bootstrap/pricemodifiers.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		PriceModifier.withTransaction {
			for (pricemodifier in json) {
				def c = new PriceModifier(
						value: pricemodifier ["value"],
						method: pricemodifier ["method"],
						type: pricemodifier ["type"],
						level: pricemodifier ["level"],
						description: pricemodifier ["description"]
						).save(flush: true, failOnError: true);
			}
		}
	}


	void populateWebsiteOrders(){
		def filePath = "bootstrap/orders.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Order.withTransaction {
			for(order in json){
				if(order["source"].equals(NawConstants.ORDER_WEBSITE)){
					User user=User.findByUsername(order["username"])
					Event event=Event.findByName(order["eventname"])
					def params = [:]
					params["userId"] = user.id
					params["eventId"] = event.id
					params["orderTotal"] = order["orderTotal"]
					params["paidAmount"] = order["paidAmount"]
					params["source"] = order["source"]
					
					def rsvps = []
					for(rsvp in order["rsvps"]){
						def r = [:]
						r["firstname"] = rsvp["firstname"]
						r["lastname"] = rsvp["lastname"]
						r["email"] = rsvp["email"]
						r["entryFee"] = rsvp["entryFee"]
						r["paidFee"] = rsvp["paidFee"]
						rsvps.add(r)
					}
					params["rsvps"] = new JsonBuilder(rsvps).toString()
					def newOrder = orderService.generateWebsiteOrder(params)
					if(order.hasProperty("creditcard")){
						def creditcard = new CreditCard()
						creditcard.setNumber(order["creditcard"][0]["number"])
						creditcard.setCvv(order["creditcard"][0]["cvv"])
						creditcard.setExpiration(order["creditcard"][0]["expiration"])
						creditcard.setStreet(order["creditcard"][0]["street"])
						creditcard.setZip(order["creditcard"][0]["zip"])
						creditcard.setPostboxnum(order["creditcard"][0]["postboxnum"])
						orderService.savePaypalProOrder(newOrder, creditcard)
					}
					else{
						newOrder.save(flush: true,failOnError: true)
					}
				}
			}
		}
	}
	
	void populateMeetupOrders(){
		def filePath = "bootstrap/orders.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Order.withTransaction {
			for(order in json){
				if(order["source"].equals(NawConstants.ORDER_MEETUP)){
					Event event=Event.findByName(order["eventname"])
					def params = [:]
					params["name"] = order["name"]
					params["eventId"] = event.id
					params["meetup_rsvp_id"] = order["meetup_rsvp_id"]
					params["pay_status"] = order["pay_status"]
					params["guests"] = order["guests"]
					orderService.saveMeetupOrder(params)
				}
			}
		}
	}
	
	void populateDoorOrders(){
		def filePath = "bootstrap/orders.json"
		def text = grailsApplication.getParentContext().getResource("classpath:$filePath").getInputStream().getText()
		def json = JSON.parse(text)
		Order.withTransaction {
			for(order in json){
				if(order["source"].equals(NawConstants.ORDER_AT_DOOR)){
					def params = [:]
					Event event=Event.findByName(order["eventname"])
					params["eventId"] = event.id
					User user=User.findByUsername(order["username"])
					params["userId"] = user.id
					if(order["email"]){
						params["email"] = order["email"]
					}
					if(order["phone"]){
						params["phone"] = order["phone"]
					}
					params["orderTotal"] = order["orderTotal"]
					params["paidAmount"] = order["paidAmount"]
					orderService.saveDoorOrder(params)
				}
			}
		}
	}
}
