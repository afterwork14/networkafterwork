dataSource {
    pooled = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
//            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
//            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
//			loggingSql = true
//			dialect = org.hibernatespatial.geodb.GeoDBDialect
			
			dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
			driverClassName = "com.mysql.jdbc.Driver"
			url = "jdbc:mysql://localhost:3306/nawDB"
			dialect = org.hibernatespatial.mysql.MySQLSpatialDialect
			username = "root"
			password = "root"
			properties {
				maxActive = 50
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 60000
				timeBetweenEvictionRunsMillis = 60000
				maxWait = 10000
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "select now()"
			}
			
        }
    }
	amzdevelopment {
		dataSource {
			dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
			driverClassName = "com.mysql.jdbc.Driver"
			url = System.getProperty("JDBC_CONNECTION_STRING");
			dialect = org.hibernatespatial.mysql.MySQLSpatialDialect
			username = System.getProperty("DB_USER");
			password = System.getProperty("DB_PSWD");
			properties {
				maxActive = 50
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 60000
				timeBetweenEvictionRunsMillis = 60000
				maxWait = 10000
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "select now()"
			}
		}
	}
    test {
       dataSource {
			dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			driverClassName = "com.mysql.jdbc.Driver"
			url = System.getProperty("JDBC_CONNECTION_STRING");
			dialect = org.hibernatespatial.mysql.MySQLSpatialInnoDBDialect
			username = System.getProperty("DB_USER");
			password = System.getProperty("DB_PSWD");
			properties {
				maxActive = 50
				maxIdle = 25
				minIdle = 5
				initialSize = 5
				minEvictableIdleTimeMillis = 60000
				timeBetweenEvictionRunsMillis = 60000
				maxWait = 10000
				numTestsPerEvictionRun = 3
				testOnBorrow = true
				testWhileIdle = true
				testOnReturn = true
				validationQuery = "select now()"
			}
		}
    }
	prod {
		dataSource {
			 dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
			 driverClassName = "com.mysql.jdbc.Driver"
			 url = System.getProperty("JDBC_CONNECTION_STRING");
			 dialect = org.hibernate.dialect.MySQL5InnoDBDialect
			 username = System.getProperty("DB_USER");
			 password = System.getProperty("DB_PSWD");
			 properties {
				 maxActive = 50
				 maxIdle = 25
				 minIdle = 5
				 initialSize = 5
				 minEvictableIdleTimeMillis = 60000
				 timeBetweenEvictionRunsMillis = 60000
				 maxWait = 10000
				 numTestsPerEvictionRun = 3
				 testOnBorrow = true
				 testWhileIdle = true
				 testOnReturn = true
				 validationQuery = "select now()"
			 }
		 }
	 }
    
}
