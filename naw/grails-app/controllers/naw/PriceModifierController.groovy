package naw

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional
class PriceModifierController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [priceModifierInstanceList: PriceModifier.list(params), priceModifierInstanceTotal: PriceModifier.count()]
    }

    def create() {
        [priceModifierInstance: new PriceModifier(params)]
    }

    def save() {
        def priceModifierInstance = new PriceModifier(params)
        if (!priceModifierInstance.save(flush: true)) {
            render(view: "create", model: [priceModifierInstance: priceModifierInstance])
            return
        }
        flash.message = message(code: 'default.created.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), priceModifierInstance.id])
        redirect(action: "show", id: priceModifierInstance.id)
    }

    def show(Long id) {
        def priceModifierInstance = PriceModifier.get(id)
        if (!priceModifierInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "list")
            return
        }

        [priceModifierInstance: priceModifierInstance]
    }

    def edit(Long id) {
        def priceModifierInstance = PriceModifier.get(id)
        if (!priceModifierInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "list")
            return
        }

        [priceModifierInstance: priceModifierInstance]
    }

    def update(Long id, Long version) {
        def priceModifierInstance = PriceModifier.get(id)
        if (!priceModifierInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "list")
            return
        }
		
        if (version != null) {
            if (priceModifierInstance.version > version) {
                priceModifierInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'PriceModifier.label', default: 'PriceModifier')] as Object[],
                          "Another user has updated this PriceModifier while you were editing")
                render(view: "edit", model: [priceModifierInstance: priceModifierInstance])
                return
            }
        }

        priceModifierInstance.properties = params

        if (!priceModifierInstance.save(flush: true)) {
            render(view: "edit", model: [priceModifierInstance: priceModifierInstance])
            return
        }
        flash.message = message(code: 'default.updated.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), priceModifierInstance.id])
        redirect(action: "show", id: priceModifierInstance.id)
    }

    def delete(Long id) {
        def priceModifierInstance = PriceModifier.get(id)
        if (!priceModifierInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "list")
            return
        }

        try {
            priceModifierInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'PriceModifier.label', default: 'PriceModifier'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def history(Long id) {
		[priceModifierInstanceHisotryList: PriceModifier.findAllRevisionsById(id), id: id]
	}
}
