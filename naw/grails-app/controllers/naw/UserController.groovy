package naw

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional
import grails.gorm.DetachedCriteria
import grails.plugin.springsecurity.annotation.Secured


@Secured(['ROLE_ADMIN'])
@Transactional
class UserController {
	
	def userService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
		if(params.reset!=null){
			redirect(action: "list")
		}
		def criteria = userService.getCriteria(params)
		if(params?.format && params.format != "html"){
		}
		else{
			params.max = Math.min(max ?: 10, 100)
			[userInstanceList: criteria.list(params), userInstanceTotal: criteria.count()]
		}
    }

    def create() {
        [userInstance: new User(params)]
    }

    def save() {
		def userInstance = userService.save(params)
        if (userInstance.hasErrors()) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }
		
		flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
		redirect(action: "show", id: userInstance.id)
    }

    def show(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def edit(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        [userInstance: userInstance]
    }

    def update(Long id, Long version) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'user.label', default: 'User')] as Object[],
                          "Another user has updated this User while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }
		
		userInstance = userService.update(userInstance, params)
		if (userInstance.hasErrors()) {
			render(view: "edit", model: [userInstance: userInstance])
			return
		}
		
        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def delete(Long id) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        try {
            userInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'user.label', default: 'User'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def picture() {
		def user = User.get(params.id)
		if (!user) {
		  response.sendError(404)
		  return
		}
		response.contentType = 'image/jpeg'
		response.contentLength = user.picture.size()
		OutputStream out = response.outputStream
		out.write(user.picture)
		out.close()
	  }
	
}
