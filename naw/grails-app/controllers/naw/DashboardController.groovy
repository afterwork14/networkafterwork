package naw

import grails.plugin.springsecurity.annotation.Secured
import grails.plugin.springsecurity.SpringSecurityUtils

@Secured(['ROLE_MANAGER','ROLE_ADMIN'])
class DashboardController {
	
    def index() {
		if(SpringSecurityUtils.ifAllGranted("ROLE_ADMIN")){ 
			redirect(action: "home")
		}
		else if(SpringSecurityUtils.ifAllGranted("ROLE_MANAGER")){
			redirect(action: "manager")
		}
	}
	
	def home() {
		return
	}
	
	def manager() {
		redirect(controller: "manager", action: "list")
	}
}
