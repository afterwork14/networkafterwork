package naw

import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured


@Secured(['ROLE_ADMIN'])
class OrderController {

	def orderService
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
		if(params.reset!=null){
			redirect(action: "list")
		}
		def criteria = orderService.getCriteria(params)
        params.max = Math.min(max ?: 10, 100)
        [orderInstanceList: criteria.list(params), orderInstanceTotal: criteria.count()]
    }

    def create() {
        [orderInstance: new Order(params)]
    }

    def save() {
        def orderInstance = orderService.generateWebsiteOrder(params)
        if (orderInstance.hasErrors()) {
            render(view: "create", model: [orderInstance: orderInstance])
            return
        }
		else{
			orderService.save(orderInstance)
	        flash.message = message(code: 'default.created.message', args: [message(code: 'order.label', default: 'Order'), orderInstance.id])
	        redirect(action: "show", id: orderInstance.id)
		}
    }

    def show(Long id) {
        def orderInstance = Order.get(id)
        if (!orderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "list")
            return
        }
        [orderInstance: orderInstance]
    }

    def edit(Long id) {
        def orderInstance = Order.get(id)
        if (!orderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "list")
            return
        }
        [orderInstance: orderInstance]
    }

    def update(Long id, Long version) {
        def orderInstance = Order.get(id)
        if (!orderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (orderInstance.version > version) {
                orderInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'order.label', default: 'Order')] as Object[],
                          "Another user has updated this Order while you were editing")
                render(view: "edit", model: [orderInstance: orderInstance])
                return
            }
        }

        orderInstance.properties = params

        if (!orderInstance.save(flush: true)) {
            render(view: "edit", model: [orderInstance: orderInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'order.label', default: 'Order'), orderInstance.id])
        redirect(action: "show", id: orderInstance.id)
    }

    def delete(Long id) {
        def orderInstance = Order.get(id)
        if (!orderInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "list")
            return
        }
        try {
            orderInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'order.label', default: 'Order'), id])
            redirect(action: "show", id: id)
        }
    }
}
