package naw

import grails.converters.JSON


class MailboxRestController {

	def mailboxService
	//This is post api - /api/contactus
	def save(){
		Hashtable table=new Hashtable();

		def mailbox=new Mailbox(params)

		if(mailbox.validate()){
			//Successful and response with no content
			mailboxService.saveMailbox(mailbox)
			render status:204
		}else{
			//Invalid data
			//messages can be customized as requested
			mailbox.errors.fieldErrors.each {
				def key= it.getField()
				def value= it.getDefaultMessage()
				table.put(key, value);
			}
			response.status=400
			render table as JSON
		}
	}
}
