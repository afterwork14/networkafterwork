package naw

class ForgotPasswordController {
	def emailConfirmationService
	/*
	 * UI will call this with post method method
	 * /api/forgotpwd
	 */
	def save() {
		//1. User will enter email and will click submit
		//2. This controller method will be called
		//3. It will check if email exist in user table.
		//4. If email not exist it will return page with message saying error
		//5. If email exist, it will return page with message to check email for link
		//6. It will also send email with link to reset password.
		//7.
		User user=User.findByEmail(params.email)
		if(user==null){
			//return error page
			render status:400
		}else{
			emailConfirmationService.sendConfirmation([
				to: user.email,
				from: 'noreply@subomatic.com',
				subject: 'Reset your NAW password',
				event: 'resetpwd',
				eventNamespace: 'app',
				view: '/mailtemplates/_resetpwd_email',
				id:'1234'
			])
			render status:200
		}


	}
}
