package naw

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.transaction.annotation.Transactional;

import grails.converters.JSON
import grails.gorm.DetachedCriteria
import grails.plugin.springsecurity.annotation.Secured
import groovy.json.JsonBuilder
import org.apache.commons.logging.LogFactory


@Transactional
class UIController {

	private static final log = LogFactory.getLog(this);
	
	static allowedMethods = [getUser: "GET", getIndustries: "GET", saveUser: "POST", updateUser: "PUT"]

	def userService
	def emailConfirmationService
	def orderService
	def eventService
	def cityService
	def venueService
	def countryService
	def facebookService
	
	def getUserProfile(Long id) {
		if(id == null){
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		def userInstance = User.get(id)
		if (!userInstance) {
			response.status = 400
			render([
				"ErrorCode": 152,
				"Message": "User not found"
			] as JSON)
		}
		else {
			response.status = 201
			render([
				"id": userInstance.id,
				"username" : userInstance.username,
				"firstname" : userInstance.firstname,
				"lastname" : userInstance.lastname,
				"email" : userInstance.email,
				"phone" : userInstance.phone,
				"gender" : userInstance.gender,
				"jobtitle" : userInstance.jobtitle,
				"company" : userInstance.company,
				"industry" : userInstance.industry?.name,
				"source" : userInstance.source?.name,
				"cities" : userInstance.cities.collect { it.name } 
			] as JSON)
		}
	}
	
	def getUserOrders(Long id) {
		if(id == null){
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		def userInstance = User.get(id)
		if (!userInstance) {
			response.status = 400
			render([
				"ErrorCode": 152,
				"Message": "User not found"
			] as JSON)
		}
		else {
			render userInstance.orders as JSON
		}
	}

	def checkUserExists(String email) {
		if(email == null){
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		def userInstance = User.findByEmail(email)
		if (!userInstance) {
			response.status = 400
			render([
				"ErrorCode": 152,
				"Message": "User not found"
			] as JSON)
		}
		else {
			response.status = 201
			render([
				"id": userInstance.id,
				"email" : userInstance.email
			] as JSON)
		}
	}

	def getIndustries() {
		def criteria = new DetachedCriteria(Industry)
		criteria.eq("active", true)
		criteria.order("name", "asc")
		def results = criteria.list()
		render(contentType: "application/json") {
			industries = array {
				for (i in results) {
					industry(name : i.name, id : i.id)
				}
			}
		}
	}

	def getCities() {
		def criteria = new DetachedCriteria(City)
		criteria.eq("active", true)
		criteria.order("name", "asc")
		def results = criteria.list()
		render(contentType: "application/json") {
			cities = array {
				for (c in results) {
					city(name : c.name, id : c.id)
				}
			}
		}
	}
	
	def getUserLeadSources() {
		def criteria = new DetachedCriteria(Source)
		criteria.eq("active", true)
		criteria.order("name", "asc")
		def results = criteria.list()
		render(contentType: "application/json") {
			sources = array {
				for (c in results) {
					source(name : c.name, id : c.id)
				}
			}
		}
	}

	def saveUser(params) {
		if(request.JSON.email == null
		|| request.JSON.password == null
		|| request.JSON.repassword == null
		|| request.JSON.selectedCities == null
		|| request.JSON.selectedCities.size() == 0){
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		
		if(!request.JSON.password.equals(request.JSON.repassword)){
			response.status = 400
			render([
				"ErrorCode": 150,
				"Message": "Passwords dont match"
			] as JSON)
		}
		def userInstance = User.findByEmail(request.JSON.email)
		if (userInstance) {
			response.status = 400
			render([
				"ErrorCode": 151,
				"Message": "Email address already exists"
			] as JSON)
		}
		request.JSON.webuser = true
		userInstance = userService.save(request.JSON)
		if (userInstance.hasErrors()) {
			response.status = 400
			render([
				"ErrorCode": 154,
				"Message": "Validation errors found",
				"Errors": userInstance.errors
			] as JSON)
		}
		else {
			response.status = 201
			render([
				"id": userInstance.id
			] as JSON)
		}
	}
	

	//@Secured(['ROLE_USER'])
	def updateUser(params) {
		if(request.JSON.id == null || 
			(request.JSON.selectedCities != null
			&& request.JSON.selectedCities.size() == 0)){
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		def userInstance = User.get(request.JSON.id)
		if (!userInstance) {
			response.status = 400
			render([
				"ErrorCode": 152,
				"Message": "User not found"
			] as JSON)
		}
		request.JSON.webuser = true
		userInstance = userService.update(request.JSON)
		if (userInstance.hasErrors()) {
			response.status = 400
			render([
				"ErrorCode": 154,
				"Message": "Validation errors found",
				"Errors": userInstance.errors
			] as JSON)
		}
		else {
			response.status = 201
			render([
				"id": userInstance.id,
				"success" : true
			] as JSON)
		}
	}
	
	//@Secured(['ROLE_USER'])
	def forgotPassword(params) {
		if(request.JSON.email == null) {
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
		def userInstance = User.findByEmail(request.JSON.email)
		if (!userInstance) {
			response.status = 400
			render([
				"ErrorCode": 152,
				"Message": "User not found"
			] as JSON)
		}
		else {
			emailConfirmationService.sendConfirmation([
				to: request.JSON.email,
				from: 'noreply@networkafterwork.com',
				subject: 'Reset your NAW password',
				event: 'resetpwd',
				eventNamespace: 'app',
				view: '/mailtemplates/_resetpwd_email',
				id:'1234'
			])
			
			response.status = 201
			render([
				"id": userInstance.id,
				"success" : true
			] as JSON)
		}
	}
	
	//@Secured(['ROLE_USER'])
	def placeOrder(params) {
		if(request.JSON.userId == null
			|| request.JSON.eventId == null
			|| request.JSON.userId == null
			|| request.JSON.rsvps == null
			|| request.JSON.orderTotal == null
			|| request.JSON.paidAmount == null) {
			response.status = 400
			render([
				"ErrorCode": 100,
				"Message": "Invalid Payload"
			] as JSON)
		}
			
		def orderParams = [:]
		
		User user=User.get(request.JSON.userId)
		Event event=Event.get(request.JSON.eventId)
		
		orderParams["userId"] = user.id
		orderParams["eventId"] = event.id
		orderParams["orderTotal"] = request.JSON.orderTotal
		orderParams["paidAmount"] = request.JSON.paidAmount
		orderParams["source"] = NawConstants.ORDER_WEBSITE
		def rsvps = []
		for(rsvp in request.JSON.rsvps){
			def r = [:]
			r["firstname"] = rsvp.firstname
			r["lastname"] = rsvp.lastname
			r["email"] = rsvp.email
			rsvps.add(r)
		}
		orderParams["rsvps"] = new JsonBuilder(rsvps).toString()
		
		def newOrder = orderService.generateWebsiteOrder(params)
//		if(order.hasProperty("creditcard")){
//			def creditcard = new CreditCard()
//			creditcard.setNumber(order["creditcard"][0]["number"])
//			creditcard.setCvv(order["creditcard"][0]["cvv"])
//			creditcard.setExpiration(order["creditcard"][0]["expiration"])
//			creditcard.setStreet(order["creditcard"][0]["street"])
//			creditcard.setZip(order["creditcard"][0]["zip"])
//			creditcard.setPostboxnum(order["creditcard"][0]["postboxnum"])
//			orderService.savePaypalProOrder(newOrder, creditcard)
//		}
//		else{
//			newOrder.save(flush: true,failOnError: true)
//		}
	}
	
	def upcomingEventsByGeoLocation(Double latitude, Double longitude) {
		def events = eventService.getUpcomingEventsByGeoLocation(latitude, longitude)
		render events as JSON
	}
	
	def futureEventsByGeoLocation(Double latitude, Double longitude) {
		def events = eventService.getFutureEventsByGeoLocation(latitude, longitude)
		render events as JSON
	}
	
	def pastEventsByGeoLocation(Double latitude, Double longitude) {
		def events = eventService.getPastEventsByGeoLocation(latitude, longitude)
		render events as JSON
	}
	
	def upcomingEventsByCity(Long id) {
		def cityInstance = City.get(id)
		def events = eventService.getUpcomingEventsByGeoLocation(cityInstance.location.x, cityInstance.location.y)
		render events as JSON
	}
	
	def futureEventsByCity(Long id) {
		def cityInstance = City.get(id)
		def events = eventService.getFutureEventsByGeoLocation(cityInstance.location.x, cityInstance.location.y)
		render events as JSON
	}
	
	def pastEventsByCity(Long id) {
		def cityInstance = City.get(id)
		def events = eventService.getPastEventsByGeoLocation(cityInstance.location.x, cityInstance.location.y)
		render events as JSON
	}
	
	def getCityImages(Long id) {
		def images = cityService.getImages(id)
		render images as JSON
	}
	
	def getCountryImages(Long id) {
		def images = countryService.getImages(id)
		render images as JSON
	}
	
	def getVenueImages(Long id) {
		def venueInstance = Venue.get(id)
		if (!venueInstance) {
		  response.sendError(404)
		  return
		}
		def images = venueService.getImages(venueInstance)
		render images as JSON
	  }
}