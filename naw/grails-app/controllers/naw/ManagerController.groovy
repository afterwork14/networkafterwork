package naw

import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.transaction.annotation.Transactional;

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_MANAGER'])
@Transactional
class ManagerController {

	def springSecurityService
	def orderService
	
    def index() { }
	
	
	def list() {
		[eventInstanceList: Event.findAllByManager(springSecurityService.currentUser)]
	}
	
	def event(Long id) {
		def eventInstance = Event.get(id)
		if (!eventInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
			redirect(action: "list")
			return
		}
		JSONObject event = new JSONObject();
		event.put("id", eventInstance.id)
		event.put("name", eventInstance.getName())
		def rsvps = []
		for(order in eventInstance.orders){
			for(rsvp in order.rsvps) {
				JSONObject rsvp1 = new JSONObject();
				rsvp1.put("orderId", order.id)
				rsvp1.put("rsvpId", rsvp.id)
				rsvp1.put("company", rsvp.user?.company?.split(' ').collect{ it.capitalize() }.join(' '))
				rsvp1.put("orderSource", order.source)
				rsvp1.put("orderTime", order.getDateCreated())
				rsvp1.put("orderedBy", (order.user.firstname + " " + order.user.lastname).split(' ').collect{ it.capitalize() }.join(' '))
				if(rsvp.user) {
					rsvp1.put("label", (rsvp.user.firstname + " " + rsvp.user.lastname).split(' ').collect{ it.capitalize() }.join(' '))
				}
				else if(rsvp.prospectiveUser) {
					if(rsvp.prospectiveUser.name) {
						rsvp1.put("label", rsvp.prospectiveUser.name.split(' ').collect{ it.capitalize() }.join(' '))
					}
					else if(rsvp.prospectiveUser.email) {
						rsvp1.put("label", rsvp.prospectiveUser.email)
					}
					else if(rsvp.prospectiveUser.phone) {
						rsvp1.put("label", rsvp.prospectiveUser.phone)
					}
					else {
						rsvp1.put("label", "Not Available")
					}
				}
				if(order.paidAmount == order.orderTotal){
					rsvp1.put("paid", true)
				} else{
					rsvp1.put("paid", false)
				}
				if(rsvp.entryTime) {
					rsvp1.put("checkedin", true)
					rsvp1.put("entryTime", rsvp.entryTime)
				} else {
					rsvp1.put("checkedin", false)
				}
				rsvps.add(rsvp1);
			}
		}
		rsvps.sort {it.label.toLowerCase()}
		[event: event, rsvps: rsvps]
	}
	
	def checkin(Long id) {
		Rsvp rsvp = Rsvp.get(id);
		Order order = rsvp.order;
		rsvp.setEntryType(NawConstants.MANUAL_CHECKIN)
		rsvp.setEntryTime(new Date())
		int c = Double.compare(rsvp.getPaidFee(), rsvp.getEntryFee())
		if(c != 0){
			rsvp.setPaidFee(rsvp.getEntryFee())
			if(!order.getPayStatus().equalsIgnoreCase(NawConstants.ORDER_FULLY_PAID)) {
				order.setPaidAmount(order.getPaidAmount() + rsvp.getPaidFee())
				c = Double.compare(order.getPaidAmount(), order.getOrderTotal())
				if(c >= 0) {
					order.setPayStatus(NawConstants.ORDER_FULLY_PAID)
				}
				else if (c < 0 && Double.compare(order.getPaidAmount(), 0.00) > 0) {
					order.setPayStatus(NawConstants.ORDER_PARTIALLY_PAID)
				}
				order.save(flush: true)
			}
		}
		rsvp.save(flush: true)
		response.status = 200
			render([
				"id": id,
				"entryTime": rsvp.getEntryTime().format("hh:mm:ss aa")
			] as JSON)
	}
	
	def undocheckin(Long id) {
		Rsvp rsvp = Rsvp.get(id);
		rsvp.setEntryType(null)
		rsvp.setEntryTime(null)
		rsvp.save(flush: true)
		response.status = 200
	}
	
	def order() {
		JSONObject orderParams = new JSONObject();
		orderParams.put("eventId", params.eventId)
		orderParams.put("userId", springSecurityService.currentUser.id)
		orderParams.put("email", params.email)
		orderParams.put("phone", params.phone)
		orderParams.put("orderTotal", "20.00")
		orderParams.put("paidAmount", params.amount)
		def order = orderService.saveDoorOrder(orderParams)
		response.status = 200
		render([
			"success": true,
			"orderNumber": order.id
		] as JSON)
	}
}
