package naw

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

import grails.plugin.springsecurity.SpringSecurityUtils;
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional
class EventController {

	def eventService
	def springSecurityService
	def grailsApplication
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }
  
    def list(Integer max) {
		if(params.reset!=null){
			redirect(action: "list")
		}
		def criteria = eventService.getCriteria(params)
        params.max = Math.min(max ?: 10, 100)
        [eventInstanceList: criteria.list(params), eventInstanceTotal: criteria.count()]
    }

    def create() {
        [eventInstance: new Event(params)]
    }

    def save() {
        def eventInstance = eventService.save(params)

        if (eventInstance.hasErrors()) {
            render(view: "create", model: [eventInstance: eventInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'event.label', default: 'Event'), eventInstance.id])
        redirect(action: "show", id: eventInstance.id)
    }
	
	def show(Long id) {
		def eventInstance = Event.get(id)
		if (!eventInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
			redirect(action: "list")
			return
		}
		def eventVO = eventService.show(id)
		[eventInstance: eventInstance, eventVO: eventVO]
	}

    def edit(Long id) {
        def eventInstance = Event.get(id)
        if (!eventInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
            redirect(action: "list")
            return
        }

        [eventInstance: eventInstance]
    }

    def update(Long id, Long version) {
        def eventInstance = Event.get(id)
        if (!eventInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (eventInstance.version > version) {
                eventInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'event.label', default: 'Event')] as Object[],
                          "Another user has updated this Event while you were editing")
                render(view: "edit", model: [eventInstance: eventInstance])
                return
            }
        }

        eventInstance.properties = params

        if (!eventInstance.save(flush: true)) {
            render(view: "edit", model: [eventInstance: eventInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'event.label', default: 'Event'), eventInstance.id])
        redirect(action: "show", id: eventInstance.id)
    }

    def delete(Long id) {
        def eventInstance = Event.get(id)
        if (!eventInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
            redirect(action: "list")
            return
        }

        try {
            eventInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'event.label', default: 'Event'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'event.label', default: 'Event'), id])
            redirect(action: "show", id: id)
        }
    }
	
//	def gallery(Long id) {
//		def eventInstance = Event.get(id)
//		if (!eventInstance) {
//			flash.message = message(code: 'default.not.found.message', args: [message(code: 'event.label', default: 'Event'), id])
//			redirect(action: "list")
//			return
//		}
//		def imageURLs = []
//		ObjectListing objectListing = amazonWebService.s3.listObjects(grailsApplication.grails.aws.s3.bucket)
//		objectListing.objectSummaries.each { S3ObjectSummary summary ->
//			imageURLs.push(summary.key);
//		}
//        [imageURLs: imageURLs]
//	  }
//	
//	def picture() {
//		S3Object image = amazonWebService.s3.getObject(grailsApplication.grails.aws.s3.bucket, params.key)
//		response.contentType = 'image/jpeg'
//		OutputStream out = response.outputStream
//		out.write(image.objectContent.in.getBytes())
//		out.close()
//	  }
}
