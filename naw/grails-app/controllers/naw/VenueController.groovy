package naw
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.transaction.annotation.Transactional;

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional
class VenueController {
	
	def venueService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
	def filterPaneService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [venueInstanceList: Venue.list(params), venueInstanceTotal: Venue.count()]
    }

    def create() {
        [venueInstance: new Venue(params)]
    }

    def save() {
       def venueInstance = venueService.save(params)

        if (!venueInstance) {
            render(view: "create", model: [venueInstance: venueInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'venue.label', default: 'Venue'), venueInstance.id])
        redirect(action: "show", id: venueInstance.id)
    }

    def show(Long id) {
        def venueInstance = Venue.get(id)
        if (!venueInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "list")
            return
        }
		def images = venueService.getImages(venueInstance)
        [venueInstance: venueInstance, images: images]
    }

    def edit(Long id) {
        def venueInstance = Venue.get(id)
        if (!venueInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "list")
            return
        }

        [venueInstance: venueInstance]
    }

    def update(Long id, Long version) {
        def venueInstance = Venue.get(id)
        if (!venueInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (venueInstance.version > version) {
                venueInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'venue.label', default: 'Venue')] as Object[],
                          "Another user has updated this Venue while you were editing")
                render(view: "edit", model: [venueInstance: venueInstance])
                return
            }
        }

        venueInstance.properties = params

        if (!venueInstance.save(flush: true)) {
            render(view: "edit", model: [venueInstance: venueInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'venue.label', default: 'Venue'), venueInstance.id])
        redirect(action: "show", id: venueInstance.id)
    }

    def delete(Long id) {
        def venueInstance = Venue.get(id)
        if (!venueInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "list")
            return
        }

        try {
            venueInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'venue.label', default: 'Venue'), id])
            redirect(action: "show", id: id)
        }
    }
	
	def history(Long id) {
		[venueInstanceHistoryList: Venue.findAllRevisionsById(id), id: id]
	}
	
	def venuesByCity() {
		def city = City.get(params.cityId as long)
		def venues = Venue.findAllByCity(city)
		def map = [:]
		for(venue in venues){
			map.put(venue.id, venue.name + " - " + venue.address1)
		}
		render map as JSON
	 }
}
