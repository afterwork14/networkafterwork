package naw

import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured


@Secured(['ROLE_ADMIN'])
class RsvpController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [rsvpInstanceList: Rsvp.list(params), rsvpInstanceTotal: Rsvp.count()]
    }

    def create() {
        [rsvpInstance: new Rsvp(params)]
    }

    def save() {
        def rsvpInstance = new Rsvp(params)
        if (!rsvpInstance.save(flush: true)) {
            render(view: "create", model: [rsvpInstance: rsvpInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), rsvpInstance.id])
        redirect(action: "show", id: rsvpInstance.id)
    }

    def show(Long id) {
        def rsvpInstance = Rsvp.get(id)
        if (!rsvpInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "list")
            return
        }

        [rsvpInstance: rsvpInstance]
    }

    def edit(Long id) {
        def rsvpInstance = Rsvp.get(id)
        if (!rsvpInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "list")
            return
        }

        [rsvpInstance: rsvpInstance]
    }

    def update(Long id, Long version) {
        def rsvpInstance = Rsvp.get(id)
        if (!rsvpInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (rsvpInstance.version > version) {
                rsvpInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'rsvp.label', default: 'Rsvp')] as Object[],
                          "Another user has updated this Rsvp while you were editing")
                render(view: "edit", model: [rsvpInstance: rsvpInstance])
                return
            }
        }

        rsvpInstance.properties = params

        if (!rsvpInstance.save(flush: true)) {
            render(view: "edit", model: [rsvpInstance: rsvpInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), rsvpInstance.id])
        redirect(action: "show", id: rsvpInstance.id)
    }

    def delete(Long id) {
        def rsvpInstance = Rsvp.get(id)
        if (!rsvpInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "list")
            return
        }

        try {
            rsvpInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'rsvp.label', default: 'Rsvp'), id])
            redirect(action: "show", id: id)
        }
    }
}
