package naw
import org.hibernate.envers.Audited;

@Audited
class Comment extends AbstractDomain {

	String text
	
	static belongsTo = [user:User, event: Event]
	
	static mapping = {
		id generator: 'identity'
	}
	
    static constraints = {
		text(blank:false, nullable:false)
    }
}
