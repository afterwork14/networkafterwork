package naw
import org.hibernate.envers.Audited

@Audited
class Event extends AbstractDomain {
	
	String name;
	Integer capacity;
	String description
	Double price;
	Date startTime;
	Date endTime;
	String meetupId;
	String facebookEventId;
	String eventbriteId;
	String s3Bucket;
	byte[] document;
	boolean active
	
	Venue venue;
	User manager;
	static hasMany = [orders: Order, priceModifiers: PriceModifier, comments: Comment];
	
	static mapping = {
		id generator: 'identity'
		cache usage: 'nonstrict-read-write'
		active(defaultValue:true)
		orders sort: 'dateCreated', order: 'desc'
	}
			
    static constraints = {
		name(blank:false, nullable:false)
		capacity(blank:false, nullable:false)
		description(size: 1..5000, blank:true, nullable:true)
		price(blank:false, nullable:false)
		startTime(blank:false, nullable:false)
		endTime(blank:false, nullable:false)
		venue(nullable:false)
		meetupId(nullable:true)
		facebookEventId(nullable:true)
		eventbriteId(nullable:true)
		s3Bucket(nullable:false)
		document(nullable:true, maxSize:1073741824)
		manager(nullable:true)
		endTime(validator: { val, obj ->
			if(val?.before(obj.startTime)) ['dateBefore', val.toString(), obj.startTime] 
		})
    }
	
	def beforeValidate() {
		s3Bucket = UUID.randomUUID().toString()
	}
}
