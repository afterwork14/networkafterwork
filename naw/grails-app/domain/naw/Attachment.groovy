package naw

import org.hibernate.envers.Audited;

@Audited
class Attachment extends AbstractDomain {

	byte[] item
	
    static constraints = {
		item(blank:false, nullable:false)
    }
}
