package naw

import org.hibernate.envers.Audited

@Audited
class Role extends AbstractDomain {
	
	String authority
	String description
	boolean active

	static mapping = {
		cache true
		id generator: 'identity'
		active(defaultValue:true)
	}

	static constraints = {
		authority blank: false, unique: true
	}
}
