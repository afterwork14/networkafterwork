package naw

import org.hibernate.envers.Audited;

@Audited
class PriceModifier extends AbstractDomain {
	
	double value
	String method
	String type
	String level
	String description
	boolean active

	static mapping = { 
		id generator: 'identity' 
		active(defaultValue:true)
	}
	
    static constraints = {
		value(blank: false, nullable: false)
		method(inList: ["Amount", "Percent"])
		type(inList: ["Discount", "Surcharge"])
		level(inList: ["Order", "RSVP"])
		description(blank: false, nullable: false)
    }
}
