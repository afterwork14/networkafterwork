package naw

class Log {
	
	long id
	String name;
	Date date;
	
    static constraints = {
    }
	
	static mapping = {
		id generator: 'identity'
	}
}
