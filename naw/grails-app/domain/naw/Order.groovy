package naw

import org.hibernate.envers.Audited

@Audited
class Order extends AbstractDomain {
	
	String source
//	String orderNumber
	String sourceId
	Double orderTotal
	Double paidAmount
	String payStatus
	boolean active
	Transaction transaction
	Attachment contract
	static hasMany = [rsvps: Rsvp]
	static belongsTo = [user:User, event: Event]
	
	static mapping = {
		table "orders"
	    id generator: 'identity'
		transaction cascade: 'all'
		active(defaultValue:true)
		rsvps sort: 'dateCreated', order: 'desc'
	}
	
    static constraints = {
		source(inList: [NawConstants.ORDER_EVENTBRITE, NawConstants.ORDER_MEETUP, NawConstants.ORDER_WEBSITE, NawConstants.ORDER_AT_DOOR])
		sourceId(blank:true, nullable:true)
		orderTotal(blank:false, nullable:false)
		paidAmount(blank:false, nullable:false)
		contract(blank:true, nullable:true)
		payStatus(inList: [NawConstants.ORDER_FULLY_PAID, NawConstants.ORDER_NOT_PAID, NawConstants.ORDER_PARTIALLY_PAID])
		transaction(blank:true, nullable:true)
    }
	
//	def beforeValidate() {
//		int nextVal = this.ident()
//		if(user.orders){
//			nextVal = user.orders.size()
//		}
//		if (orderNumber == null) {
//			orderNumber = event.venue.state.abbreviation.concat("-")
//			.concat(event.venue.city.name.substring(0, 3))
//			.concat(event.id.toString()).concat("E")
//			.concat(user.id.toString()).concat("U")
//			.concat(nextVal.toString()).toUpperCase()
//		}
//	 }
}
