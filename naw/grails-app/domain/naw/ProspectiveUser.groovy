package naw

import org.hibernate.envers.Audited;

@Audited
class ProspectiveUser extends AbstractDomain {

	String name
	String email
	String phone
	String sourceId
	static belongsTo = [rsvp:Rsvp]
	
	static mapping = {
		id generator: 'identity'
	}
	
	static constraints = {
		name(blank:true, nullable:true)
		sourceId(blank:true, nullable:true)
		email(blank:true, nullable:true)
		phone(blank:true, nullable:true)
	}
}
