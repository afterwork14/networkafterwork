package naw

import org.hibernate.envers.Audited;

import com.vividsolutions.jts.geom.Point;
import org.hibernatespatial.GeometryUserType

@Audited
class City extends AbstractDomain {

	transient springSecurityService
	
	String name;
	String meetupGroupUrlName;
	String meetupGroupId;
	String meetupKey;
	State state;
	int emailDirectListId;
	boolean active
	static hasMany = [users:User, venues: Venue, pictures: Attachment];
	static transients = ['springSecurityService']
	Point location
	
	static mapping = { 
		id generator: 'identity'
		active(defaultValue:true)
		location type: GeometryUserType
	}
	
    static constraints = {
		name(blank:false, nullable:false)
		meetupGroupUrlName(nullable:true)
		meetupGroupId(nullable:true)
		meetupKey(nullable:true)
		state(blank:false, nullable:false)
		emailDirectListId(nullable:true)
		location(nullable: false)
    }
	
	def beforeInsert() {
		if(meetupKey != null){
			encodeMeetupKey()
		}
	}

	def beforeUpdate() {
		if (meetupKey != null && isDirty('meetupKey')) {
			encodeMeetupKey()
		}
	}

	protected void encodeMeetupKey() {
		meetupKey = springSecurityService.encodePassword(meetupKey)
	}
}
