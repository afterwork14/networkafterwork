package naw

import org.hibernate.envers.Audited;

@Audited
class User extends AbstractDomain {

	transient springSecurityService

	String username
	String password
	String firstname
	String lastname
	String email
	String phone
	String gender
	String jobtitle
	String company
	byte[] picture
	int emailDirectEmailId
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
	boolean emailVerified
	boolean active
	String facebookId;

	Industry industry
	Source source
	static hasMany = [orders: Order,cities:City, comments: Comment]
	static belongsTo = City
	static transients = ['springSecurityService']
	
	static mapping = {
		password column: '`password`'
		id generator: 'identity'
		enabled defaultValue:true
		active defaultValue:true
		emailVerified defaultValue:false
	}

	static constraints = {
		username(blank: false, unique: true)
		password(blank: true, nullable:true)
		firstname(blank:true, nullable:true)
		lastname(blank:true, nullable:true)
		email(blank:false, nullable:false, email:true, unique: true)
		phone(blank:true, nullable:true)
		gender(inList: ["Male", "Female"], nullable:true)
		jobtitle(blank:true, nullable:true)
		company(blank:true, nullable:true)
		picture (nullable: true, maxSize:1073741824)
		emailDirectEmailId(nullable:true)
		industry(blank: true, nullable: true)
		source(blank: true, nullable: true)
		facebookId(blank: true, nullable: true)
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		if(password != null) {
			password = springSecurityService.encodePassword(password)
		}
	}
}
