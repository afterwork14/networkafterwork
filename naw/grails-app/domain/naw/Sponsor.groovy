package naw
import org.hibernate.envers.Audited

@Audited
class Sponsor extends AbstractDomain {
	
	String name
	boolean active
	
	static mapping = {
		id generator: 'identity'
		active(defaultValue:true)
	}
	
    static constraints = {
		id blank: false, unique: false
		name size: 6..255, blank: false, unique: false
    }
}
