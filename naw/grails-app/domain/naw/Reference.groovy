package naw
import org.hibernate.envers.Audited

@Audited
class Reference extends AbstractDomain {

	String name
	String value
	boolean active
	
	static mapping = {
		id generator: 'identity'
		cache usage: 'nonstrict-read-write'
		active(defaultValue:true)
	}
	
    static constraints = {
		name(blank:false, nullable:false)
		value(blank:false, nullable:false)
    }
}
