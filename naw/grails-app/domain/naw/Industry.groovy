package naw
import org.hibernate.envers.Audited

@Audited
class Industry extends AbstractDomain {

	String name
	boolean active

	static mapping = {
		id generator: 'identity'
		cache usage: 'nonstrict-read-write'
		active(defaultValue:true)
	}
	
	static constraints = {
		name(blank:false, nullable:false)
	}
}
