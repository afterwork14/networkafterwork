package naw
import com.vividsolutions.jts.geom.Point
import org.hibernate.envers.Audited;
import org.hibernatespatial.GeometryUserType

@Audited
class Venue extends AbstractDomain {
	
	String name;
	String address1;
	String address2;
	City city;
	String postalcode;
	State state;
	Country country;
	String meetupId;
	boolean active
	Point location

	static hasMany = [events: Event];
	
	static mapping = {
		id generator: 'identity'
		active(defaultValue:true)
		location type: GeometryUserType
	}
	
    static constraints = {
		name(blank:false, nullable:false)
		address1(blank:false, nullable:false)
		address2(blank:true, nullable:true)
		city(blank:false, nullable:false)
		postalcode(blank:false, nullable:false)
		state(blank:false, nullable:false)
		country(blank:false, nullable:false)
		meetupId(blank:true, nullable:true)
		location(nullable: false)
    }
}
