package naw
import org.hibernate.envers.Audited

@Audited
class Source extends AbstractDomain {

    String name
	String description
	boolean active
	
	static mapping = {
		id generator: 'identity'
		cache usage: 'nonstrict-read-write'
		active(defaultValue:true)
	}
	
    static constraints = {
		name(blank:false, nullable:false)
		description(blank:false, nullable:false)
    }
}
