package naw
import org.hibernate.envers.Audited

@Audited
class Rsvp extends AbstractDomain {
	
	User user
	ProspectiveUser prospectiveUser
	Date entryTime
	String entryType
	Double entryFee
	Double paidFee

   static belongsTo = [order:Order]
   
   static mapping = {
	   id generator: 'identity'
   }
	
   static constraints = {
		user(nullable:true)
		prospectiveUser(nullable:true)
		entryType(inList: ["AUTO", "MANUAL"], blank:true, nullable:true)
		entryTime(blank:true, nullable:true)
		entryFee(blank:false, nullable:false)
		paidFee(blank:false, nullable:false)
    }
}
