package naw
import org.hibernate.envers.Audited

@Audited
class Country extends AbstractDomain {

	String name
	String abbreviation
	boolean active
	static hasMany = [states: State]
	
	static mapping = {
		id generator: 'identity'
		active(defaultValue:true)
	}

	static constraints = {
		name(blank:false, nullable:false)
		abbreviation(blank:false, nullable:false)
	}	
}
