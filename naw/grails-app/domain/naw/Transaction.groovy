package naw
import org.hibernate.envers.Audited

@Audited
class Transaction extends AbstractDomain {

	String paymentGateway
	String transactionId
	
	static mapping = {
		id generator: 'identity'
	}
	
    static constraints = {
		paymentGateway(inList: ["PAYPAL PRO", "PAYPAL EXPRESS", "CASH"])
		transactionId(blank:true, nullable:true)
    }
}
