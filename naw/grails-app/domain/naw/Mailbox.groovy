package naw

import org.hibernate.envers.Audited;

@Audited
class Mailbox extends AbstractDomain {
	
	String firstname;
	String lastname;
	String email;
	String phone;
	String company;
	String message;
	
	static mapping = {
		id generator: 'identity'
	}

    static constraints = {
		firstname(blank:false, nullable:false)
		lastname(blank:false, nullable:false)
		email(blank:false, nullable:false, email:true)
		phone(blank:false, nullable:false)
		company(blank:false, nullable:false)
		message(blank:false, nullable:false)
    }
}
