package naw
import org.hibernate.envers.Audited

@Audited
class State extends AbstractDomain {

	String name
	String abbreviation
	boolean active
	Country country;
	static hasMany = [cities: City];
	
	static mapping = { 
		id generator: 'identity'
		active(defaultValue:true)
	}

	static constraints = {
		name(blank:false, nullable:false)
		abbreviation(blank:false, nullable:false)
		country(blank:false, nullable:false)
	}
}
