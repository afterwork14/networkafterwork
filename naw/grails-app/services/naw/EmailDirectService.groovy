package naw

import org.codehaus.groovy.grails.web.json.JSONArray
import naw.RestBuilder
import grails.gsp.PageRenderer
import grails.plugins.rest.client.RestResponse
import groovy.json.JsonBuilder

class EmailDirectService {

	PageRenderer groovyPageRenderer
	def grailsApplication
	
	private String getApiKey(){
		return grailsApplication.config.rest.providers.emaildirect.key
	}

	public Integer addNewSubscriber(User user){

		def form = [:]
		form.put("EmailAddress",user.email)
		form.put("ForcePublications", "true")
		form.put("Force", "true")
		
		def CustomFields =  []
		def firstName = [:]
		firstName.put("FieldName","FirstName")
		firstName.put("Value",user.firstname)
		CustomFields.add(firstName)
		
		def lastName = [:]
		lastName.put("FieldName","LastName")
		lastName.put("Value",user.lastname)
		CustomFields.add(lastName)
		
		def gender = [:]
		gender.put("FieldName","gender")
		gender.put("Value",user.gender)
		CustomFields.add(gender)
		
		form.put("CustomFields", CustomFields)

		def mailingLists =  []
		user.cities.each {
			mailingLists.add(it.emailDirectListId)
		}
		form.put("Lists", mailingLists)

		RestResponse postResponse= new RestBuilder().post(Reference.findByName("ADD_NEW_SUBSCRIBER_URL").value){
			contentType("application/json")
			header("ApiKey", getApiKey())
			json new JsonBuilder(form).toString()
		}
		if(postResponse.status == 201){
			return postResponse.json.EmailID
		}
	}

	public Integer addList(String name){
		def form = [:]
		form.put("Name", name);
		form.put("Description", "Change my Description");
		RestResponse postResponse= new RestBuilder().post(Reference.findByName("ADD_NEW_LIST").value) {
			contentType("application/json")
			header("ApiKey", getApiKey())
			json new JsonBuilder(form).toString()
		}
		if(postResponse.status == 201){
			return postResponse.json.ListID
		}
		if(postResponse.status == 400){
			if(postResponse.json.ErrorCode == 141){
				RestResponse getResponse= new RestBuilder().get(Reference.findByName("GET_ALL_LISTS").value) {
					contentType("application/json")
					header("ApiKey", getApiKey())
				}
				JSONArray mailingLists = getResponse.json.Lists
				for (mailingList in mailingLists) {
					if(mailingList.Name.equalsIgnoreCase(name)){
						return mailingList.ListID
					}
				}
			}
		}
	}
	
	public void sendProfileUpdateEmail(User userInstance){
		if(Reference.findByName("email_profile_update").value.equalsIgnoreCase(NawConstants.ENABLED)){
			def form = [:]
			form.put("ToEmail", userInstance.email)
			form.put("Subject", "Profile Updated")
			def body = groovyPageRenderer.render(view:"/mailtemplates/_profile_update", model:[email: 'purvin'])
			form.put("HTML", body)
			RestResponse postResponse= new RestBuilder().post(Reference.findByName("PROFILE_UPDATE_CONFIRMATION_URL").value) {
				contentType("application/json")
				header("ApiKey", getApiKey())
				json new JsonBuilder(form).toString()
			}
		}
	}
	
	public void sendOrderConfirmation(){
		if(Reference.findByName("email_orders").value.equalsIgnoreCase(NawConstants.ENABLED)){
			def form = [:]
			form.put("ToEmail", 'purvin.84@gmail.com')
			form.put("Subject", "Order placed successfully")
			def body = groovyPageRenderer.render(view:"/mailtemplates/_order_confirmation", model:[email: 'purvin'])
			form.put("HTML", body)
			form.put("Text", "Text Version of the Email")
			RestResponse postResponse= new RestBuilder().post(Reference.findByName("ORDER_CONFIRMATION_URL").value) {
				contentType("application/json")
				header("ApiKey", getApiKey())
				json new JsonBuilder(form).toString()
			}
			log.debug(postResponse)
		}
	}
}
