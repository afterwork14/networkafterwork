package naw

import com.amazonaws.services.s3.model.ListObjectsRequest
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.transaction.Transactional

@Transactional
class CityService {

	def emailDirectService
	def googleService
	def amazonService
	def grailsApplication

	def City save(params) {

		def cityInstance = new City()
		cityInstance.setName(params.name)
		cityInstance.setMeetupGroupUrlName(params.meetupGroupUrlName)
		cityInstance.setMeetupGroupId(params.meetupGroupId)
		cityInstance.setMeetupKey(params.meetupKey)
		cityInstance.setActive(params.active)
		cityInstance.setState(State.findByAbbreviation(params.state))

		if(cityInstance.name.equals("Chicago") || cityInstance.name.equals("Boston") || cityInstance.name.equals("San Francisco")){
			try{
				int listId = emailDirectService.addList(cityInstance.name)
				cityInstance.setEmailDirectListId(listId);
			}catch(Exception e){
				e.printStackTrace()
			}
		}
		
		def address = cityInstance.name + ", " + cityInstance.state.name + ", " + cityInstance.state.country.name
		def geoLocation = googleService.getPoint(address)
		def lat = (geoLocation.json.results.geometry.location.lat).get(0)
		def lng = (geoLocation.json.results.geometry.location.lng).get(0)
		def point = new GeometryFactory().createPoint(new Coordinate(lat,lng))
		cityInstance.setLocation(point)

		cityInstance.save(flush: true, failOnError: true);
	}
	
	def getImages(Long id){
		City city = City.get(id)
		ListObjectsRequest lor = new ListObjectsRequest();
		lor.setBucketName(grailsApplication.config.grails.aws.s3.bucket)
		lor.setDelimiter("/")
		lor.setPrefix("cities/"+city.name.toLowerCase()+"/banner/")
		return amazonService.getImages(lor)
	}
}
