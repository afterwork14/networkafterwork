package naw

import org.codehaus.groovy.grails.web.json.JSONArray

import grails.converters.JSON

class OrderService extends AbstractService {

	def paypalService
	def emailDirectService
	
	public Order generateWebsiteOrder(Map params){
		User user=User.get(params.get("userId"))
		Event event=Event.get(params.get("eventId"))
		def order = new Order(source: params.get("source"),
			orderTotal:params.get("orderTotal"),
			paidAmount:params.get("paidAmount"),
			user:user,
			event:event
		)
		int c = Double.compare(order.getPaidAmount(), order.getOrderTotal())
		if(c >= 0) {
			order.setPayStatus(NawConstants.ORDER_FULLY_PAID)
		}
		else if (c < 0 && Double.compare(order.getPaidAmount(), 0.00) > 0) {
			order.setPayStatus(NawConstants.ORDER_PARTIALLY_PAID)
		}
		else {
			order.setPayStatus(NawConstants.ORDER_NOT_PAID)
		}
		List rsvps = JSON.parse(params.get("rsvps"))
		for(rsvp in rsvps){
			User rsvpUser = User.findByEmail(rsvp["email"])
			def r=new Rsvp(order: order)
			if(rsvpUser == null){
				def prospectiveUser = new ProspectiveUser(rsvp:r, name: rsvp["firstname"] + " " + rsvp["lastname"], email: rsvp["email"])
				r.prospectiveUser = prospectiveUser
			}
			else{
				r.user = rsvpUser
			}
			r.entryFee = Double.valueOf(rsvp["entryFee"]);
			r.paidFee = Double.valueOf(rsvp["paidFee"]);
			order.addToRsvps(r);
		}
		return order
	}
	
	public Order saveDoorOrder(Map params) {
		User user=User.get(params.get("userId"))
		Event event=Event.get(params.get("eventId"))
		
		def order = new Order(source: NawConstants.ORDER_AT_DOOR,
			orderTotal:params.get("orderTotal"),
			paidAmount:params.get("paidAmount"),
			user:user,
			event:event
		)
		int c = Double.compare(order.getPaidAmount(), order.getOrderTotal())
		if(c >= 0) {
			order.setPayStatus(NawConstants.ORDER_FULLY_PAID)
		}
		else if (c < 0 && Double.compare(order.getPaidAmount(), 0.00) > 0) {
			order.setPayStatus(NawConstants.ORDER_PARTIALLY_PAID)
		}
		else {
			order.setPayStatus(NawConstants.ORDER_NOT_PAID)
		}
		def r=new Rsvp(order: order)
		if(params.get("email")) {
			User doorUser = User.findByEmail(params.get("email"))
			if(doorUser == null){
				def prospectiveUser = new ProspectiveUser(rsvp:r, email: params.get("email"), phone: params.get("phone"))
				prospectiveUser.save()
				r.setProspectiveUser(prospectiveUser)
			}
			else{
				r.setUser(doorUser)
			}
		}
		else if(params.get("phone")){
			User doorUser = User.findByPhone(params.get("phone"))
			if(doorUser == null){
				def prospectiveUser = new ProspectiveUser(rsvp:r, phone: params.get("phone"))
				prospectiveUser.save()
				r.setProspectiveUser(prospectiveUser)
			}
			else{
				r.setUser(doorUser)
			}
		}
		r.entryFee = Double.valueOf(params.get("orderTotal"));
		r.paidFee = Double.valueOf(params.get("paidAmount"));
		r.setEntryType(NawConstants.MANUAL_CHECKIN)
		r.setEntryTime(new Date())
		order.addToRsvps(r);
		
		def transaction = new Transaction()
		transaction.setPaymentGateway(NawConstants.CASH)
		order.setTransaction(transaction)
		
		order.save(flush: true,failOnError: true)
		
		if(params.get("email")) {
			emailDirectService.sendOrderConfirmation()
		}
		
		return order
	}
	
	public Order savePaypalProOrder(Order order, CreditCard cc){
		try{	
			def payment = [:]
			payment.put("orderId", order.id)
			payment.put("amount", String.valueOf(order.orderTotal))
			payment.put("number", cc.number)
			payment.put("cvv", cc.cvv)
			payment.put("expiration", cc.expiration)
			payment.put("street", cc.street)
			payment.put("zip", cc.zip)
			payment.put("postboxnum", cc.postboxnum)
			def paypalResponse = paypalService.submitTransaction(payment)
			
			def transaction = new Transaction()
			transaction.setPaymentGateway(NawConstants.PAYPAL_PRO)
			transaction.setTransactionId(paypalResponse.substring(0, 200))
			order.setTransaction(transaction)
			order.save(flush: true,failOnError: true)
			
			emailDirectService.sendOrderConfirmation()
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public Order saveMeetupOrder(Map params){
		Order order = new Order()
		order.setSource(NawConstants.ORDER_MEETUP)
		order.setSourceId(params.get("meetup_rsvp_id").toString())
		Event event=Event.get(params.get("eventId"))
		if(params.get("pay_status").toString().equalsIgnoreCase(NawConstants.MEETUP_ORDER_PAID_STATUS)){
			order.orderTotal = event.price
			order.paidAmount = event.price
			order.payStatus = NawConstants.ORDER_FULLY_PAID
		} 
		else{
			order.orderTotal = event.price //TODO: calculate price
			order.paidAmount = 0.00
			order.payStatus = NawConstants.ORDER_NOT_PAID
		}
		def rsvp = new Rsvp(order: order)
		def meetupUser = new ProspectiveUser(rsvp:rsvp, name: params.get("name"))
		rsvp.prospectiveUser = meetupUser
		rsvp.entryFee = event.price
		rsvp.paidFee = event.price
		if(params.get("pay_status").toString().equalsIgnoreCase(NawConstants.MEETUP_ORDER_PAID_STATUS)){
			rsvp.paidFee = event.price
		}
		else {
			rsvp.paidFee = 0.00
		}
		order.addToRsvps(rsvp);
		order.event = event
		order.user = User.findByUsername("system")
		
		for (int i = 0; i < params.get("guests"); i++) {
			def r=new Rsvp(order: order)
			def prospectiveUser = new ProspectiveUser(rsvp:r, name: "Guest " + (i+1))
			r.prospectiveUser = prospectiveUser
			r.entryFee = event.price
			if(params.get("pay_status").toString().equalsIgnoreCase(NawConstants.MEETUP_ORDER_PAID_STATUS)){
				r.paidFee = event.price
			}
			else {
				r.paidFee = 0.00
			}
			order.addToRsvps(r);
		}	
		return order.save(flush: true,failOnError: true)
	}
	
	def getCriteria(params){
		def c1=Order.where{
			or{
				if(params.ordernumber){
					eq("id",params.ordernumber)
				}
				if(params.email){
					user{
						ilike("email",getLikeString(params.email))
					}
				}
				if(params.event){
					event{
						eq("id",params.event)
					}
				}
			}
		}
		return c1
	}
	
}
