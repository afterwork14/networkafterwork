package naw

import grails.converters.JSON
import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.json.JSONArray

@Transactional
class ParseService {

    final static PUSH_NOTIFICATION_URL = "https://api.parse.com/1/push"

	def grailsApplication
	def rest = new RestBuilder()
	
	def sendNewEventPushNotification (Event event) {
		
		JSONObject payload = new JSONObject()
		def channels = new JSONArray()
		channels.add("global")
		payload.put("channels", channels)
		JSONObject data = new JSONObject()
		data.put("alert", "New event created in " + event.venue.city.name)
		data.put("badge", "Increment")
		data.put("sound", "default")
		data.put("badge", "Increment")
		data.put("title", "NAW")
		payload.put("data", data)
		payload.put("eventId", event.id)
		def resp = rest.post(PUSH_NOTIFICATION_URL){
			header 'X-Parse-Application-Id', '27SlWUmDrxhDQCf90MWE649wZwaOFBhX1CcHORLs'
			header 'X-Parse-REST-API-Key', 'wyzE4bkE1zRoc5FtesECVz7hGL7v8RvB0oaqrcYx'
			contentType("application/json")
			body(payload.toString())
		}
	}
}
