package naw

import com.amazonaws.services.s3.model.ListObjectsRequest
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory

import grails.gorm.DetachedCriteria
import grails.transaction.Transactional

@Transactional
class VenueService {

	def meetupService
	def googleService
	def amazonService
	def grailsApplication

	public Venue save(params){
		try{
			def venueInstance = new Venue()
			venueInstance.setName(params.name);
			venueInstance.setAddress1(params.address1);
			venueInstance.setAddress2(params.address2);
			venueInstance.setCity(City.findById(params.city));
			venueInstance.setPostalcode(params.postalcode);
			venueInstance.setState(State.findById(params.state));
			venueInstance.setCountry(Country.findById(params.country));
			if(params.meetup){
				def meetupResponse = meetupService.postVenue(venueInstance)
				venueInstance.setMeetupId(meetupResponse.id);
			}
			def address = venueInstance.address1 + ", " + venueInstance.city.name + ", " + venueInstance.postalcode + ", " + venueInstance.state.name +
					", " + venueInstance.country.name

			def geoLocation = googleService.getPoint(address)
			def lat = (geoLocation.json.results.geometry.location.lat).get(0)
			def lng = (geoLocation.json.results.geometry.location.lng).get(0)
			def point = new GeometryFactory().createPoint(new Coordinate(lat,lng))
			venueInstance.setLocation(point)
			return venueInstance.save(flush: true,failOnError: true);
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	def getImages(def venueInstance){
		ListObjectsRequest lor = new ListObjectsRequest();
		lor.setBucketName(grailsApplication.config.grails.aws.s3.bucket)
		lor.setDelimiter("/")
		lor.setPrefix("cities/"+venueInstance.city.name.toLowerCase()+"/venues/"+venueInstance.id.toString()+"/")
		return amazonService.getImages(lor)
	}
}
