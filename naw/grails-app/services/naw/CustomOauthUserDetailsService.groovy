package naw

import java.util.Collection;

import com.odobo.grails.plugin.springsecurity.rest.oauth.OauthUser
import com.odobo.grails.plugin.springsecurity.rest.oauth.OauthUserDetailsService

import grails.transaction.Transactional

import org.pac4j.core.profile.CommonProfile;
import org.pac4j.oauth.profile.OAuth20Profile
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException


class CustomOauthUserDetailsService implements OauthUserDetailsService {

    @Delegate
    UserDetailsService userDetailsService
	
	def facebookService
	def userService

    public OauthUser loadUserByUserProfile(OAuth20Profile userProfile, Collection<GrantedAuthority> defaultRoles)
            throws UsernameNotFoundException {
        UserDetails userDetails
        OauthUser oauthUser

        try {
            log.debug "Trying to fetch user details for user profile: ${userProfile}"
            userDetails = userDetailsService.loadUserByUsername userProfile.id
            Collection<GrantedAuthority> allRoles = userDetails.authorities + defaultRoles
            oauthUser = new OauthUser(userDetails.username, userDetails.password, allRoles, userProfile)
        } catch (UsernameNotFoundException unfe) {
            log.debug "User not found. Creating a new one with default roles: ${defaultRoles}"
			def fbUser = facebookService.getFacebookUser(userProfile);
			def user = userService.saveFacebookUser(fbUser);
            oauthUser = new OauthUser(user.username, 'N/A', defaultRoles, userProfile)
        }
        return oauthUser
    }
	@Override
	public UserDetails loadUserByUsername(String arg0)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public OauthUser loadUserByUserProfile(CommonProfile userProfile,
			Collection<GrantedAuthority> defaultRoles)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}
}
