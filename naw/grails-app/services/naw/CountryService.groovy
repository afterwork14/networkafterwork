package naw

import grails.transaction.Transactional

import com.amazonaws.services.s3.model.ListObjectsRequest

@Transactional

class CountryService {

	def amazonService
	def grailsApplication
	
	def getImages(Long id){
		Country country = Country.get(id)
		ListObjectsRequest lor = new ListObjectsRequest();
		lor.setBucketName(grailsApplication.config.grails.aws.s3.bucket)
		lor.setDelimiter("/")
		lor.setPrefix("countries/"+country.abbreviation.toUpperCase()+"/banner/")
		return amazonService.getImages(lor)
	}
}
