package naw

import grails.transaction.Transactional
import groovy.json.JsonSlurper
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

@Transactional
class GoogleService {

	final static GOOGLE_GEO_URL = "https://maps.googleapis.com/maps/api/geocode/json"
	def grailsApplication
	def rest = new RestBuilder()

	
    def getPoint(String address) {
		def url = GOOGLE_GEO_URL + "?address="+address + "&key="+ grailsApplication.config.grails.google.geolocation.key
		def resp = rest.get(url)
		log.debug("response: " + resp)
		return resp
    }
}
