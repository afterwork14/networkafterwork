package naw

import grails.transaction.Transactional

@Transactional
class MailboxService {
	public Event saveMailbox(Mailbox mailbox){
		try{
			mailbox.save(flush: true,failOnError: true);
			return Mailbox.get(mailbox.id)
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
}
