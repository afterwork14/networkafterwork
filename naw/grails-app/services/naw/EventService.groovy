package naw

import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3ObjectSummary

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.dao.OptimisticLockingFailureException

class EventService {
	
	def springSecurityService
	def meetupService
	def amazonWebService
	def venueService
	def sessionFactory
	def grailsApplication
	def parseService
	
	public Event save(params){
		try{
			def eventInstance = new Event(params)
			
			String df = "MM/dd/yyyy HH:mm:ss"
			String startTime = params.sDate+ " " + params.sTime
			eventInstance.setStartTime(Date.parse(df, startTime))
			String endTime = params.eDate+ " " + params.eTime
			eventInstance.setEndTime(Date.parse(df, endTime))
			
			def priceModifiers = [] as Set
			for(priceModifierId in params.pricemodifiers){
				priceModifiers.add(PriceModifier.findById(priceModifierId))
			}
			eventInstance.setPriceModifiers(priceModifiers)
			
			eventInstance.setActive(false);
			if(!eventInstance.validate()){
				return eventInstance
			}
			
			if(params.meetup){
				def meetupResponse = meetupService.postEvent(eventInstance)
				eventInstance.setMeetupId(meetupResponse.id);
			}
			
			if(params.facebook){
			}
			def event = eventInstance.save(flush: true,failOnError: true);
			parseService.sendNewEventPushNotification(event)
			return event
			
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}


	def show(Long id) {
		def eventInstance = Event.get(id)
		def eventVO = new EventVO()
		int totalOrders = 0
		double totalAmount = 0.00
		double totalPaidAmount = 0.00
		double totalUnpaidAmount = 0.00
		int totalWebsiteOrders = 0
		double totalWebsiteAmount = 0.00
		double totalWebsitePaidAmount = 0.00
		double totalWebsiteUnpaidAmount = 0.00
		int totalMeetupOrders = 0
		double totalMeetupAmount = 0.00
		double totalMeetupPaidAmount = 0.00
		double totalMeetupUnpaidAmount = 0.00
		int totalEventbriteOrders = 0
		double totalEventbriteAmount = 0.00
		double totalEventbritePaidAmount = 0.00
		double totalEventbriteUnpaidAmount = 0.00
		int totalAtDoorOrders = 0
		double totalAtDoorAmount = 0.00
		double totalAtDoorPaidAmount = 0.00
		double totalAtDoorUnpaidAmount = 0.00
		int totalRsvps = 0;
		int newCustomers = 0
		int attendees = 0
		for(order in eventInstance.orders){
			totalOrders++
			totalAmount = totalAmount + order.orderTotal;
			totalPaidAmount = totalPaidAmount + order.paidAmount;
			double unpaidAmount = order.orderTotal - order.paidAmount;
			totalUnpaidAmount = totalUnpaidAmount + unpaidAmount;
			if(order.getSource().equalsIgnoreCase(NawConstants.ORDER_WEBSITE)){
				totalWebsiteOrders++
				totalWebsiteAmount = totalWebsiteAmount + order.orderTotal;
				totalWebsitePaidAmount = totalWebsitePaidAmount + order.paidAmount;
				totalWebsiteUnpaidAmount = totalWebsiteUnpaidAmount + unpaidAmount;
			}
			else if(order.getSource().equalsIgnoreCase(NawConstants.ORDER_AT_DOOR)){
				totalAtDoorOrders++
				totalAtDoorAmount = totalAtDoorAmount + order.orderTotal;
				totalAtDoorPaidAmount = totalAtDoorPaidAmount + order.paidAmount;
				totalAtDoorUnpaidAmount = totalAtDoorUnpaidAmount + unpaidAmount;
			}
			else if(order.getSource().equalsIgnoreCase(NawConstants.ORDER_MEETUP)){
				totalMeetupOrders++
				totalMeetupAmount = totalMeetupAmount + order.orderTotal;
				totalMeetupPaidAmount = totalMeetupPaidAmount + order.paidAmount;
				totalMeetupUnpaidAmount = totalMeetupUnpaidAmount + unpaidAmount;
			}
			else if(order.getSource().equalsIgnoreCase(NawConstants.ORDER_EVENTBRITE)){
				totalEventbriteOrders++
				totalEventbriteAmount = totalEventbriteAmount + order.orderTotal;
				totalEventbritePaidAmount = totalEventbritePaidAmount + order.paidAmount;
				totalEventbriteUnpaidAmount = totalEventbriteUnpaidAmount + unpaidAmount;
			}
			for(rsvp in order.rsvps){
				totalRsvps++;
				if(rsvp.prospectiveUser){
					newCustomers++
				}
				if(rsvp.entryType != null){
					attendees++
				}
			}
		}
		eventVO.setTotalOrders(totalOrders)
		eventVO.setTotalAmount(totalAmount)
		eventVO.setTotalPaidAmount(totalPaidAmount)
		eventVO.setTotalUnpaidAmount(totalUnpaidAmount)
		
		eventVO.setTotalWebsiteOrders(totalWebsiteOrders)
		eventVO.setTotalWebsiteAmount(totalWebsiteAmount)
		eventVO.setTotalWebsitePaidAmount(totalWebsitePaidAmount)
		eventVO.setTotalWebsiteUnpaidAmount(totalWebsiteUnpaidAmount)
		
		eventVO.setTotalAtDoorOrders(totalAtDoorOrders)
		eventVO.setTotalAtDoorAmount(totalAtDoorAmount)
		eventVO.setTotalAtDoorPaidAmount(totalAtDoorPaidAmount)
		eventVO.setTotalAtDoorUnpaidAmount(totalAtDoorUnpaidAmount)
		
		eventVO.setTotalMeetupOrders(totalMeetupOrders)
		eventVO.setTotalMeetupAmount(totalMeetupAmount)
		eventVO.setTotalMeetupPaidAmount(totalMeetupPaidAmount)
		eventVO.setTotalMeetupUnpaidAmount(totalMeetupUnpaidAmount)
		
		eventVO.setTotalEventbriteOrders(totalEventbriteOrders)
		eventVO.setTotalEventbriteAmount(totalEventbriteAmount)
		eventVO.setTotalEventbritePaidAmount(totalEventbritePaidAmount)
		eventVO.setTotalEventbriteUnpaidAmount(totalEventbriteUnpaidAmount)
		
		eventVO.setTotalRsvps(totalRsvps)
		eventVO.setNewCustomers(newCustomers)
		eventVO.setAttendees(attendees)
		
		use(groovy.time.TimeCategory) {
			def timeToGo = eventInstance.getStartTime() - new Date()
			if(timeToGo.days > 0){
				eventVO.setUpcoming(true)
			}
			else if(timeToGo.days < 0){
				eventVO.setPast(true)
			}
			else{
				eventVO.setToday(true)
			}
			eventVO.setDaysToGo(Math.abs(timeToGo.days))
		}
		
//		ObjectListing objectListing = amazonWebService.s3.listObjects(grailsApplication.grails.aws.s3.bucket)
//		def imageURLs = []
//		objectListing.objectSummaries.each { S3ObjectSummary summary ->
//			imageURLs.push(summary.key);
//		}
//		eventVO.setImageURLs(imageURLs)
		
		return eventVO
	}
	
	def getUpcomingEventsByGeoLocation(Double latitude, Double longitude) {
		def today= new Date()
		def startDate = today + 7
		final session = sessionFactory.currentSession
		final String query = "SELECT e.id,\
							 (3959 * Acos(Cos(Radians(:latitude)) * Cos(Radians(X(v.location))) * \
							 Cos(Radians(Y(v.location)) - Radians(:longitude)) +\
							 Sin(Radians(:latitude)) * Sin(Radians(X(v.location))))) AS distance\
							 FROM   event e left join venue v on e.`venue_id` = v.`id`\
							 where e.start_time <= :startDate\
							 and e.start_time >= :today\
							 ORDER  BY distance, e.start_time\
							 LIMIT  0, 5;"
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {
					setDouble('latitude', latitude)
					setDouble('longitude', longitude)
					setDate('startDate', startDate)
					setDate('today', today)
					list()
		}
		JSONArray items = new JSONArray()
		for(row in queryResults) {
			JSONObject item = new JSONObject()
			def event = Event.get(row[0])
			item.put("id", event.id)
			item.put("name", event.name)
			item.put("date", event.startTime.format("MM/dd/yyyy"))
			item.put("startTime", event.startTime.format("HH:mm"))
			item.put("endTime", event.endTime.format("HH:mm"))
			item.put("venue", event.venue.name)
			item.put("city", event.venue.city.name)
			item.put("state", event.venue.state.name)
			item.put("venuePictures", venueService.getImages(event.venue))
			items.add(item)
		}
		return items
	}
	
	def getFutureEventsByGeoLocation(Double latitude, Double longitude) {
		def today= new Date()
		final session = sessionFactory.currentSession
		final String query = "SELECT e.id,\
							 (3959 * Acos(Cos(Radians(:latitude)) * Cos(Radians(X(v.location))) * \
							 Cos(Radians(Y(v.location)) - Radians(:longitude)) +\
							 Sin(Radians(:latitude)) * Sin(Radians(X(v.location))))) AS distance\
							 FROM event e left join venue v on e.`venue_id` = v.`id`\
							 where e.start_time >= :today\
							 ORDER  BY distance, e.start_time;"
							 
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {
					setDouble('latitude', latitude)
					setDouble('longitude', longitude)
					setDate('today', today)
					list()
		}
		JSONArray items = new JSONArray()
		for(row in queryResults) {
			JSONObject item = new JSONObject()
			def event = Event.get(row[0])
			item.put("id", event.id)
			item.put("name", event.name)
			item.put("date", event.startTime.format("MM/dd/yyyy"))
			item.put("startTime", event.startTime.format("HH:mm"))
			item.put("endTime", event.endTime.format("HH:mm"))
			item.put("venue", event.venue.name)
			item.put("city", event.venue.city.name)
			item.put("state", event.venue.state.name)
			item.put("venuePictures", venueService.getImages(event.venue))
			items.add(item)
		}
		return items
	}
	
	def getPastEventsByGeoLocation(Double latitude, Double longitude) {
		def today= new Date()
		final session = sessionFactory.currentSession
		final String query = "SELECT e.id,\
							 (3959 * Acos(Cos(Radians(:latitude)) * Cos(Radians(X(v.location))) * \
							 Cos(Radians(Y(v.location)) - Radians(:longitude)) +\
							 Sin(Radians(:latitude)) * Sin(Radians(X(v.location))))) AS distance\
							 FROM   event e left join venue v on e.`venue_id` = v.`id`\
							 where e.start_time < :today\
							 ORDER  BY distance, e.start_time;"
		final sqlQuery = session.createSQLQuery(query)
		final queryResults = sqlQuery.with {
					setDouble('latitude', latitude)
					setDouble('longitude', longitude)
					setDate('today', today)
					list()
		}
		JSONArray items = new JSONArray()
		for(row in queryResults) {
			JSONObject item = new JSONObject()
			def event = Event.get(row[0])
			item.put("id", event.id)
			item.put("name", event.name)
			item.put("date", event.startTime.format("MM/dd/yyyy"))
			item.put("startTime", event.startTime.format("HH:mm"))
			item.put("endTime", event.endTime.format("HH:mm"))
			item.put("venue", event.venue.name)
			item.put("city", event.venue.city.name)
			item.put("state", event.venue.state.name)
			item.put("venuePictures", venueService.getImages(event.venue))
			items.add(item)
		}
		return items
	}
	
	def getEventDetails(Long eventId) {
		Event event = Event.get(eventId);
		JSONObject item = new JSONObject()
		item.put("id", event.id)
		item.put("name", event.name)
		item.put("date", event.startTime.format("MM/dd/yyyy"))
		item.put("startTime", event.startTime.format("HH:mm"))
		item.put("endTime", event.endTime.format("HH:mm"))
		item.put("venue", event.venue.name)
		item.put("city", event.venue.city.name)
		item.put("state", event.venue.state.name)
		item.put("venuePictures", venueService.getImages(event.venue))
		
		return item
	}

	def getCriteria(params){
		def c1=Event.where{
			if(params.venue){
				venue{
					eq("id",params.venue)
				}
			}
		}
		return c1
	}
	
}
