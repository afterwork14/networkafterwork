package naw

import grails.plugins.rest.client.RestBuilder
import grails.transaction.Transactional
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

@Transactional

class MeetupService {
	
	final static MEETUP_URL = "https://api.meetup.com"
	final static POST_EVENT_URI = "/2/event"
	final static POST_VENUE_URI = "/{urlname}/venues"
	final static GET_RSVPS_URI = "/2/rsvps"
	final static GET_MEMBER_URI = "/2/member"
	
	def grailsApplication
	def rest = new RestBuilder()
	
	def postEvent(Event eventInstance) {	
		MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>()
		form.add("group_id", eventInstance.getVenue().getCity().getMeetupGroupId())
		form.add("group_urlname", eventInstance.getVenue().getCity().getMeetupGroupUrlName())
		form.add("name", eventInstance.name)
		form.add("publish_status", "draft")
		form.add("rsvp_limit", eventInstance.getCapacity())
		form.add("description", eventInstance.getDescription())
		form.add("time", eventInstance.getStartTime().getTime().toString())
		form.add("duration", eventInstance.getStartTime().minus(eventInstance.getEndTime()).toString())
		
		String key = eventInstance.getVenue().getCity().getMeetupKey() // TODO: decode key
		def url = MEETUP_URL + POST_EVENT_URI + "?key="+ key
		def resp = rest.post(url){
			contentType("application/x-www-form-urlencoded")
			body(form)
		}
		log.debug("response: " + resp.json)	
		return resp.json		
	}
	
	def postVenue(Venue venueInstance) {
		MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>()
		
		String key = eventInstance.getVenue().getCity().getMeetupKey() // TODO: decode key
		def url = MEETUP_URL + POST_EVENT_URI + "?key="+ grailsApplication.config.rest.providers.meetup.key
		def resp = rest.post(url){
			contentType("application/x-www-form-urlencoded")
			body(form)
		}
		log.debug("response: " + resp.json)
		return resp.json
	}
	
    def getRsvps(String meetupEventId, String meetUpkey) {
		def variables = [:]
		variables.put("key", meetUpkey)
		variables.put("event_id", meetupEventId)
		variables.put("fields", "pay_status")
		
		def url = MEETUP_URL + GET_RSVPS_URI + "?key={key}&event_id={event_id}&fields={fields}"
		def getResponse = rest.get(url, variables)
		def rsvps = getResponse.json.results
		log.debug("---------------------------RSVPs------------------------------")
		for(rsvp in rsvps){
			log.debug("rsvp id: " + rsvp.rsvp_id + " member id: "+ rsvp.member.member_id + " name: " + rsvp.member.name + " pay status: " + rsvp.pay_status)
		}
		log.debug("---------------------------RSVPs------------------------------")
    }
}
