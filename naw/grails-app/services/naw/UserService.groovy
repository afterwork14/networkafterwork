package naw

import grails.gorm.DetachedCriteria
import grails.transaction.Transactional

import org.springframework.dao.OptimisticLockingFailureException

class UserService extends AbstractService {

	def emailConfirmationService
	def emailDirectService

	public User save(params){
		try{
			def userInstance = new User(params)
			if(params.selectedCities){
				for(cityId in params.selectedCities) {
					userInstance.addToCities(City.get(cityId))
				}
			}
			if(!params.username){
				userInstance.username = params.email
			}
			if(!userInstance.validate()){
				return userInstance
			}
			else {
				int emailId = emailDirectService.addNewSubscriber(userInstance)
				userInstance.setEmailDirectEmailId(emailId)
				userInstance.enabled = true
				userInstance.save(flush: true,failOnError: true);
				
				def userRoles = []
				if(params.roles){
					for(roleId in params.roles) {
						userRoles.add(UserRole.create(userInstance, Role.get(roleId)))
					}
				}
				else {
					if(params.webuser) {
						userRoles.add(UserRole.create(userInstance, Role.findByAuthority("ROLE_USER")))
					}
				}
				UserRole.withTransaction{
					for (userRole in userRoles) {
						userRole.save(flush: true,failOnError: true)
					}
				}
				
				emailConfirmationService.sendConfirmation([
					to: userInstance.email,
					from: 'noreply@nawtest.com',
					subject: 'Confirm your membership!',
					event: 'signup',
					eventNamespace: 'app',
					view: '/mailtemplates/_signup_confirmation',
				])
				return userInstance
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public User saveFacebookUser(fbUser){
		try{
			def userInstance = new User();
			userInstance.email = fbUser.email;
			userInstance.username = fbUser.username;
			userInstance.firstname = fbUser.first_name;
			userInstance.lastname = fbUser.last_name;
			userInstance.gender = fbUser.gender.capitalize() ;
			userInstance.facebookId = fbUser.id;
			
			if(!userInstance.validate()){
				return userInstance
			}
			else {
				int emailId = emailDirectService.addNewSubscriber(userInstance)
				userInstance.setEmailDirectEmailId(emailId)
				userInstance.enabled = true
				userInstance.save(flush: true,failOnError: true);
				
				def userRoles = []
				userRoles.add(UserRole.create(userInstance, Role.findByAuthority("ROLE_USER")))
				
				UserRole.withTransaction{
					for (userRole in userRoles) {
						userRole.save(flush: true,failOnError: true);
					}
				}
				
				return userInstance;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public User update(params){
		try{
			def userInstance = User.get(params.id)
			if(params.selectedCities) {
				for(city in userInstance.cities){
					userInstance.removeFromCities(city)
				}
				for(cityId in params.get("selectedCities")) {
					userInstance.addToCities(City.get(cityId))
				}
			}
			if(params.source){
				userInstance.source = Source.get(params.source)
			}
			if(params.industry){
				userInstance.industry = Industry.get(params.industry)
			}
			userInstance.properties = params
			if(!userInstance.validate()){
				return userInstance
			}
			else{
				def userRoles = []
				UserRole.removeAll(userInstance)
				if(!params.webuser){
					for(roleId in params.get("roles")) {
						userRoles.add(UserRole.create(userInstance, Role.get(roleId)))
					}
				}
				else {
					if(params.webuser) {
						userRoles.add(UserRole.create(userInstance, Role.findByAuthority("ROLE_USER")))
					}
				}
				userInstance.save(flush: true,failOnError: true)
				UserRole.withTransaction{
					for (userRole in userRoles) {
						userRole.save(flush: true,failOnError: true)
					}
				}
				emailDirectService.sendProfileUpdateEmail(userInstance)
				return userInstance
			}
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}

	def getCriteria(params){

		def c1=User.where{
			or{
				if(params.email){
					ilike("email", getLikeString(params.email))
				}
				if(params.username){
					ilike("username", getLikeString(params.username))
				}
				if(params.firstname){
					ilike("firstname", getLikeString(params.firstname))
				}
				if(params.lastname){
					ilike("lastname", getLikeString(params.lastname))
				}
				if(params.city){
					cities{
						eq("id",params.city)
					}
				}
				if(params.industry){
					industry{
						eq("id",params.industry)
					}
				}
				if(params.company){
					ilike("company", getLikeString(params.company))
				}
				if(params.active){
					eq("active", true)
				}
				
			}
		}

		return c1
	}
}
