package naw

import grails.events.Listener
import grails.transaction.Transactional

import com.grailsrocks.emailconfirmation.PendingEmailConfirmation
/*
 * This service listens for email link clicks that we send out using email confirmation plugins. 
 * We can control and add custom logic for different events like link for sign up confirmation, reset password etc.
 * 
 */

@Transactional
class SubscriptionService {
	
	/*
	 * This is to redirect to custom view after successful confirmation.
	 */

	@Listener(topic='signup.confirmed')
	def userConfirmedSubscription(confirmation) {
		def subscription = PendingEmailConfirmation.findByEmailAddress(confirmation.email)
		if (subscription) {
			User user=User.findByEmail(confirmation.email)
			user.emailVerified = true
			user.save(flush: true,failOnError: true);
			return [controller:'emailConfirmation', action:'welcome']
		}
	}


	@Listener(topic='resetpwd.confirmed')
	def resetPasswordConfirmationLink(confirmation) {
		def subscription = PendingEmailConfirmation.findByEmailAddress(confirmation.email)
		if (subscription) {
			//We can do any custome handling here for confirmation link
			//Send user to reset pwd page where he can set new password
			return [controller:'emailConfirmation', action:'resetpwd']
		}
	}


	/*
	 * THis is to redirect to custom view when user tries to access invalid confirmation link
	 */
	@Listener(namespace='plugin.emailConfirmation', topic='invalid')
	def invalidUserConfirmedSubscription(info) {
		log.warn "Invalid confirmation received for token [${info.token}]"
		return [controller:'emailConfirmation', action:'oops']
	}

	// We will need to talk about this.
	@Listener(namespace='plugin.emailConfirmation', topic='timeout')
	void userConfirmationTimeout(confirmation) {
		log.warn "Confirmation timed out for address [${confirmation.email}] with app id [${confirmation.id}]"
		// You might send an email here to your support team
	}
}
