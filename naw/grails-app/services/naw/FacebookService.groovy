package naw

import java.util.Properties;

import groovy.text.SimpleTemplateEngine

import org.apache.commons.logging.LogFactory
import org.pac4j.oauth.profile.OAuth20Profile;

class FacebookService {

	private static final log = LogFactory.getLog(this);
	
	final static FACEBOOK_BASE_URL = "https://graph.facebook.com"
	final static VALIDATE_TOKEN_URL = '/debug_token?input_token=$inputToken&access_token=$accessToken'
	final static GET_USER_INFO = '/me?access_token=$accessToken'
	
	def emailDirectService;
	def grailsApplication;
	def rest = new RestBuilder();
	
	public getFacebookUser(OAuth20Profile userProfile) {
		def endPoint = FACEBOOK_BASE_URL + GET_USER_INFO;
		def binding = ["accessToken": userProfile.attributes.access_token];
		def engine = new SimpleTemplateEngine()
		def template = engine.createTemplate(endPoint).make(binding)
		def fbResponse = rest.get(template.toString());
		def fbUser = fbResponse.json;
		return fbUser;
	}
	
	public isValidToken(token, userId) {
		def endPoint = FACEBOOK_BASE_URL + VALIDATE_TOKEN_URL;
		Properties pros = grailsApplication.getConfig().toProperties();
		String appToken = (String) pros.get("grails.plugin.springsecurity.facebook.appToken");
		def binding = ["inputToken": token, "accessToken": appToken];
		def engine = new SimpleTemplateEngine()
		def template = engine.createTemplate(endPoint).make(binding)		
		def fbResponse = rest.get(template.toString());
		def data = fbResponse.json.data;
		log.debug data;
		if(data.is_valid && String.valueOf(data.user_id) == userId){
			return true;
		}
		return false;
	}
	
}
