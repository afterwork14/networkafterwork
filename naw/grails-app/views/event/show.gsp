<%@ page import="naw.Event" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<r:require module="event"/>	
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
           <span class="divider"><i class="fa fa-angle-right"></i></span>
       </li>
       <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
<div class="col-md-12">
	<div class="row">
		  <div class="col-md-6">
		  <div class="row">
	  			<div class="col-md-12">
	            <div class="box box-gray"><div class="box-title"><h3><i class="fa fa-glass"></i>Event</h3></div>
                <div class="box-content">
                    <dl>
                        <dd><h3><g:fieldValue bean="${eventInstance}" field="name"/></h3></dd>
                        <dd><h5><g:formatDate format="MM/dd/yyyy" date="${eventInstance.startTime}"/>
                          		<g:formatDate format="HH:mm:ss" date="${eventInstance.startTime}"/> - 
			   					<g:formatDate format="HH:mm:ss" date="${eventInstance.endTime}"/></h5></dd>
			   			<dd><h5><g:fieldValue bean="${eventInstance}" field="capacity"/> spots</h5></dd>
			   			<dd><h5>Manager: <g:fieldValue bean="${eventInstance.manager}" field="firstname"/> 
			   							 <g:fieldValue bean="${eventInstance.manager}" field="lastname"/></h5></dd>
			   			<dd><h5>Photo bucket: ${eventInstance.s3Bucket}</h5></dd>
				   	</dl>
                </div>
                </div>
	            </div>
	            <div class="col-md-12">
				<div class="panel panel-success">
						<div class="panel-heading"><h4 class="panel-title">Payment Summary</h4></div>
						<div class="panel-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Source</th>
									<th>Orders</th>
									<th>Total</th>
									<th>Paid</th>
									<th>Unpaid</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Website</td>
									<td><g:fieldValue bean="${eventVO}" field="totalWebsiteOrders"/></td>
									<td><g:formatNumber number="${eventVO.totalWebsiteAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalWebsitePaidAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalWebsiteUnpaidAmount}" type="currency" currencyCode="USD" /></td>
								</tr>
								<tr>
									<td>Meetup</td>
									<td><g:fieldValue bean="${eventVO}" field="totalMeetupOrders"/></td>
									<td><g:formatNumber number="${eventVO.totalMeetupAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalMeetupPaidAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalMeetupUnpaidAmount}" type="currency" currencyCode="USD" /></td>
								</tr>
								<tr>
									<td>Eventbrite</td>
									<td><g:fieldValue bean="${eventVO}" field="totalEventbriteOrders"/></td>
									<td><g:formatNumber number="${eventVO.totalEventbriteAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalEventbritePaidAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalEventbriteUnpaidAmount}" type="currency" currencyCode="USD" /></td>
								</tr>
								<tr>
									<td>At Door</td>
									<td><g:fieldValue bean="${eventVO}" field="totalAtDoorOrders"/></td>
									<td><g:formatNumber number="${eventVO.totalAtDoorAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalAtDoorPaidAmount}" type="currency" currencyCode="USD" /></td>
									<td><g:formatNumber number="${eventVO.totalAtDoorUnpaidAmount}" type="currency" currencyCode="USD" /></td>
								</tr>
								<tr>
									<td><b>Total</b></td>
									<td><g:fieldValue bean="${eventVO}" field="totalOrders"/></td>
									<td><b><g:formatNumber number="${eventVO.totalAmount}" type="currency" currencyCode="USD" /></b></td>
									<td><b><g:formatNumber number="${eventVO.totalPaidAmount}" type="currency" currencyCode="USD" /></b></td>
									<td><b><g:formatNumber number="${eventVO.totalUnpaidAmount}" type="currency" currencyCode="USD" /></b></td>
								</tr>
							</tbody>
						</table>
						</div>
				</div>
				</div>
	      </div>   
          </div>
          
     <div class="col-md-6">
          <div class="row">
	          	 <div class="col-md-12">
		            <div class="box"><div class="box-title"><h3><i class="fa fa-location-arrow"></i>Location</h3></div>
		            <div class="box-content">
	                    <dl>
	                          <dd><h3><g:fieldValue bean="${eventInstance.venue}" field="name"/></h3></dd>
							  <dd><h5><g:fieldValue bean="${eventInstance.venue}" field="address1"/></h5></dd>
							  <dd><h5><g:fieldValue bean="${eventInstance.venue}" field="address2"/></h5></dd>
							  <dd><h5><g:fieldValue bean="${eventInstance.venue.city}" field="name"/>
						      <g:fieldValue bean="${eventInstance.venue}" field="postalcode"/></h5></dd>
							  <dd><h5><g:fieldValue bean="${eventInstance.venue.state}" field="name"/></h5></dd>
							  <dd><h5><g:fieldValue bean="${eventInstance.venue.country}" field="name"/></h5></dd>
						 </dl>
		            </div>
		            </div>
		         </div>
	   			 <div class="col-md-4">
			         <div class="tile tile-red">
		                 <div class="content">
		                 <g:if test="${eventVO.upcoming == true}">
		                     <p class="big">${eventVO.daysToGo}</p>
		                     <p class="title">Days to Go</p>
		                 </g:if>
		                 <g:if test="${eventVO.today == true}">
		                     <p class="big">Today</p>
		                 </g:if>
		                 <g:if test="${eventVO.past == true}">
		                     <p class="big">${eventVO.daysToGo}</p>
		                     <p class="title">Days Ago</p>
		                 </g:if>
		                 </div>
			         </div>
		         </div>
		         <div class="col-md-4">
			         <div class="tile tile-dark-blue">
		                 <div class="content">
		                     <p class="big">${eventInstance.orders.size()}</p>
		                     <p class="title">Orders</p>
		                 </div>
			         </div>
		         </div>
		         <div class="col-md-4">
			         <div class="tile tile-magenta">
		                 <div class="content">
		                     <p class="big">${eventVO.totalRsvps}</p>
		                     <p class="title">RSVPs</p>
		                 </div>
			         </div>
		         </div>
		         <div class="col-md-4">
			         <div class="tile tile-orange">
		                 <div class="content">
		                     <p class="big">${eventVO.newCustomers}</p>
		                     <p class="title">New Customers</p>
		                 </div>
			         </div>
		         </div>
		         <div class="col-md-4">
			         <div class="tile tile-pink">
		                 <div class="content">
		                     <p class="big">${eventInstance.comments.size()}</p>
		                     <p class="title">Comments</p>
		                 </div>
			         </div>
		         </div>
		         <div class="col-md-4">
			         <div class="tile tile-light-blue">
		                 <div class="content">
		                     <p class="big">${eventVO.attendees}</p>
		                     <p class="title">Attendees</p>
		                 </div>
			         </div>
	         	</div>
          </div>
          </div>
  </div>

	
  <div class="row">
  <div class="col-md-12">
	    <g:form class="form-horizontal">
			<g:hiddenField name="id" value="${eventInstance?.id}" />
			<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
			<g:actionSubmit class="btn btn-primary" value="History" action="history" />
		</g:form>
		<br/>
   </div>
   </div>
   
   <div class="row">
	<div class="col-md-12">
	<div class="panel panel-success">
		<div class="panel-heading"><h4 class="panel-title">Orders</h4></div>
		<div class="panel-body">
		<table class="table table-striped">
				<thead>
					<tr>
						<th>Order #</th>
						<th>Source</th>
						<th>Name</th>
						<th>Order Amount</th>
						<th>Paid Amount</th>
						<th>Rsvps</th>
						<th>Email</th>
						<g:if test="${eventVO.past == true || eventVO.today == true}">
						     <th>Entry Time</th>
						     <th>Entry Type</th>
						</g:if>
					</tr>
				</thead>
				<tbody>
					<g:each in="${eventInstance.orders}" var="order">
						<g:if test="${order.rsvps.size() == 1}">
							<tr>
								<td>${fieldValue(bean: order, field: "id")}</td>
								<td>${fieldValue(bean: order, field: "source")}</td>
								<td>${order.user?.firstname} ${order.user?.lastname}</td>
								<td><g:formatNumber number="${order.orderTotal}" type="currency" currencyCode="USD" /></td>
								<td><g:formatNumber number="${order.paidAmount}" type="currency" currencyCode="USD" /></td>
								<g:each in="${order.rsvps}" var="rsvp">
									<g:if test="${rsvp.user != null}">
										<td>${rsvp.user.firstname} ${rsvp.user.lastname}</td>
										<td><g:link controller="user" action="show" id="${rsvp.user.id}">${fieldValue(bean: rsvp.user, field: "email")}</g:link></td>
									</g:if>
									<g:if test="${rsvp.prospectiveUser != null}">
										<td>${fieldValue(bean: rsvp.prospectiveUser, field: "name")}</td>
										<td>${fieldValue(bean: rsvp.prospectiveUser, field: "email")}</td>
									</g:if>
									<g:if test="${eventVO.past == true || eventVO.today == true}">
										 <g:if test="${rsvp.entryTime != null}">
									     	<td>${fieldValue(bean: rsvp, field: "entryTime")}</td>
									     </g:if>
									     <td>${fieldValue(bean: rsvp, field: "entryType")}</td>
									</g:if>
								</g:each>
							</tr>
						</g:if>
						<g:else>
							<tr>
								<td rowspan="${order.rsvps.size() + 1}">${fieldValue(bean: order, field: "id")}</td>
								<td rowspan="${order.rsvps.size() + 1}">${fieldValue(bean: order, field: "source")}</td>
								<td rowspan="${order.rsvps.size() + 1}">${order.user.firstname} ${order.user.lastname}</td>
								<td rowspan="${order.rsvps.size() + 1}"><g:formatNumber number="${order.orderTotal}" type="currency" currencyCode="USD" /></td>
								<td rowspan="${order.rsvps.size() + 1}"><g:formatNumber number="${order.paidAmount}" type="currency" currencyCode="USD" /></td>
							</tr>
							<g:each in="${order.rsvps}" var="rsvp">
								<tr>
									<g:if test="${rsvp.user != null}">
										<td>${rsvp.user.firstname} ${rsvp.user.lastname}</td>
										<td><g:link controller="user" action="show" id="${rsvp.user.id}">${fieldValue(bean: rsvp.user, field: "email")}</g:link></td>
									</g:if>
									<g:if test="${rsvp.prospectiveUser != null}">
										<td>${fieldValue(bean: rsvp.prospectiveUser, field: "name")}</td>
										<td>${fieldValue(bean: rsvp.prospectiveUser, field: "email")}</td>
									</g:if>
									<g:if test="${eventVO.past == true || eventVO.today == true}">
									     <g:if test="${rsvp.entryTime != null}">
								     		<td>${fieldValue(bean: rsvp, field: "entryTime")}</td>
								     	 </g:if>
									     <td>${fieldValue(bean: rsvp, field: "entryType")}</td>
									</g:if>
								</tr>
							</g:each>
						</g:else>
					</g:each>
				</tbody>
			</table>
	</div>
	</div>
	</div>
	</div>
			
</div>
</div>
     
</body>
</html>
