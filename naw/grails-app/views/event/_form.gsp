<%@ page import="naw.Event" %>
<%@ page import="naw.City" %>
<%@ page import="naw.Venue" %>
<%@ page import="naw.PriceModifier" %>
<%@ page import="naw.User" %>
<%@ page import="naw.Role" %>
<%@ page import="naw.UserRole" %>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="name">
		<g:message code="event.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
		<g:textField class="form-control input-sm" name="name" required="" placeholder="Name of the Event" value="${eventInstance?.name}"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="city">
		<g:message code="event.city.label" default="City" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<g:select class="form-control input-sm" 
		name="city" 
		from="${City.list()}"
		value="${eventInstance.venue?.city?.id}"
		optionKey="id"
		optionValue="name"
		noSelection="['':'-Choose city for the event-']"
		valueMessagePrefix="event.city"
		onchange="${remoteFunction(
				  controller: 'venue',
				  action: 'venuesByCity',
                  onSuccess: 'populateVenues(data)',
                  params: '\'cityId=\' + this.value')}"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="venue">
		<g:message code="event.venue.label" default="Venue" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<g:select class="form-control input-sm" 
		name="venue" 
		id="venue"
		from="${Venue.list()}"
		value="${eventInstance.venue?.id}"
		optionKey="id"
		optionValue="${{it.name +' - '+ it.address1}}"
		noSelection="['':'-Choose venue for the event-']"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="manager">
		<g:message code="event.manager.label" default="Manager" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<g:select class="form-control input-sm" 
		name="manager" 
		from="${UserRole.executeQuery("select distinct userrole.user from UserRole userrole " +
                     "where userrole.role.authority = ? ", ['ROLE_MANAGER'])}"
		value="${eventInstance.manager?.id}"
		optionKey="id"
		optionValue="${{it.firstname +' '+ it.lastname}}"
		noSelection="['':'-Choose manager for the event-']"
		valueMessagePrefix="event.manager"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="price">
		<g:message code="event.price.label" default="Price" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<div class="input-group">
		<span class="input-group-addon">$</span>
		<g:field class="form-control input-sm" name="price" placeholder="Regular Price" value="${fieldValue(bean: eventInstance, field: 'price')}" 
		type="number" 
		step="0.01" currencyCode="USD" required=""/>
 	</div>
 	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="event.pricemodifiers.label" default="Price Modifiers" />
		<g:message code="event.pricemodifiers.label" default="(CTRL+Click)" />
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<g:select class="form-control" 
		name="pricemodifiers" 
		from="${PriceModifier.list()}"
		value="${eventInstance.priceModifiers?.id}"
		optionKey="id"
		optionValue="${{it.value +'  '+ it.type +'  On  '+ it.level + '  -  ' + it.description}}"
		valueMessagePrefix="event.pricemodifiers"
		multiple="true"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="capacity">
		<g:message code="event.capacity.label" default="Capacity" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	 <g:field class="form-control input-sm"  type="number" min="1" name="capacity" placeholder="Maximum spots available" required="" 
	 value="${eventInstance?.capacity}"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="startTime">
		<g:message code="event.startTime.label" default="Start Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		<input class="form-control date-picker" name="sDate" type="text"  required placeholder="Select start date" 
		value="<g:formatDate format="MM/dd/yyyy" date="${eventInstance?.startTime}"/>" onchange="setEndDate()"/>
		<a class="input-group-addon" href="#"><i class="fa fa-clock-o"></i></a>
		<g:if test="${industryInstance?.startTime}">
			<input class="form-control timepicker-24" name="sTime" type="text" required placeholder="Select start time"
			value="<g:formatDate format="HH:mm:ss" date="${eventInstance?.startTime}"/>"/>
		</g:if>
		<g:else>
     		<input class="form-control timepicker-24" name="sTime" type="text" required placeholder="Select start time" value="18:00:00"/>
		</g:else>
	</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="endTime">
		<g:message code="event.endTime.label" default="End Time" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-6 controls">
	<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
		<input class="form-control date-picker" name="eDate" type="text"  required placeholder="Select end date" 
		value="<g:formatDate format="MM/dd/yyyy" date="${eventInstance?.endTime}"/>" onchange="setStartDate()"/>
		<a class="input-group-addon" href="#"><i class="fa fa-clock-o"></i></a>
		<g:if test="${industryInstance?.endTime}">
			<input class="form-control timepicker-24" name="eTime" type="text" required placeholder="Select end time" 
			value="<g:formatDate format="HH:mm:ss" date="${eventInstance?.endTime}"/>"/>
		</g:if>
		<g:else>
			<input class="form-control timepicker-24" name="eTime" type="text" required placeholder="Select end time" value="21:00:00"/>
		</g:else>
	</div>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="document">
		<g:message code="event.document.label" default="Venue Contract" />
	</label>
	<div class="col-sm-8 col-lg-6 controls"><input type="file" name="document" class="form-control" /></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="meetup">
		<g:message code="event.meetup.label" default="Meetup Draft" />
	</label>
	<div class="col-sm-8 col-lg-6 controls"><g:checkBox name="meetup"/></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="facebook">
		<g:message code="event.facebook.label" default="Facebook Draft" />
	</label>
	<div class="col-sm-8 col-lg-6 controls"><g:checkBox name="facebook"/></div>
</div>

<div class="form-group">
	<label class="col-sm-2 col-lg-2 control-label" for="description">
		<g:message code="event.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-10 col-lg-6 controls">
		<textarea class="form-control col-md-12 wysihtml5" name="description" rows="15" required>${eventInstance?.description}</textarea>
	</div>
</div>
