<%@ page import="naw.Event"%>
<%@ page import="naw.City" %>
<%@ page import="naw.Venue" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Excel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'event.name.label', default: 'Name')}" />
								<th>Manager</th>
								<g:sortableColumn class="sort-desc sort-asc" property="venue" title="${message(code: 'event.venue.label', default: 'Venue')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="city" title="${message(code: 'event.city.label', default: 'City')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="date" title="${message(code: 'event.startTime.label', default: 'Date')}" />
								<th>Time</th>
								<g:sortableColumn class="sort-desc sort-asc" property="capacity" title="${message(code: 'event.capacity.label', default: 'Capacity')}" />
								<th>Orders</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${eventInstanceList}" status="i" var="eventInstance">
								<tr>
									<td><g:link action="show" id="${eventInstance.id}">${fieldValue(bean: eventInstance, field: "name")}</g:link></td>
									<td><g:link controller="user" action="show" id="${eventInstance.manager?.id}">
										${fieldValue(bean: eventInstance.manager, field: "firstname")}
										${fieldValue(bean: eventInstance.manager, field: "lastname")}</g:link>
									</td>
									<td><g:link controller="venue" action="show" id="${eventInstance.venue.id}">
										${fieldValue(bean: eventInstance.venue, field: "name")}</g:link>
									</td>
									<td><g:link controller="city" action="show" id="${eventInstance.venue.city.id}">
										${fieldValue(bean: eventInstance.venue.city, field: "name")}</g:link>
									</td>
									<td><g:formatDate format="MM/dd/yyyy" date="${eventInstance.startTime}"/></td>
									<td><g:formatDate format="HH:mm:ss" date="${eventInstance.startTime}"/> - 
										<g:formatDate format="HH:mm:ss" date="${eventInstance.endTime}"/></td>
									<td>${fieldValue(bean: eventInstance, field: "capacity")}</td>
									<td>${eventInstance.orders.size()}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${eventInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
               
                <div class="clearfix"></div>
				<div class="box-content">
				<g:form controller="event" action="list" class="form-horizontal">
					<div class="row">
	  				    <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="city"><g:message code="city.label" default="City" /></label>
							<div class="col-sm-8 col-lg-8 controls">
									<g:select class="form-control" name="city" 
										from="${City.list()}"
										optionKey="id"
										optionValue="name"
										value="${params?.city}"
										noSelection="${['':'Select One...']}"
										valueMessagePrefix="city"/></div>
							</div>
						</div>
						<div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="venue"><g:message code="venu.label" default="Venue" /></label>
							<div class="col-sm-8 col-lg-8 controls">
									<g:select class="form-control" name="venue" 
										from="${Venue.list()}"
										optionKey="id"
										optionValue="name"
										value="${params?.venue}"
										noSelection="${['':'Select One...']}"
										valueMessagePrefix="venue"/></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4 col-lg-8 col-lg-offset-4">
								<g:submitButton name="search" class="btn btn-primary" value="${message(code: 'default.button.search.label', default: 'Search')}" />
								<g:submitButton name="reset" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Reset')}" />
							</div>
						</div>
					</div>
				</g:form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
