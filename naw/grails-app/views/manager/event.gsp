<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<r:require module="jquery" />
	<r:require module="mobile" />
	<r:layoutResources />
</head>
<g:javascript>
  var checkinURL = "${createLink(controller:'manager',action:'checkin')}"
  var undocheckinURL = "${createLink(controller:'manager',action:'undocheckin')}"
  var newOrderURL = "${createLink(controller:'manager',action:'order')}"
</g:javascript>
<script>
$(document).on('pageinit', '#newpage', function(){ 
    $(document).on('click', '#submitOrder', function() {
        if($('#email').val().length > 0 || $('#phone').val().length > 0){
                $.ajax({
                    url: newOrderURL,
                    data: $('#newOrderForm').serialize(),
                    type: 'post',                  
                    async: 'true',
                    dataType: 'json',
                    beforeSend: function() {
                    	$.mobile.loading('show');
                    },
                    complete: function() {
                    	$.mobile.loading('hide');
                    },
                    success: function (result) {
                        if(result.success) {
                        	$('#newOrderForm').each(function(){
                        	    this.reset();
                        	});
                            alert("Order submitted :" + result.orderNumber);
                        }
                    },
                    error: function (request,error) {           
                        alert('Network error has occurred please try again!');
                    }
                });                  
        } else {
            alert('Please fill all necessary fields');
        }          
        return false;
    });   
});
</script>

<body>

<div data-role="page" id="checkinpage">
	<div data-role="panel" id="myprofile" data-position="right" data-display="overlay" data-theme="b" data-position-fixed="true">
		<ul data-role="listview">
            <li><a href="#"><sec:username /></a></li>
            <li><g:link controller="logout" data-ajax="false">Logout</g:link></li>
        </ul>
	</div>
	<div data-role="header" data-position="fixed">
		<g:link controller="manager" action="list"
		class="ui-btn ui-btn-left ui-alt-icon ui-nodisc-icon ui-corner-all ui-btn-icon-notext ui-icon-carat-l">Back</g:link>
		<h1>${event.name}</h1>
		<a href="#myprofile" data-icon="bars" data-iconpos="notext" 
		class="ui-link ui-btn-right ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all">Menu</a>
	</div>
	<div data-role="main" class="ui-content">
		<form class="ui-filterable">
		<input id="myFilter" data-type="search" placeholder="Search for names.."></form>
		<ul data-role="listview" data-filter="true" data-input="#myFilter" data-autodividers="true" data-inset="true">
			<g:each in="${rsvps}" var="rsvp">
				<g:if test="${rsvp.paid}">
					<g:if test="${rsvp.checkedin}">
						<li><a href="#${rsvp.rsvpId}">
								<font id="rsvp-${rsvp.rsvpId}" style="text-decoration: line-through">${rsvp.label}</font>
							</a>
							<p class="ui-li-aside"><g:formatDate format="hh:mm:ss aa" date="${rsvp.entryTime}"/></p>
						</li>
					</g:if>
					<g:else>
						<li><a href="#${rsvp.rsvpId}"><font id="rsvp-${rsvp.rsvpId}" style="color: green">${rsvp.label}</font></a></li>
					</g:else>	
				</g:if>
				<g:else>
					<g:if test="${rsvp.checkedin}">
						<li><a href="#${rsvp.rsvpId}">
								<font id="rsvp-${rsvp.rsvpId}" style="text-decoration: line-through">${rsvp.label}</font>
							</a>
							<p class="ui-li-aside"><g:formatDate format="hh:mm:ss aa" date="${rsvp.entryTime}"/></p>
						</li>
					</g:if>
					<g:else>
						<li><a href="#${rsvp.rsvpId}"><font id="rsvp-${rsvp.rsvpId}" style="color: red">${rsvp.label}</font></a></li>
					</g:else>
				</g:else>
			</g:each>
		</ul>
		<g:each in="${rsvps}" var="rsvp">
			<div data-role="panel" id="${rsvp.rsvpId}" data-position="right" data-display="overlay" data-theme="b" data-position-fixed="true"> 
			    <h2 style="text-transform: capitalize;">${rsvp.label}<br/>${rsvp.company}</h2>
			    <p style="text-transform: capitalize;"><b>Ordered By</b><br/>${rsvp.orderedBy}</p>
			    <p><b>Order Time</b><br/>${rsvp.orderTime}</p>
			    <p><b>Order Number</b><br/>${rsvp.orderId}</p>
			    <p><b>Rsvp Number</b><br/>${rsvp.rsvpId}</p>
			    <g:if test="${rsvp.paid}">
					<p><b><font color="green">PAID</font></b></p>
				</g:if>
				<g:else>
					<p><b><font color="red">UNPAID</font></b></p>
				</g:else>
			    <a href="#checkinpage" data-rel="close" class="ui-btn ui-corner-all ui-btn-inline">Close</a>
			    <g:if test="${rsvp.checkedin}">
			   		<a id="button-checkin-${rsvp.rsvpId}" href="javascript:undocheckin(${rsvp.rsvpId})" class="ui-btn ui-corner-all ui-btn-inline">Undo</a>
				</g:if>
				<g:else>
					<a id="button-checkin-${rsvp.rsvpId}" href="javascript:checkin(${rsvp.rsvpId})" class="ui-btn ui-corner-all ui-btn-inline">Check-in</a>
				</g:else>
			</div> 
		</g:each>
	</div>
	<div data-role="footer" data-position="fixed">
		<div data-role="navbar">
			<ul>
				<li><a href="#checkinpage" data-icon="home" data-transition="pop" class="ui-btn-active">RSVPs</a></li>
				<li><a href="#newpage" data-icon="plus" data-transition="pop">New</a></li>
				<li><a href="#infopage" data-icon="info" data-transition="pop">Information</a></li>
				<li><a href="#surveypage" data-icon="bars" data-transition="pop">Survey</a></li>
			</ul>
		</div>
	</div>
</div>

<div data-role="page" id="newpage">
	<div data-role="header" data-position="fixed">
		<g:link controller="manager" action="list" 
		class="ui-btn ui-btn-left ui-alt-icon ui-nodisc-icon ui-corner-all ui-btn-icon-notext ui-icon-carat-l">Back</g:link>
		<h1>${event.name}</h1>
		<a href="#myprofile" data-icon="bars" data-iconpos="notext" 
		class="ui-link ui-btn-right ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all">Menu</a>
	</div>
	<div data-role="main" class="ui-content" class="ui-body ui-body-a ui-corner-all">
		<form id="newOrderForm" data-ajax="false">
			<input type="hidden" name="eventId" value="${event.id}">
		    <input type="email" data-clear-btn="true" name="email" id="email" placeholder="E-mail" value="">
		    <input type="tel" data-clear-btn="true" name="phone" id="phone" placeholder="Phone" value="">
     		<select name="amount" id="amount">
			    <option value="10">10.00</option>
			    <option value="12">12.00</option>
			    <option value="15">15.00</option>
			    <option value="20">20.00</option>
			</select>
     		<input type="button" id="submitOrder" value="Submit">
		</form>
	</div>
	<div data-role="footer" data-position="fixed">
		<div data-role="navbar">
			<ul>
				<li><a href="#checkinpage" data-icon="home" data-transition="pop">RSVPs</a></li>
				<li><a href="#newpage" data-icon="plus" data-transition="pop" class="ui-btn-active">New</a></li>
				<li><a href="#infopage" data-icon="info" data-transition="pop">Information</a></li>
				<li><a href="#surveypage" data-icon="bars" data-transition="pop">Survey</a></li>
			</ul>
		</div>
	</div>
</div>

<div data-role="page" id="infopage">
	<div data-role="header" data-position="fixed">
		<g:link controller="manager" action="list"
		class="ui-btn ui-btn-left ui-alt-icon ui-nodisc-icon ui-corner-all ui-btn-icon-notext ui-icon-carat-l">Back</g:link>
		<h1>${event.name}</h1>
		<a href="#myprofile" data-icon="bars" data-iconpos="notext" 
		class="ui-link ui-btn-right ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all">Menu</a>
	</div>
	<div data-role="main" class="ui-content">
		<h1>Venue will serve drinks for 1 hour</h1>
	</div>
	<div data-role="footer" data-position="fixed">
		<div data-role="navbar">
			<ul>
				<li><a href="#checkinpage" data-icon="home" data-transition="pop">RSVPs</a></li>
				<li><a href="#newpage" data-icon="plus" data-transition="pop">New</a></li>
				<li><a href="#infopage" data-icon="info" data-transition="pop" class="ui-btn-active">Information</a></li>
				<li><a href="#surveypage" data-icon="bars" data-transition="pop">Survey</a></li>
			</ul>
		</div>
	</div>
</div>

<div data-role="page" id="surveypage">
	<div data-role="header" data-position="fixed">
		<g:link controller="manager" action="list"
		class="ui-btn ui-btn-left ui-alt-icon ui-nodisc-icon ui-corner-all ui-btn-icon-notext ui-icon-carat-l">Back</g:link>
		<h1>${event.name}</h1>
		<a href="#myprofile" data-icon="bars" data-iconpos="notext" 
		class="ui-link ui-btn-right ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all">Menu</a>
	</div>
	<div data-role="main" class="ui-content">
		<h1>How was the event?</h1>
	</div>
	<div data-role="footer" data-position="fixed">
		<div data-role="navbar">
			<ul>
				<li><a href="#checkinpage" data-icon="home" data-transition="pop">RSVPs</a></li>
				<li><a href="#newpage" data-icon="plus" data-transition="pop">New</a></li>
				<li><a href="#infopage" data-icon="info" data-transition="pop">Information</a></li>
				<li><a href="#surveypage" data-icon="bars" data-transition="pop" class="ui-btn-active">Survey</a></li>
			</ul>
		</div>
	</div>
</div>

<r:layoutResources />
</body>
</html>
