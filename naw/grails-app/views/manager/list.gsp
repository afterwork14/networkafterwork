<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<r:require module="jquery" />
	<r:require module="mobile" />
	<r:layoutResources />
</head>
<body>
<div data-role="page" id="checkinpage">

	<div data-role="panel" id="myprofile" data-position="right" data-display="overlay" data-theme="b" data-position-fixed="true">
		<ul data-role="listview">
            <li><a href="#"><sec:username /></a></li>
            <li><g:link controller="logout" data-ajax="false">Logout</g:link></li>
        </ul>
	</div>
	
	<div data-role="header" data-position="fixed">
		<h1>My Events</h1>
		<a href="#myprofile" data-icon="bars" data-iconpos="notext" 
		class="ui-link ui-btn-right ui-btn ui-icon-bars ui-btn-icon-notext ui-shadow ui-corner-all">Menu</a>
	</div>
	<div data-role="main" class="ui-content">
		<ul data-role="listview" data-inset="true">
			<g:each in="${eventInstanceList}" var="eventInstance">
			    <li>
				    <g:link action="event" id="${eventInstance.id}" data-ajax="false">
					    <h2>${fieldValue(bean: eventInstance, field: "name")}</h2>
					    <p>${fieldValue(bean: eventInstance.venue.city, field: "name")},
					   		<g:formatDate format="MM/dd/yyyy" date="${eventInstance.startTime}"/>
							<g:formatDate format="HH:mm:ss" date="${eventInstance.startTime}"/> - 
							<g:formatDate format="HH:mm:ss" date="${eventInstance.endTime}"/> 
					    </p>
				    </g:link>
			    </li>
			 </g:each>
		</ul>
	</div>
	<div data-role="footer" data-position="fixed">
		<h1>&copy; Network After Work</h1>
	</div>
</div>

<r:layoutResources />
</body>
</html>
