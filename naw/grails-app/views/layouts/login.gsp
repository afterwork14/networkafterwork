<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Network After Work"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<r:require module="jquery"/>
		<r:require module="core"/>
		<r:require module="flaty"/>
		<g:if env='prod'>
		     <r:use module='prod'/>
		</g:if>
		<g:else>
		     <r:use module='test'/> 
		</g:else>
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body class="login-page">
	
		<!-- BEGIN Main Content -->
		<g:layoutBody/>
		<!-- END Main Content -->
		
	<g:javascript library="application"/>
	<r:layoutResources />
	</body>
</html>
