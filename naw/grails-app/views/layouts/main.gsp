<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Network After Work"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<r:require module="jquery"/>
		<r:require module="core"/>
		<r:require module="flaty"/>
		<r:require module="export"/>
		<g:if env='prod'>
		     <r:use module='prod'/>
		</g:if>
		<g:else>
		     <r:use module='test'/> 
		</g:else>
		<g:layoutHead/>
		<r:layoutResources />
	</head>
	<body>
		
<!-- BEGIN topbar -->
<div id="navbar" class="navbar">
	<button type="button" class="navbar-toggle navbar-btn collapsed"
		data-toggle="collapse" data-target="#sidebar">
		<span class="fa fa-bars"></span>
	</button>
	<a class="navbar-brand" href="#"> <small> <i
			class="fa fa-desktop"></i> Network After Work Admin
	</small>
	</a>
	<!-- BEGIN Navbar Buttons -->
	<sec:ifLoggedIn>
	<ul class="nav flaty-nav pull-right">
		<li class="user-profile">
			<a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
				<img class="nav-user-photo" src="${createLink(controller:'user', action:'picture', id:sec.loggedInUserInfo(field:"id"))}" alt="<sec:username /> Photo" />
				<span class="hhh" id="user_info"> <sec:username /> </span> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-navbar" id="user_menu">
				<li><a href="#"> <i class="fa fa-cog"></i>Account Settings</a></li>
				<li><a href="#"> <i class="fa fa-user"></i>Edit Profile</a></li>
				<li class="divider"></li>
				<li><g:link controller="logout"><i class="fa fa-off"></i>Logout</g:link></li>
			</ul>
		</li>
	</ul>
	</sec:ifLoggedIn>
	<!-- END Navbar Buttons -->
</div>
<!-- END topbar -->

<!-- BEGIN Container -->
<div class="container" id="main-container">

	<!-- BEGIN Sidebar -->
	<sec:ifLoggedIn>
	<div id="sidebar" class="navbar-collapse collapse">
		<!-- BEGIN Navlist -->
		<ul class="nav nav-list">
			<li><br/></li>
			<sec:ifAllGranted roles="ROLE_ADMIN">
			<li>
				<form target="#" method="GET" class="search-form">
					<span class="search-pan">
						<button type="submit">
							<i class="fa fa-search"></i>
						</button> <input type="text" name="search" placeholder="Search ..."
						autocomplete="off" />
					</span>
				</form>
			</li>
			</sec:ifAllGranted>
			<li><g:link controller="dashboard" action="home"><i class="fa fa-dashboard"></i><span>Dashboard</span></g:link></li>
			<sec:ifAllGranted roles="ROLE_MANAGER">
				<li><g:link controller="manager" action="list"><i class="fa fa-dashboard"></i><span>My Events</span></g:link></li>
			</sec:ifAllGranted>
			<sec:ifAllGranted roles="ROLE_ADMIN">
				<li><g:link controller="user"><i class="fa fa-users"></i><span>Users</span></g:link></li>
				<li><g:link controller="order"><i class="fa fa-credit-card"></i><span>Orders</span></g:link></li>
				<li><g:link controller="event"><i class="fa fa-glass"></i><span>Events</span></g:link></li>
				<li><g:link controller="venue"><i class="fa fa-location-arrow"></i><span>Venues</span></g:link></li>
	            <li><g:link controller="city"><i class="fa fa-thumb-tack"></i><span>Cities</span></g:link></li>
	            <li><g:link controller="state"><i class="fa fa-arrows"></i><span>States</span></g:link></li>
	            <li><g:link controller="country"><i class="fa fa-globe"></i><span>Countries</span></g:link></li>
				<li><g:link controller="report"><i class="fa fa-bar-chart-o"></i><span>Reports</span></g:link></li>
	            <li><g:link controller="role"><i class="fa fa-key"></i><span>Roles</span></g:link></li>
				<li><g:link controller="industry"><i class="fa fa-building-o"></i><span>Industries</span></g:link></li>  
				<li><g:link controller="priceModifier"><i class="fa fa-building-o"></i><span>Price Modifiers</span></g:link></li>   
			</sec:ifAllGranted> 
		</ul>
		<!-- END Navlist -->
	
		<!-- BEGIN Sidebar Collapse Button -->
		<div id="sidebar-collapse" class="visible-lg">
			<i class="fa fa-angle-double-left"></i>
		</div>
		<!-- END Sidebar Collapse Button -->
	</div>
	<!-- END Sidebar -->
</sec:ifLoggedIn>

	<!-- BEGIN Content -->
	<div id="main-content">
	
		<!-- BEGIN Main Content -->
		<g:layoutBody/>
		<!-- END Main Content -->
		
		<footer>
			<p>2014 © Network After Work. All Rights Reserved.</p>
		</footer>
		<a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i
			class="fa fa-chevron-up"></i></a>
		
	</div>
	<!-- END Content -->

</div>
<!-- END Container -->
		
<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
<g:javascript library="application"/>
<r:layoutResources />
</body>
</html>
