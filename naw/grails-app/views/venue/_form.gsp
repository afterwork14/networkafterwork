<%@ page import="naw.Venue" %>
<%@ page import="naw.City" %>
<%@ page import="naw.State" %>
<%@ page import="naw.Country" %>

<div class="col-md-7">
<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="name" required="" value="${venueInstance?.name}" /></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.address1.label" default="Address1" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="address1" required="" value="${venueInstance?.address1}"/></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.address2.label" default="Address2" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="address2" value="${venueInstance?.address2}"/></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.city.label" default="City" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls">
	<g:select class="form-control input-lg" name="city" 
	from="${City.list()}"
	optionKey="id"
	optionValue="name"
	value="${venueInstance.city?.id}"
	valueMessagePrefix="venue.city"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.postalcode.label" default="Postalcode" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="postalcode" required="" value="${venueInstance?.postalcode}"/></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.state.label" default="State" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls">
	<g:select class="form-control input-lg" name="state" 
	from="${State.list()}"
	optionKey="id"
	optionValue="name"
	value="${venueInstance.state?.id}"
	valueMessagePrefix="venue.state"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls">
	<g:select class="form-control input-lg" name="country" 
	from="${Country.list()}"
	optionKey="id"
	optionValue="name"
	value="${venueInstance.country?.id}"
	valueMessagePrefix="venue.country"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-4 control-label">
		<g:message code="venue.meetup.label" default="Meetup Draft" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:checkBox name="meetup"/></div>
</div>
</div>