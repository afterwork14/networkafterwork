<%@ page import="naw.Venue" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'venue.label', default: 'Venue')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>

<div class="row">
<div class="col-md-12">
<div class="box">
<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
<div class="box-content">
  	<g:if test="${flash.message}">
	 	<div class="alert alert-success">${flash.message}</div>
	</g:if>
    <div class="col-md-5">
    	<g:each in="${images}" var="image">
        	<img class="img-responsive img-thumbnail" src="${image.url}" width="400" alt="venue picture" />
        </g:each>
	</div>
	<div class="col-md-7">
	    <dl>
	        <g:if test="${venueInstance?.name}">
	       		<dt><g:message code="venue.name.label" default="Name" /></dt>
				<dd><g:fieldValue bean="${venueInstance}" field="name"/></dd>
			</g:if>
		</dl>
	    <dl>
	    	<dt><g:message code="venue.address.label" default="Address" /></dt>
			<dd><g:fieldValue bean="${venueInstance}" field="address1"/></dd>
			<dd><g:fieldValue bean="${venueInstance}" field="address2"/></dd>
			<dd><g:link controller="city" action="show" id="${venueInstance.city.id}">${fieldValue(bean: venueInstance.city, field: "name")}</g:link>
				${fieldValue(bean: venueInstance, field: "postalcode")}
			</dd>
			<dd><g:link controller="state" action="show" id="${venueInstance.state.id}">${fieldValue(bean: venueInstance.state, field: "name")}</g:link></dd>
			<dd><g:link controller="country" action="show" id="${venueInstance.country.id}">${fieldValue(bean: venueInstance.country, field: "name")}</g:link>
			</dd>
		</dl>
	    <dl>
			<g:if test="${venueInstance?.dateCreated}">
				<dt><g:message code="venue.dateCreated.label" default="Date Created" /></dt>
				<dd><g:formatDate date="${venueInstance?.dateCreated}" /></dd>
			</g:if>
		</dl>
	    <dl>
			<g:if test="${venueInstance?.lastUpdated}">
				<dt><g:message code="venue.lastUpdated.label" default="Last Updated" /></dt>
				<dd><g:formatDate date="${venueInstance?.lastUpdated}" /> by </dd>
			</g:if>
		</dl>
	</div>

	<div class="row">
	<div class="col-md-12">
	   <g:form class="form-horizontal">
			<g:hiddenField name="id" value="${venueInstance?.id}" />
			<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
			<g:actionSubmit class="btn btn-primary" value="History" action="history" />
		</g:form>
		<br/>
	</div>
	</div>
			
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-success">
		<div class="panel-heading"><h4 class="panel-title">Events</h4></div>
		<div class="panel-body">
				<g:if test="${venueInstance?.events}">
					<table class="table">
						<thead>
							<tr>
								<th><g:message code="venue.events.name.label" default="Name" /></th>
								<th><g:message code="venue.events.startTime.label" default="Date"/></th>
								<th><g:message code="venue.events.capacity.label" default="Capacity" /></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${venueInstance.events}" var="event">
								<tr>
									<td><g:link controller="event" action="show" id="${event.id}">${event.name}</g:link></td>
									<td><g:formatDate format="MM/dd/yyyy" date="${event.startTime}"/></td>
									<td><g:fieldValue bean="${event}" field="capacity"/></td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</g:if>
		</div>
		</div>
	</div>
	</div>
	
</div>
</div>

</div>
</div>

</body>
</html>
