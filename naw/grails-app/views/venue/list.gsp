<%@ page import="naw.Venue"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'venue.label', default: 'Venue')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
	</div>
	<div class="box-content">
		<g:if test="${flash.message}">
			<div class="alert alert-success">${flash.message}</div>
		</g:if>
		<div class="btn-toolbar pull-right clearfix">
			<div class="btn-group">
				<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
			</div>
			<div class="btn-group">
				<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
				<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
				<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
			</div>
		</div>
		<br /> <br />
		<div class="clearfix"></div>
		<div class="table-responsive">
			<table class="table table-advance">
				<thead>
					<tr>
						<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'venue.name.label', default: 'Name')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="address1" title="${message(code: 'venue.address1.label', default: 'Address1')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="address2" title="${message(code: 'venue.address2.label', default: 'Address2')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="city" title="${message(code: 'venue.city.name.label', default: 'City')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="postalcode" title="${message(code: 'venue.postalcode.label', default: 'Postal Code')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="state" title="${message(code: 'venue.state.label', default: 'State')}" />
						<g:sortableColumn class="sort-desc sort-asc" property="country" title="${message(code: 'venue.country.label', default: 'Country')}" />
						<th>Events</th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${venueInstanceList}" status="i" var="venueInstance">
						<tr>
							<td><g:link action="show" id="${venueInstance.id}">${fieldValue(bean: venueInstance, field: "name")}</g:link></td>
							<td>${fieldValue(bean: venueInstance, field: "address1")}</td>
							<td>${fieldValue(bean: venueInstance, field: "address2")}</td>
							<td><g:link controller="city" action="show" id="${venueInstance.city.id}">${fieldValue(bean: venueInstance.city, field: "name")}</g:link></td>
							<td>${fieldValue(bean: venueInstance, field: "postalcode")}</td>
							<td><g:link controller="state" action="show" id="${venueInstance.state.id}">${fieldValue(bean: venueInstance.state, field: "name")}</g:link></td>
							<td><g:link controller="country" action="show" id="${venueInstance.country.id}">${fieldValue(bean: venueInstance.country, field: "name")}</g:link></td>
							<td>${venueInstance.events.size()}</td>
						</tr>
					</g:each>
				</tbody>
			</table>
			<div class="text-center">
                   <ul class="pagination pagination-colory">
                       <li><g:paginate total="${venueInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
