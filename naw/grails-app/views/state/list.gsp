<%@ page import="naw.State"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'state.label', default: 'State')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'state.name.label', default: 'Name')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="abbreviation" title="${message(code: 'state.abbreviation.label', default: 'Abbreviation')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="country" title="${message(code: 'state.country.label', default: 'Country')}" />
								<th>Cities</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${stateInstanceList}" status="i" var="stateInstance">
								<tr>
									<td><g:link action="show" id="${stateInstance.id}">${fieldValue(bean: stateInstance, field: "name")}</g:link></td>
									<td><g:link action="show" id="${stateInstance.id}">${fieldValue(bean: stateInstance, field: "abbreviation")}</g:link></td>
									<td><g:link controller="country" action="show" id="${stateInstance.country.id}">${fieldValue(bean: stateInstance.country, field: "name")}</g:link></td>
									<td>${stateInstance.cities.size()}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${stateInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
