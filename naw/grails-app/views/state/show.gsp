<%@ page import="naw.State" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'state.label', default: 'State')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
		<div class="col-md-12">
		<div class="box">
		<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
		
			<div class="box-content">
	    	<g:if test="${flash.message}">
				 <div class="alert alert-success">${flash.message}</div>
			</g:if>
        
           	<div class="col-md-12">
             	<dl>
	                 <g:if test="${stateInstance?.name}">
	                 	<dt><g:message code="state.name.label" default="Name" /></dt>
						<dd><g:fieldValue bean="${stateInstance}" field="name"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
	                 <g:if test="${stateInstance?.abbreviation}">
	                 	<dt><g:message code="state.abbreviation.label" default="Abbreviation" /></dt>
						<dd><g:fieldValue bean="${stateInstance}" field="abbreviation"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
	                 <g:if test="${stateInstance?.country}">
	                 	<dt><g:message code="state.country.label" default="Country" /></dt>
						<dd><g:link controller="country" action="show" id="${stateInstance.country.id}">
								${fieldValue(bean: stateInstance.country, field: "name")}
							</g:link>
						</dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${stateInstance?.dateCreated}">
						<dt><g:message code="stateInstance.dateCreated.label" default="Date Created" /></dt>
						<dd><g:formatDate date="${stateInstance?.dateCreated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${stateInstance?.lastUpdated}">
						<dt><g:message code="stateInstance.lastUpdated.label" default="Last Updated" /></dt>
						<dd><g:formatDate date="${stateInstance?.lastUpdated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${stateInstance?.lastUpdated}">
						<dt><g:message code="stateInstance.lastUpdated.label" default="Last Change By" /></dt>
						<dd></dd>
					</g:if>
				</dl>
			</div>
			
			<div class="col-md-12">
			<g:if test="${stateInstance?.cities}">
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<th><g:message code="state.city.label" default="Cities" /></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${stateInstance.cities}" var="city">
								<tr>
									<td><g:link controller="city" action="show" id="${city.id}">${city.name}</g:link></td>
								</tr>
							</g:each>
						</tbody>
					</table>
                </div>
			</g:if>
			</div>
			
			<div class="col-md-12">
             	<g:form class="form-horizontal">
					<g:hiddenField name="id" value="${stateInstance?.id}" />
					<g:link class="btn btn-link" action="edit" id="${stateInstance?.id}">
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<g:link class="btn btn-link" action="history" id="${stateInstance?.id}">
						<g:message code="default.button.history.label" default="History" />
					</g:link>
				</g:form>
			</div>
     </div>
     </div>
     </div>
</div>
</body>
</html>
