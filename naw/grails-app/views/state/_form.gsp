<%@ page import="naw.State" %>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="state.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="name" required="" value="${stateInstance?.name}" /></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="state.abbreviation.label" default="Abbreviation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="abbreviation" required="" value="${stateInstance?.abbreviation}" /></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label"><g:message code="industry.active.label" default="Active" /></label>
	<div class="col-sm-9 col-lg-10 controls"><g:checkBox name="active" value="${stateInstance?.active}" /></div>
</div>