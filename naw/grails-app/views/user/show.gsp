<%@ page import="naw.User" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<r:require module="userprofile"/>
</head>
<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
      		<span class="divider"><i class="fa fa-angle-right"></i></span>
   		</li>
   		<li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
	    <div class="box">
        <div class="box-title"><h3><i class="fa fa-file"></i>Profile Info</h3></div>
        <div class="box-content">
            <div class="row">
	           <div class="col-md-3">
	           		<dl>
			            <dt><g:message code="user.username.label" default="Username" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="username"/></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.birthday.label" default="Birthday" /></dt>
						<dd></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.email.label" default="Email" /></dt>
						<dd><a href="mailto:#"><g:fieldValue bean="${userInstance}" field="email"/></a></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.roles.label" default="Roles" /></dt>
						<g:each var="role" in="${userInstance.getAuthorities()}">
							<dd>${role.authority}</dd>
						</g:each>
					</dl>
	           </div>
	           <div class="col-md-3">
	           		<dl>
			            <dt><g:message code="user.firstname.label" default="First Name" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="firstname"/></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.gender.label" default="Gender" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="gender"/></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.industry.label" default="Industry" /></dt>
						<dd><g:fieldValue bean="${userInstance.industry}" field="name"/></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.jobtitle.label" default="Job Title" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="jobtitle"/></dd>
					</dl>
	           </div>
	           <div class="col-md-3">
	           		<dl>
			            <dt><g:message code="user.lastname.label" default="Last Name" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="lastname"/></dd>
					</dl>
	           		<dl>
			            <dt><g:message code="user.phone.label" default="Phone" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="phone"/></dd>
					</dl>
					<dl>
			            <dt><g:message code="user.company.label" default="Company" /></dt>
						<dd><g:fieldValue bean="${userInstance}" field="company"/></dd>
					</dl>
	           </div>
	           <div class="col-md-3">
	               <img class="img-responsive img-thumbnail" src="${createLink(controller:'user', action:'picture', id:userInstance.id)}" alt="profile picture"/>
	           </div>
	           </div>
	           
	           <div class="row">
			   <div class="col-md-12">
			       <g:form class="form-horizontal">
						<g:hiddenField name="id" value="${userInstance?.id}" />
						<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
						<g:actionSubmit class="btn btn-primary" value="History" action="history" />
					</g:form>
					<br/>
				</div>
				</div>
				
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h4 class="panel-title">Cities</h4></div>
					<div class="panel-body">
							<g:if test="${userInstance?.cities}">
								<table class="table">
									<thead>
										<tr>
											<th><g:message code="user.city.name.label" default="Name" /></th>
										</tr>
									</thead>
									<tbody>
										<g:each in="${userInstance.cities}" var="city">
											<tr>
												<td><g:link controller="city" action="show" id="${city.id}">${city.name}</g:link></td>
											</tr>
										</g:each>
									</tbody>
								</table>
							</g:if>
						</div>
					</div>
				</div>
				</div>
				
				<div class="row">
				<div class="col-md-12">
				<div class="panel panel-success">
					<div class="panel-heading"><h4 class="panel-title">Orders</h4></div>
					<div class="panel-body">
					<g:if test="${userInstance?.orders}">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Order Id</th>
									<th>Order Date</th>
									<th>Event</th>
									<th>Order Amount</th>
									<th>Paid Amount</th>
									<th>Attendees</th>
								</tr>
							</thead>
							<tbody>
								<g:each in="${userInstance.orders}" var="order">
									<g:if test="${order.rsvps.size() == 1}">
										<tr>
											<td>${order.id}</td>
											<td><g:formatDate format="MM/dd/yyyy" date="${order.dateCreated}"/></td>
											<td><g:link controller="event" action="show" id="${order.event.id}">${order.event.name}</g:link></td>
											<td><g:formatNumber number="${order.orderTotal}" type="currency" currencyCode="USD" /></td>
											<td><g:formatNumber number="${order.paidAmount}" type="currency" currencyCode="USD" /></td>
											<g:each in="${order.rsvps}" var="rsvp">
												<g:if test="${rsvp.user != null}">
													<td>${rsvp.user.firstname} ${rsvp.user.lastname}</td>
												</g:if>
												<g:if test="${rsvp.prospectiveUser != null}">
													<td>${rsvp.prospectiveUser.name}</td>
												</g:if>
											</g:each>
										</tr>
									</g:if>
									<g:else>
										<tr>
											<td rowspan="${order.rsvps.size() + 1}">${order.id}</td>
											<td rowspan="${order.rsvps.size() + 1}"><g:formatDate format="MM/dd/yyyy" date="${order.dateCreated}"/></td>
											<td rowspan="${order.rsvps.size() + 1}"><g:link controller="event" action="show" id="${order.event.id}">${order.event.name}</g:link></td>
											<td rowspan="${order.rsvps.size() + 1}"><g:formatNumber number="${order.orderTotal}" type="currency" currencyCode="USD" /></td>
											<td rowspan="${order.rsvps.size() + 1}"><g:formatNumber number="${order.paidAmount}" type="currency" currencyCode="USD" /></td>
										</tr>
										<g:each in="${order.rsvps}" var="rsvp">
											<tr>
												<g:if test="${rsvp.user != null}">
													<td>${rsvp.user.firstname} ${rsvp.user.lastname}</td>
												</g:if>
												<g:if test="${rsvp.prospectiveUser != null}">
													<td>${rsvp.prospectiveUser.name}</td>
												</g:if>
											</tr>
										</g:each>
									</g:else>
								</g:each>
							</tbody>
						</table>
					</g:if>
					</div>
					</div>
				</div>
				</div>
				
	</div>
	</div>
    </div>
</div>
</body>
</html>
