<%@ page import="naw.User" %>
<%@ page import="naw.City" %>
<%@ page import="naw.Industry" %>
<%@ page import="naw.Role" %>

<div class="row">
<div class="col-md-12">
<div class="form-group">
	<label class="col-sm-3 col-md-2 control-label">Image Upload</label>
	<div class="col-sm-9 col-md-7 controls">
		<div class="fileupload fileupload-new" data-provides="fileupload">
			<div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
		<g:if test="${userInstance?.id}">
			<img src="${createLink(controller:'user', action:'picture', id:userInstance.id)}" alt="" />
		</g:if>
	</div>
	<div class="fileupload-preview fileupload-exists img-thumbnail"
		style="max-width: 200px; max-height: 150px; line-height: 20px;">
	</div>
	<div>
		<span class="btn btn-file">
			<span class="fileupload-new">Select image</span> 
			<span class="fileupload-exists">Change</span> 
			<input type="file" class="default" name="picture"/>
		</span> 
				<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
			</div>
		</div>
	</div>
</div>
<g:if test="${userInstance?.id}">
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="user.roles.label" default="Roles" />
		<g:message code="event.roles.label" default="(CTRL+Click)" />
	</label>
	<div class="col-sm-8 col-lg-7 controls">
	<g:select class="form-control"
		name="roles" 
		from="${Role.list()}"
		value="${userInstance.getAuthorities()}"
		optionKey="id"
		optionValue="authority"
		valueMessagePrefix="user.roles"
		multiple="true"/>
	</div>
</div>
</g:if>
<g:else>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="user.roles.label" default="Roles" />
		<g:message code="event.roles.label" default="(CTRL+Click)" />
	</label>
	<div class="col-sm-8 col-lg-7 controls">
	<g:select class="form-control"
		name="roles" 
		from="${Role.list()}"
		optionKey="id"
		optionValue="authority"
		valueMessagePrefix="user.roles"
		multiple="true"/>
	</div>
</div>
</g:else>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="username">
		<g:message code="user.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="username" required="" value="${userInstance?.username}"/></div>
</div>
<g:if test="${!userInstance?.password}">
	<div class="form-group">
		<label class="col-sm-3 col-lg-2 control-label" for="password">
			<g:message code="user.password.label" default="Password" />
			<span class="required-indicator">*</span>
		</label>
		<div class="col-sm-95 col-lg-7 controls"><g:passwordField class="form-control input-lg" name="password" required="" value="${userInstance?.password}"/></div>
	</div>
</g:if>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="firstname">
		<g:message code="user.firstname.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="firstname" required="" value="${userInstance?.firstname}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="lastname">
		<g:message code="user.lastname.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="lastname" required="" value="${userInstance?.lastname}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="email">
		<g:message code="user.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:field class="form-control input-lg" type="email" name="email" required="" value="${userInstance?.email}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="phone">
		<g:message code="user.phone.label" default="Phone" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="phone" required="" value="${userInstance?.phone}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="jobtitle">
		<g:message code="user.jobtitle.label" default="Job Title" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="jobtitle" value="${userInstance?.jobtitle}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="company">
		<g:message code="user.company.label" default="Company" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:textField class="form-control input-lg" name="company" value="${userInstance?.company}"/></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="gender">
		<g:message code="user.gender.label" default="Gender" />
	</label>
	<div class="col-sm-95 col-lg-7 controls">
		<g:select class="form-control input-lg" name="gender" from="${userInstance.constraints.gender.inList}" value="${userInstance?.gender}" 
		required="" valueMessagePrefix="user.gender" noSelection="['': '']"/>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="accountExpired">
		<g:message code="user.accountExpired.label" default="Account Expired" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:checkBox name="accountExpired" value="${userInstance?.accountExpired}" /></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="accountLocked">
		<g:message code="user.accountLocked.label" default="Account Locked" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:checkBox name="accountLocked" value="${userInstance?.accountLocked}" /></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="enabled">
		<g:message code="user.enabled.label" default="Enabled" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:checkBox name="enabled" value="${userInstance?.enabled}" /></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="passwordExpired">
		<g:message code="user.passwordExpired.label" default="Password Expired" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:checkBox name="passwordExpired" value="${userInstance?.passwordExpired}" /></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="industry">
		<g:message code="user.industry.label" default="Industry" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-8 col-lg-7 controls">
	<g:select class="form-control input-lg" 
		name="industry" 
		from="${Industry.list()}"
		value="${userInstance.industry?.id}"
		optionKey="id"
		optionValue="name"
		valueMessagePrefix="user.industry"/>
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="selectedCities"><g:message code="user.cities.label" default="Cities Interested" /></label>
	<div class="col-sm-95 col-lg-8 controls">
	<table>
	    <tr>
	        <g:each in="${City.list()}" var="city" status="i">
	            <g:if test="${i % 6 == 0}">
	                </tr><tr>
	            </g:if>
	            <td><g:checkBox name="selectedCities" value="${city.id}" checked="${city in userInstance?.cities}" /><label>${city.name}</label></td>
	        </g:each>
	    </tr>
	</table>
	</div>
</div>
</div>
</div>


