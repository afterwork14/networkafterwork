<%@ page import="naw.User"%>
<%@ page import="naw.City" %>
<%@ page import="naw.Industry" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a> 
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /><br/>
				
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<th class="sort-desc sort-asc"><a href="/naw/user/list?sort=username&amp;max=10&amp;order=asc">Username</a></th>
								<th class="sort-desc sort-asc"><a href="/naw/user/list?sort=firstname&amp;max=10&amp;order=asc">Firstname</a></th>
								<th class="sort-desc sort-asc"><a href="/naw/user/list?sort=lastname&amp;max=10&amp;order=asc">Lastname</a></th>
								<th class="sort-desc sort-asc"><a href="/naw/user/list?sort=email&amp;max=10&amp;order=asc">Email</a></th>
								<th class="sort-desc sort-asc"><a href="/naw/user/list?sort=phone&amp;max=10&amp;order=asc">Phone</a></th>
								<th>Orders</th>
							</tr>
						</thead>
						<g:each in="${userInstanceList}" status="i" var="userInstance">
							<tr>
								<td><g:link action="show" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>
								<td>${fieldValue(bean: userInstance, field: "firstname")}</td>
								<td>${fieldValue(bean: userInstance, field: "lastname")}</td>
								<td>${fieldValue(bean: userInstance, field: "email")}</td>
								<td>${fieldValue(bean: userInstance, field: "phone")}</td>
								<td>${userInstance.orders.size()}</td>
							</tr>
						</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${userInstanceTotal}" params="${params}"/></li>
	                    </ul>
	                </div>
				</div>
				
				<div class="clearfix"></div>
				<div class="box-content">
				<g:form controller="user" action="list" class="form-horizontal">
					<div class="row">
	  				    <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="email"><g:message code="user.email.label" default="Email" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="email" value="${params.email}" class="form-control" placeholder="email" /></div>
							</div>
						</div>
						<div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="username"><g:message code="user.username.label" default="Username" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="username" value="${params.username}" class="form-control" placeholder="username" /></div>
							</div>
						</div>
					</div>
					<div class="row">
	  				    <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="firstname"><g:message code="user.firstname.label" default="First Name" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="firstname" value="${params.firstname}" class="form-control" placeholder="first name" /></div>
							</div>
						</div>
						<div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="lastname"><g:message code="user.lastname.label" default="Last Name" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="lastname" value="${params.lastname}" class="form-control" placeholder="last name" /></div>
							</div>
						</div>
					</div>
					<div class="row">
	  				    <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="city"><g:message code="city.label" default="City" /></label>
							<div class="col-sm-8 col-lg-8 controls">
									<g:select class="form-control" name="city" 
										from="${City.list()}"
										optionKey="id"
										value="${params?.city}"
										optionValue="name"
										noSelection="${['""':'Select One...']}"
										valueMessagePrefix="city"/></div>
							</div>
						</div>
						<div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="industry"><g:message code="industry.label" default="Industry" /></label>
							<div class="col-sm-8 col-lg-8 controls">
									<g:select class="form-control" name="industry" 
										from="${Industry.list()}"
										optionKey="id"
										value="${params?.industry}"
										optionValue="name"
										noSelection="${['""':'Select One...']}"
										valueMessagePrefix="industry"/></div>
							</div>
						</div>
					</div>
					<div class="row">
	  				    <div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 col-lg-3 control-label" for="active">
									<g:message code="user.active.label" default="Active" />
								</label>
								<div class="col-sm-8 col-lg-8 controls"><g:checkBox name="active" value="${params?.active}" /></div>
							</div>
						</div>
						 <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-3 control-label" for="company"><g:message code="user.firstname.label" default="Company" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="company" value="${params.company}" class="form-control" placeholder="company" /></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4 col-lg-8 col-lg-offset-4">
								<g:submitButton name="search" class="btn btn-primary" value="${message(code: 'default.button.search.label', default: 'Search')}" />
								<g:submitButton name="reset" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Reset')}" />
							</div>
						</div>
					</div>
				</g:form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
