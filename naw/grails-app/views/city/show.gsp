<%@ page import="naw.City" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'city.label', default: 'City')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>

<div class="row">
<div class="col-md-12">
<div class="box">
	<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
	<div class="box-content">
    <g:if test="${flash.message}">
		 <div class="alert alert-success">${flash.message}</div>
	</g:if>
		
	<div class="row">
    <div class="col-md-3">
        <dl>
           <g:if test="${cityInstance?.name}">
                	<dt><g:message code="city.name.label" default="Name" /></dt>
				<dd><g:fieldValue bean="${cityInstance}" field="name"/></dd>
			</g:if>
		</dl>
	</div>
	<div class="col-md-3">
        <dl>
            <g:if test="${cityInstance?.state}">
                <dt><g:message code="city.state.label" default="State" /></dt>
				<dd><g:link controller="state" action="show" id="${cityInstance.state.id}">${fieldValue(bean: cityInstance.state, field: "name")}</g:link></dd>
			</g:if>
		</dl>
	</div>
	<div class="col-md-3">
        <dl>
            <g:if test="${cityInstance?.state}">
                <dt><g:message code="city.country.label" default="Country" /></dt>
				<dd><g:link controller="country" action="show" id="${cityInstance.state.country.id}">
						${fieldValue(bean: cityInstance.state.country, field: "name")}
					</g:link>
				</dd>
			</g:if>
		</dl>
	</div>
	<div class="col-md-3">
        <dl>
            <dt><g:message code="city.emailDirectListId.label" default="Email Direct List Id" /></dt>
			<dd><g:fieldValue bean="${cityInstance}" field="emailDirectListId"/></dd>
		</dl>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-3">
        <dl>
            <dt><g:message code="city.meetupGroupId.label" default="Meetup Group Id" /></dt>
			<dd><g:fieldValue bean="${cityInstance}" field="meetupGroupId"/></dd>
		</dl>
	</div>
	<div class="col-md-9">
        <dl>
            <dt><g:message code="city.meetupGroupUrlName.label" default="Meetup Group URL Name" /></dt>
			<dd><a href="${fieldValue(bean: cityInstance, field: "meetupGroupUrlName")}" target="_blank">
				<g:fieldValue bean="${cityInstance}" field="meetupGroupUrlName"/></a>
			</dd>
		</dl>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-3">
        <dl>
			<g:if test="${cityInstance?.dateCreated}">
				<dt><g:message code="cityInstance.dateCreated.label" default="Date Created" /></dt>
				<dd><g:formatDate date="${cityInstance?.dateCreated}" /></dd>
			</g:if>
		</dl>
	</div>
	<div class="col-md-3">
        <dl>
			<g:if test="${cityInstance?.lastUpdated}">
				<dt><g:message code="cityInstance.lastUpdated.label" default="Last Updated" /></dt>
				<dd><g:formatDate date="${cityInstance?.lastUpdated}" /></dd>
			</g:if>
		</dl>
	</div>
	<div class="col-md-3">
        <dl>
			<g:if test="${cityInstance?.lastUpdated}">
				<dt><g:message code="cityInstance.lastUpdated.label" default="Last Change By" /></dt>
				<dd></dd>
			</g:if>
		</dl>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
        <g:form class="form-horizontal">
			<g:hiddenField name="id" value="${cityInstance?.id}" />
			<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
			<g:actionSubmit class="btn btn-primary" value="History" action="history" />
		</g:form>
		<br/>
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
	<div class="panel panel-success">
		<div class="panel-heading"><h4 class="panel-title">Venues</h4></div>
		<div class="panel-body">
				<g:if test="${cityInstance?.venues}">
					<table class="table">
						<thead>
							<tr>
								<th><g:message code="city.venues.label" default="Name" /></th>
								<th><g:message code="venue.address1.label" default="Address1"/></th>
								<th><g:message code="venue.address2.label" default="Address2"/></th>
								<th><g:message code="venue.postalcode.label" default="Postal Code"/></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${cityInstance.venues}" var="venue">
								<tr>
									<td><g:link controller="venue" action="show" id="${venue.id}">${venue.name}</g:link></td>
									<td>${fieldValue(bean: venue, field: "address1")}</td>
									<td>${fieldValue(bean: venue, field: "address2")}</td>
									<td>${fieldValue(bean: venue, field: "postalcode")}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
				</g:if>
			</div>
		</div>
	</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-success">
				<div class="panel-heading">
					<h4 class="panel-title">Images</h4>
				</div>
				<div class="panel-body">
						 <g:each in="${images}" var="image">
							<img src="${image.url}" width="100%"/><br/><br/>
						 </g:each>
					</div>  
				</div>
			</div>
		</div>
	</div>

</div>
</div>


</div>


</body>
</html>
