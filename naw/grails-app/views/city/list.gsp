<%@ page import="naw.City"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'city.label', default: 'City')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'city.name.label', default: 'Name')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="state" title="${message(code: 'city.state.label', default: 'State')}" />
								<th>Venues</th>
								<g:sortableColumn class="sort-desc sort-asc" property="state" title="${message(code: 'city.emailDirectListId.label', default: 'List Id')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="state" title="${message(code: 'city.meetupGroupUrlName.label', default: 'Meetup Group URL')}" />
								
							</tr>
						</thead>
						<tbody>
							<g:each in="${cityInstanceList}" status="i" var="cityInstance">
								<tr>
									<td><g:link action="show" id="${cityInstance.id}">${fieldValue(bean: cityInstance, field: "name")}</g:link></td>
									<td><g:link controller="state" action="show" id="${cityInstance.state.id}">${fieldValue(bean: cityInstance.state, field: "name")}</g:link></td>
									<td>${cityInstance.venues.size()}</td>
									<td>${fieldValue(bean: cityInstance, field: "emailDirectListId")}</td>
									<td><a href="${fieldValue(bean: cityInstance, field: "meetupGroupUrlName")}" target="_blank">${fieldValue(bean: cityInstance, field: "meetupGroupUrlName")}</a></td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${cityInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
