<%@ page import="naw.City" %>
<%@ page import="naw.State" %>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="country.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="name" required="" value="${cityInstance?.name}" /></div>
</div>


<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="state">
		<g:message code="user.gender.label" default="State" />
	</label><%--
	<div class="col-sm-95 col-lg-5 controls">
		<g:select class="form-control input-lg" name="state" from="${State.list()}" value="${cityInstance?.state}" 
		required="" valueMessagePrefix="cityInstance.state" noSelection="['': '-Select State-']"/>
	</div>
--%>
	<div class="col-sm-95 col-lg-5 controls">
		<g:select class="form-control input-lg" name="state" from="${State.list()}" value="${cityInstance?.state?.id}"   optionKey="id" optionValue="name"  
		required="" valueMessagePrefix="cityInstance.state" noSelection="['': '-Select State-']"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="list">
		<g:message code="city.list.lable" default="Create Email Direct List" />
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:checkBox name="createList"/></div>
</div>

