<%@ page import="naw.Rsvp" %>



<div class="fieldcontain ${hasErrors(bean: rsvpInstance, field: 'entryType', 'error')} ">
	<label for="entryType">
		<g:message code="rsvp.entryType.label" default="Entry Type" />
		
	</label>
	<g:select name="entryType" from="${rsvpInstance.constraints.entryType.inList}" value="${rsvpInstance?.entryType}" valueMessagePrefix="rsvp.entryType" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: rsvpInstance, field: 'entryTime', 'error')} required">
	<label for="entryTime">
		<g:message code="rsvp.entryTime.label" default="Entry Time" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="entryTime" precision="day"  value="${rsvpInstance?.entryTime}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: rsvpInstance, field: 'order', 'error')} required">
	<label for="order">
		<g:message code="rsvp.order.label" default="Order" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="order" name="order.id" from="${naw.Order.list()}" optionKey="id" required="" value="${rsvpInstance?.order?.id}" class="many-to-one"/>
</div>

