
<%@ page import="naw.Rsvp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rsvp.label', default: 'Rsvp')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-rsvp" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="list-rsvp" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="entryType" title="${message(code: 'rsvp.entryType.label', default: 'Entry Type')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'rsvp.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="entryTime" title="${message(code: 'rsvp.entryTime.label', default: 'Entry Time')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'rsvp.lastUpdated.label', default: 'Last Updated')}" />
					
						<th><g:message code="rsvp.order.label" default="Order" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${rsvpInstanceList}" status="i" var="rsvpInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${rsvpInstance.id}">${fieldValue(bean: rsvpInstance, field: "entryType")}</g:link></td>
					
						<td><g:formatDate date="${rsvpInstance.dateCreated}" /></td>
					
						<td><g:formatDate date="${rsvpInstance.entryTime}" /></td>
					
						<td><g:formatDate date="${rsvpInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: rsvpInstance, field: "order")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${rsvpInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
