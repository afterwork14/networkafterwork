
<%@ page import="naw.Rsvp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rsvp.label', default: 'Rsvp')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-rsvp" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-rsvp" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list rsvp">
			
				<g:if test="${rsvpInstance?.entryType}">
				<li class="fieldcontain">
					<span id="entryType-label" class="property-label"><g:message code="rsvp.entryType.label" default="Entry Type" /></span>
					
						<span class="property-value" aria-labelledby="entryType-label"><g:fieldValue bean="${rsvpInstance}" field="entryType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${rsvpInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="rsvp.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${rsvpInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${rsvpInstance?.entryTime}">
				<li class="fieldcontain">
					<span id="entryTime-label" class="property-label"><g:message code="rsvp.entryTime.label" default="Entry Time" /></span>
					
						<span class="property-value" aria-labelledby="entryTime-label"><g:formatDate date="${rsvpInstance?.entryTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${rsvpInstance?.lastUpdated}">
				<li class="fieldcontain">
					<span id="lastUpdated-label" class="property-label"><g:message code="rsvp.lastUpdated.label" default="Last Updated" /></span>
					
						<span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate date="${rsvpInstance?.lastUpdated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${rsvpInstance?.order}">
				<li class="fieldcontain">
					<span id="order-label" class="property-label"><g:message code="rsvp.order.label" default="Order" /></span>
					
						<span class="property-value" aria-labelledby="order-label"><g:link controller="order" action="show" id="${rsvpInstance?.order?.id}">${rsvpInstance?.order?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${rsvpInstance?.id}" />
					<g:link class="edit" action="edit" id="${rsvpInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
