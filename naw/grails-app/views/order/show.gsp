<%@ page import="naw.Industry" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'industry.label', default: 'Industry')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
		<div class="col-md-12">
		<div class="box">
		<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
		
			<div class="box-content">
	    	<g:if test="${flash.message}">
				 <div class="alert alert-success">${flash.message}</div>
			</g:if>
        
           	<div class="col-md-12">
             	<dl>
	                 <g:if test="${industryInstance?.name}">
	                 	<dt><g:message code="industry.name.label" default="Name" /></dt>
						<dd><g:fieldValue bean="${industryInstance}" field="name"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<dt><g:message code="industry.active.label" default="Active" /></dt>
					<dd><g:formatBoolean boolean="${industryInstance?.active}" /></dd>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${industryInstance?.dateCreated}">
						<dt><g:message code="industry.dateCreated.label" default="Date Created" /></dt>
						<dd><g:formatDate date="${industryInstance?.dateCreated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${industryInstance?.lastUpdated}">
						<dt><g:message code="industry.lastUpdated.label" default="Last Updated" /></dt>
						<dd><g:formatDate date="${industryInstance?.lastUpdated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${industryInstance?.lastUpdated}">
						<dt><g:message code="industry.lastUpdated.label" default="Last Change By" /></dt>
						<dd></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<g:form class="form-horizontal">
					<g:hiddenField name="id" value="${industryInstance?.id}" />
					<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
					<g:actionSubmit class="btn btn-primary" value="History" action="history" />
				</g:form>
				<br/>
			</div>
     </div>
     </div>
     </div>
</div>
</body>
</html>
