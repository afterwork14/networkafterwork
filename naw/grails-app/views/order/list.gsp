<%@ page import="naw.Order"%>
<%@ page import="naw.Event"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="id" title="${message(code: 'order.id.label', default: 'Order Id')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="source" title="${message(code: 'order.source.label', default: 'Source')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="sourceId" title="${message(code: 'order.sourceId.label', default: 'Source Id')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="orderTotal" title="${message(code: 'order.orderTotal.label', default: 'Order Total')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="paidAmount" title="${message(code: 'order.paidAmount.label', default: 'Paid Amount')}" />
								<th>User</th>
								<th>Event</th>
								<g:sortableColumn class="sort-desc sort-asc" property="dateCreated" title="${message(code: 'order.dateCreated.label', default: 'Date Created')}" />
							</tr>
						</thead>
						<tbody>
							<g:each in="${orderInstanceList}" status="i" var="orderInstance">
								<tr>
									<td><g:link action="show" id="${orderInstance.id}">${fieldValue(bean: orderInstance, field: "id")}</g:link></td>
									<td>${fieldValue(bean: orderInstance, field: "source")}</td>
									<td>${fieldValue(bean: orderInstance, field: "sourceId")}</td>
									<td><g:formatNumber number="${orderInstance.orderTotal}" type="currency" currencyCode="USD"/></td>
									<td><g:formatNumber number="${orderInstance.paidAmount}" type="currency" currencyCode="USD"/></td>
									<td><g:link controller="user" action="show" id="${orderInstance.user.id}">${fieldValue(bean: orderInstance.user, field: "email")}</g:link></td>
									<td><g:link controller="event" action="show" id="${orderInstance.event.id}">${fieldValue(bean: orderInstance.event, field: "name")}</g:link></td>
									<td><g:formatDate date="${orderInstance.dateCreated}" /></td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${orderInstanceTotal}" /></li>
	                     </ul>
	                </div>
	                
	                                <div class="clearfix"></div>
				<div class="box-content">
				<g:form controller="order" action="list" class="form-horizontal">
					
					
					
					
					
					<div class="row">
	  				    <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-4 control-label" for="ordernumber"><g:message code="order.ordernumber.label" default="Order Number" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="ordernumber" value="${params.ordernumber}" class="form-control" placeholder="order number" /></div>
							</div>
						</div>
						 <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-4 control-label" for="email"><g:message code="order.email.label" default="Email" /></label>
							<div class="col-sm-8 col-lg-8 controls">
								<g:textField name="email" value="${params.email}" class="form-control" placeholder="email" /></div>
							</div>
						</div>
					</div>
					
					
					<div class="row">
	  				   <div class="col-md-6">
		  				    <div class="form-group">
		  				    <label class="col-sm-4 col-lg-4 control-label" for="event"><g:message code="event.label" default="Event" /></label>
							<div class="col-sm-8 col-lg-8 controls">
									<g:select class="form-control input-lg" name="event" 
										from="${Event.list()}"
										optionKey="id"
										value="${params?.event}"
										optionValue="name"
										noSelection="${['':'Select One...']}"
										valueMessagePrefix="event"/></div>
							</div>
						</div>
						
					</div>
					
					
					<div class="row">
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4 col-lg-8 col-lg-offset-4">
								<g:submitButton name="search" class="btn btn-primary" value="${message(code: 'default.button.search.label', default: 'Search')}" />
								<g:submitButton name="reset" class="btn btn-primary" value="${message(code: 'default.button.reset.label', default: 'Reset')}" />
							</div>
						</div>
					</div>
				</g:form>
				</div>
	                
	                
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
