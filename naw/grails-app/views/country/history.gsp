<%@ page import="naw.Country"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'country.label', default: 'Country')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
                <span class="divider"><i class="fa fa-angle-right"></i></span>
            </li>
            <li>
            	<g:link action="show" id="${id}"><g:message code="default.show.label" args="[entityName]" /></g:link>
            	<span class="divider"><i class="fa fa-angle-right"></i></span>
            </li>
            <li class="active"><g:message code="default.history.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.history.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<th>${message(code: 'country.name.label', default: 'Name')}</th>
								<th>${message(code: 'country.abbreviation.label', default: 'Abbreviation')}</th>
								<th>${message(code: 'country.active.label', default: 'Active')}</th>
								<th>${message(code: 'country.lastUpdated.label', default: 'Last Updated')}</th>
								<th>Revision Type</th>
								<th>User</th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${countryInstanceHistoryList}" status="i" var="countryInstanceHistory">
								<tr>
									<td>${fieldValue(bean: countryInstanceHistory, field: "name")}</td>
									<td>${fieldValue(bean: countryInstanceHistory, field: "abbreviation")}</td>
									<td>${fieldValue(bean: countryInstanceHistory, field: "active")}</td>
									<td><g:formatDate date="${countryInstanceHistory.revisionEntity.revisionDate}" /></td>
									<td>${fieldValue(bean: countryInstanceHistory, field: "revisionType")}</td>
									<td>${fieldValue(bean: countryInstanceHistory, field: "revisionEntity.currentUser.username")}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
