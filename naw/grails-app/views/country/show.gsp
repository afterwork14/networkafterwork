<%@ page import="naw.Country" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'country.label', default: 'Country')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>

<div class="row">
		<div class="col-md-12">
		<div class="box">
		<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
		
			<div class="box-content">
	    	<g:if test="${flash.message}">
				 <div class="alert alert-success">${flash.message}</div>
			</g:if>
        
           	<div class="col-md-12">
             	<dl>
	                 <g:if test="${countryInstance?.name}">
	                 	<dt><g:message code="country.name.label" default="Name" /></dt>
						<dd><g:fieldValue bean="${countryInstance}" field="name"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
	                 <g:if test="${countryInstance?.abbreviation}">
	                 	<dt><g:message code="country.abbreviation.label" default="Abbreviation" /></dt>
						<dd><g:fieldValue bean="${countryInstance}" field="abbreviation"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${countryInstance?.dateCreated}">
						<dt><g:message code="countryInstance.dateCreated.label" default="Date Created" /></dt>
						<dd><g:formatDate date="${countryInstance?.dateCreated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${countryInstance?.lastUpdated}">
						<dt><g:message code="countryInstance.lastUpdated.label" default="Last Updated" /></dt>
						<dd><g:formatDate date="${countryInstance?.lastUpdated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${countryInstance?.lastUpdated}">
						<dt><g:message code="countryInstance.lastUpdated.label" default="Last Change By" /></dt>
						<dd></dd>
					</g:if>
				</dl>
			</div>
			
			<div class="col-md-12">
			<g:if test="${countryInstance?.states}">
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<th><g:message code="country.state.label" default="States" /></th>
							</tr>
						</thead>
						<tbody>
							<g:each in="${countryInstance.states}" var="state">
								<tr>
									<td><g:link controller="state" action="show" id="${state.id}">${state.name}</g:link></td>
								</tr>
							</g:each>
						</tbody>
					</table>
                </div>
			</g:if>
			</div>
			
			<div class="col-md-12">
             	<g:form class="form-horizontal">
					<g:hiddenField name="id" value="${countryInstance?.id}" />
					<g:link class="btn btn-link" action="edit" id="${countryInstance?.id}">
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<g:link class="btn btn-link" action="history" id="${countryInstance?.id}">
						<g:message code="default.button.history.label" default="History" />
					</g:link>
				</g:form>
			</div>
     </div>
     </div>
     </div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-success">
			<div class="panel-heading">
				<h4 class="panel-title">Images</h4>
			</div>
			<div class="panel-body">
				<g:each in="${images}" var="image">
					<img src="${image.url}" width="100%" />
					<br />
					<br />
				</g:each>
			</div>
		</div>
	</div>
</div>


</body>
</html>
