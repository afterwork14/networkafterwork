<%@ page import="naw.PriceModifier"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'priceModifier.label', default: 'PriceModifier')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
	<div id="breadcrumbs">
		<ul class="breadcrumb">
			<li><i class="fa fa-home"></i> <g:link action="list">
					<g:message code="default.list.label" args="[entityName]" />
				</g:link> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
			<li class="active"><g:message code="default.show.label"
					args="[entityName]" /></li>
		</ul>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-title">
					<h3>
						<i class="fa fa-bars"></i>
						<g:message code="default.show.label" args="[entityName]" />
					</h3>
				</div>
				<div class="box-content">
					<g:if test="${flash.message}">
						<div class="alert alert-success">
							${flash.message}
						</div>
					</g:if>

					<div class="row">
						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.method}">
									<dt>
										<g:message code="priceModifier.name.label" default="Method" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}" field="method" />
									</dd>
								</g:if>
							</dl>
						</div>



						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.description}">
									<dt>
										<g:message code="priceModifier.description.label"
											default="Description" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}"
											field="description" />
									</dd>
								</g:if>
							</dl>
						</div>


						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.type}">
									<dt>
										<g:message code="priceModifier.type.label" default="Type" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}" field="type" />
									</dd>
								</g:if>
							</dl>
						</div>

					</div>







					<div class="row">

						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.level}">
									<dt>
										<g:message code="priceModifier.level.label" default="Level" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}" field="level" />
									</dd>
								</g:if>
							</dl>
						</div>


						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.value}">
									<dt>
										<g:message code="priceModifier.value.label" default="Value" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}" field="value" />
									</dd>
								</g:if>
							</dl>
						</div>

						<div class="col-md-3">
							<dl>
								<g:if test="${priceModifierInstance?.active}">
									<dt>
										<g:message code="priceModifier.active.label" default="Active" />
									</dt>
									<dd>
										<g:fieldValue bean="${priceModifierInstance}" field="active" />
									</dd>
								</g:if>
							</dl>
						</div>




					</div>













				</div>
			</div>


		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
        <g:form class="form-horizontal">
			<g:hiddenField name="id" value="${priceModifierInstance?.id}" />
			<g:actionSubmit class="btn btn-primary" value="Edit" action="edit" />
			<g:actionSubmit class="btn btn-primary" value="History" action="history" />
		</g:form>
		<br/>
	</div>
	</div>
</body>
</html>
