<%@ page import="naw.PriceModifier" %>
<%@ page import="naw.State" %>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="priceMofifier.method.label" default="Method" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls">
		<g:select class="form-control input-lg" name="method" from="${priceModifierInstance.constraints.method.inList}" value="${priceModifierInstance?.method}" 
		required="" valueMessagePrefix="priceModifierInstance.method" noSelection="['': '-Select Method-']"/>
	</div>
</div>


<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="value">
		<g:message code="priceMofifier.description.label" default="Drescription" />
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:field class="form-control input-lg" type="text" name="description" required="" value="${priceModifierInstance?.description}"/></div>
</div>



<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="priceMofifier.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls">
		<g:select class="form-control input-lg" name="type" from="${priceModifierInstance.constraints.type.inList}" value="${priceModifierInstance?.type}" 
		required="" valueMessagePrefix="priceModifierInstance.type" noSelection="['': '-Select Type-']"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="priceMofifier.level.label" default="Level" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls">
		<g:select class="form-control input-lg" name="level" from="${priceModifierInstance.constraints.level.inList}" value="${priceModifierInstance?.level}" 
		required="" valueMessagePrefix="priceModifierInstance.level" noSelection="['': '-Select Type-']"/>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label" for="value">
		<g:message code="priceMofifier.value.label" default="Value" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-7 controls"><g:field class="form-control input-lg" type="text" name="value" required="" value="${priceModifierInstance?.value}"/></div>
</div>