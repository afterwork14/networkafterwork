<%@ page import="naw.PriceModifier"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'priceModifier.label', default: 'PriceModifier')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
                <span class="divider"><i class="fa fa-angle-right"></i></span>
            </li>
            <li>
            	<g:link action="show" id="${priceModifierInstance.id}"><g:message code="default.show.label" args="[entityName]" /></g:link>
            	<span class="divider"><i class="fa fa-angle-right"></i></span>
            </li>
            <li class="active"><g:message code="default.edit.label" args="[entityName]" /></li>
        </ul>
     </div>
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.edit.label" args="[entityName]" /></h3></div>
			
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<g:hasErrors bean="${priceModifierInstance}">
		            <div class="alert alert-danger">
						<ul>
							<g:eachError bean="${priceModifierInstance}" var="error">
							<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
							</g:eachError>
						</ul>
					</div>
				</g:hasErrors>
				<g:form class="form-horizontal" method="post">
					<g:hiddenField name="id" value="${priceModifierInstance?.id}" />
					<g:hiddenField name="version" value="${priceModifierInstance?.version}" />
					<g:render template="form"/>
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
							<g:actionSubmit type="submit" action="update" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" />
						</div>
					</div>
				</g:form>
			</div>
			</div>
		</div>
	</div>
</body>
</html>
