<%@ page import="naw.PriceModifier"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'priceModifier.label', default: 'PriceModifier')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="method" title="${message(code: 'priceModifier.method.label', default: 'Method')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="type" title="${message(code: 'priceModifier.type.label', default: 'Type')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="level" title="${message(code: 'priceModifier.level.label', default: 'Level')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="value" title="${message(code: 'priceModifier.value.label', default: 'Value')}" />
								
							</tr>
						</thead>
						<tbody>
							<g:each in="${priceModifierInstanceList}" status="i" var="priceModifierInstance">
								<tr>
									<td><g:link action="show" id="${priceModifierInstance.id}">${fieldValue(bean: priceModifierInstance, field: "method")}</g:link></td>
									<td>${fieldValue(bean: priceModifierInstance, field: "type")}</td>
									<td>${fieldValue(bean: priceModifierInstance, field: "level")}</td>
									<td>${fieldValue(bean: priceModifierInstance, field: "value")}</td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${priceModifierInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
