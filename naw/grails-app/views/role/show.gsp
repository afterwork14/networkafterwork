<%@ page import="naw.Country" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <g:link action="list"><g:message code="default.list.label" args="[entityName]" /></g:link>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><g:message code="default.show.label" args="[entityName]" /></li>
    </ul>
</div>
<div class="row">
		<div class="col-md-12">
		<div class="box">
		<div class="box-title"><h3><i class="fa fa-bars"></i><g:message code="default.show.label" args="[entityName]" /></h3></div>
		
			<div class="box-content">
	    	<g:if test="${flash.message}">
				 <div class="alert alert-success">${flash.message}</div>
			</g:if>
        
           	<div class="col-md-12">
             	<dl>
	                 <g:if test="${roleInstance?.authority}">
	                 	<dt><g:message code="role.name.label" default="Role Name" /></dt>
						<dd><g:fieldValue bean="${roleInstance}" field="authority"/></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
	                 <g:if test="${roleInstance?.description}">
	                 	<dt><g:message code="role.description.label" default="Description" /></dt>
						<dd><g:fieldValue bean="${roleInstance}" field="description"/></dd>
					</g:if>
				</dl>
			</div>
			
			<div class="col-md-12">
             	<dl>
					<g:if test="${role?.dateCreated}">
						<dt><g:message code="roleInstance.dateCreated.label" default="Date Created" /></dt>
						<dd><g:formatDate date="${roleInstance?.dateCreated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${role?.lastUpdated}">
						<dt><g:message code="roleInstance.lastUpdated.label" default="Last Updated" /></dt>
						<dd><g:formatDate date="${roleInstance?.lastUpdated}" /></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<dl>
					<g:if test="${role?.lastUpdated}">
						<dt><g:message code="roleInstance.lastUpdated.label" default="Last Change By" /></dt>
						<dd></dd>
					</g:if>
				</dl>
			</div>
			<div class="col-md-12">
             	<g:form class="form-horizontal">
					<g:hiddenField name="id" value="${roleInstance?.id}" />
					<g:link class="btn btn-link" action="edit" id="${roleInstance?.id}">
						<g:message code="default.button.edit.label" default="Edit" />
					</g:link>
					<g:link class="btn btn-link" action="history" id="${roleInstance?.id}">
						<g:message code="default.button.history.label" default="History" />
					</g:link>
				</g:form>
			</div>
     </div>
     </div>
     </div>
</div>
</body>
</html>
