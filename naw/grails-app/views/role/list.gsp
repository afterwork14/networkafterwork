<%@ page import="naw.Role"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'role.label', default: 'Role')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
					</div>
					<div class="btn-group">
						<a class="btn btn-circle show-tooltip" title="Print" href="#"><i class="fa fa-print"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to PDF" href="#"><i class="fa fa-file-text-o"></i></a>
						<a class="btn btn-circle show-tooltip" title="Export to Exel" href="#"><i class="fa fa-table"></i></a>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'role.authority.label', default: 'Role Name')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="abbreviation" title="${message(code: 'role.description.label', default: 'Description')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="dateCreated" title="${message(code: 'role.dateCreated.label', default: 'Date Created')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="lastUpdated" title="${message(code: 'role.lastUpdated.label', default: 'Last Updated')}" />
							</tr>
						</thead>
						<tbody>
							<g:each in="${roleInstanceList}" status="i" var="roleInstance">
								<tr>
									<td><g:link action="show" id="${roleInstance.id}">${fieldValue(bean: roleInstance, field: "authority")}</g:link></td>
									<td><g:link action="show" id="${roleInstance.id}">${fieldValue(bean: roleInstance, field: "description")}</g:link></td>
									<td><g:formatDate date="${roleInstance.dateCreated}" /></td>
									<td><g:formatDate date="${roleInstance.lastUpdated}" /></td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${roleInstanceTotal}" /></li>
	                     </ul>
	                </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- END Main Content -->
</body>
</html>
