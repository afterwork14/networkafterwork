<%@ page import="naw.Role" %>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="role.authority.label" default="Role Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="authority" required="" value="${roleInstance?.authority}" /></div>
</div>

<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="country.abbreviation.label" default="Abbreviation" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="description" required="" value="${roleInstance?.description}" /></div>
</div>

