<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="login">
        <r:script disposition="defer">
            function goToForm(form)
            {
                $('.login-wrapper > form:visible').fadeOut(500, function(){
                    $('#form-' + form).fadeIn(500);
                });
            }
            $(function() {
                $('.goto-login').click(function(){
                    goToForm('login');
                });
                $('.goto-forgot').click(function(){
                    goToForm('forgot');
                });
                $('.goto-register').click(function(){
                    goToForm('register');
                });
            });
        </r:script>
    </head>
    <body class="login-page">

        <!-- BEGIN Main Content -->
        <div id='login' class="login-wrapper">
            <!-- BEGIN Login Form -->
            <form id="form-login" action="${postUrl}" method="POST">
                <h3>Login to your account</h3>
                <hr/>
                <g:if test="${flash.message}">
					<div class="alert alert-danger">${flash.message}</div>
				</g:if>
                <div class="form-group">
                    <div class="controls">
                        <input type="text" placeholder="Username" name='j_username' class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <input type="password" placeholder="Password" name='j_password' class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox" value="remember" /> Remember me
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary form-control">Sign In</button>
                        <a href="/naw/oauth/authenticate/facebook">
                        Connect With Facebook</a>
                    </div>
                </div>
                <hr/>
                <p class="clearfix">
                    <a href="#" class="goto-forgot pull-left">Forgot Password?</a>
                </p>
            </form>
            <!-- END Login Form -->

            <!-- BEGIN Forgot Password Form -->
            <form id="form-forgot" action="index.html" method="get" style="display:none">
                <h3>Get back your password</h3>
                <hr/>
                <div class="form-group">
                    <div class="controls">
                        <input type="text" placeholder="Email" class="form-control" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="controls">
                        <button type="submit" class="btn btn-primary form-control">Recover</button>
                    </div>
                </div>
                <hr/>
                <p class="clearfix">
                    <a href="#" class="goto-login pull-left">← Back to login form</a>
                </p>
            </form>
            <!-- END Forgot Password Form -->           
        </div>
        <!-- END Main Content -->
    </body>
</html>
