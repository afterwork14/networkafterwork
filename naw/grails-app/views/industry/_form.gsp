<%@ page import="naw.Industry" %>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label">
		<g:message code="industry.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<div class="col-sm-95 col-lg-5 controls"><g:textField class="form-control input-lg" name="name" required="" value="${industryInstance?.name}" /></div>
</div>
<div class="form-group">
	<label class="col-sm-3 col-lg-2 control-label"><g:message code="industry.active.label" default="Active" /></label>
	<div class="col-sm-9 col-lg-10 controls"><g:checkBox name="active" value="${industryInstance?.active}" /></div>
</div>