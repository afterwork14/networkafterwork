<%@ page import="naw.Industry"%>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'industry.label', default: 'Industry')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<!-- BEGIN Main Content -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li class="active">
            <i class="fa fa-home"></i>
            <g:message code="default.list.label" args="[entityName]" />
        </li>
    </ul>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-title">
				<h3><i class="fa fa-table"></i><g:message code="default.list.label" args="[entityName]" /></h3>
			</div>
			<div class="box-content">
				<g:if test="${flash.message}">
					<div class="alert alert-success">${flash.message}</div>
				</g:if>
				<div class="btn-toolbar pull-right clearfix">
					<div class="btn-group">
						<g:link class="btn btn-circle show-tooltip" title="Add new record" action="create"><i class="fa fa-plus"></i></g:link>
						<g:link class="btn btn-circle show-tooltip" title="Export to PDF" action="list" params="[format: 'pdf']">
							<i class="fa fa-file-text-o"></i>
						</g:link>
						<g:link class="btn btn-circle show-tooltip" title="Export to Excel" action="list" params="[format: 'excel']">
							<i class="fa fa-table"></i>
						</g:link>
					</div>
				</div>
				<br /> <br />
				<div class="clearfix"></div>
				<div class="table-responsive">
					<table class="table table-advance">
						<thead>
							<tr>
								<g:sortableColumn class="sort-desc sort-asc" property="name" title="${message(code: 'industry.name.label', default: 'Name')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="active" title="${message(code: 'industry.active.label', default: 'Active')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="dateCreated" title="${message(code: 'industry.dateCreated.label', default: 'Date Created')}" />
								<g:sortableColumn class="sort-desc sort-asc" property="lastUpdated" title="${message(code: 'industry.lastUpdated.label', default: 'Last Updated')}" />
							</tr>
						</thead>
						<tbody>
							<g:each in="${industryInstanceList}" status="i" var="industryInstance">
								<tr>
									<td><g:link action="show" id="${industryInstance.id}">${fieldValue(bean: industryInstance, field: "name")}</g:link></td>
									<td>${fieldValue(bean: industryInstance, field: "active")}</td>
									<td><g:formatDate date="${industryInstance.dateCreated}" /></td>
									<td><g:formatDate date="${industryInstance.lastUpdated}" /></td>
								</tr>
							</g:each>
						</tbody>
					</table>
					<div class="text-center">
	                    <ul class="pagination pagination-colory">
	                        <li><g:paginate total="${industryInstanceTotal}" params="${params}"/></li>
	                     </ul>
	                </div>
                </div>
			</div>
			
			<div class="clearfix"></div>
			<div class="box-content">
			<g:form controller="industry" action="list" class="form-horizontal">
				<div class="row">
  				    <div class="col-md-6">
	  				    <div class="form-group">
							<div class="col-sm-9 col-lg-10 controls"><g:textField name="name" value="${params.name}" class="form-control" placeholder="name" /></div>
						</div>
					</div>
					<div class="col-md-6">
	  				    <div class="form-group">
							<div class="col-sm-9 col-lg-10 controls"><g:checkBox name="active" value="${params.active}" /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-4 col-lg-8 col-lg-offset-4">
							<g:submitButton name="search" class="btn btn-primary" value="${message(code: 'default.button.search.label', default: 'Search')}" />
							<button type="reset" class="btn">Reset</button>
						</div>
					</div>
				</div>
			</g:form>
			</div>
			
		</div>
	</div>
</div>



<!-- END Main Content -->
</body>
</html>
