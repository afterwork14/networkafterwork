package naw.service

import naw.FacebookService;
import grails.converters.JSON
import grails.plugins.rest.client.RestBuilder
import grails.test.mixin.TestFor
import groovy.text.SimpleTemplateEngine
import spock.lang.Specification
import org.apache.commons.logging.LogFactory

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(FacebookService)
class FacebookServiceSpec extends Specification {
	
	private static final log = LogFactory.getLog(this);
	def rest = new RestBuilder();
	
//	def "test for getting facebook long access token"(){
//		
//		given: "valid short token"
//		def endPoint = "https://graph.facebook.com/oauth/client_code?"
//		+"access_token=CAAKdXw2kSBkBAAVA1BKybPyBR7lwJuxgK2lIS8HqRUcdnHWEgbNlWsNcIkD7wDaHZADEAUk9GjcBZBFXfCNox4ValSrGPsw5fMxVsewQoUeBzffMHxPBSE06VnW24HqejMMtXOajWR0O5CYda4budM3sZAOBXz4cQxJZBBHZAMnPVGQGZAB9G0kqHlyVZCGXbBQH0AD9kMmL4QyGx0tv0rN"
//		+"&client_secret=8562d5785a119a59913bc573f229cafe"
//		+"&redirect_uri=http://localhost:8100/"
//		+"&client_id=735981529745433";
//		
//		when: "call service to get long access token"
//		def rest = new RestBuilder();
//		def fbResponse = rest.get(endPoint);
//		
//		then: "we should see a valid response"
//		log.debug(fbResponse);
//	}
	
	def "get app access token"(){
		
		given: "valid facebook app"
		def endPoint = 'https://graph.facebook.com/oauth/access_token?client_id=$clientId&client_secret=$clientSecret&grant_type=client_credentials';
		def binding = ["clientId": "735981529745433", "clientSecret": "8562d5785a119a59913bc573f229cafe"];
		def engine = new SimpleTemplateEngine();
		def template = engine.createTemplate(endPoint).make(binding)
		
		when: "call service to get app access token"
		def restResponse = rest.get(template.toString());
		
		then: "we should see a valid response"
		log.debug restResponse;
	}
	
	def "validate facebook token"(){
		
		given: "facebook token"
		def input_token="CAAKdXw2kSBkBADIRzExToJgZCtSauYu3QJbTPdpUW7rzI8E253JD1UYFgDmt8ukqixWX1JWikaVzKm2W9yDZAEDOcVEZCjbkdfnFEef8lLzgug7QcSujfsNZBLiAZBBXEdTjZBVeUxUuNnexQjdahh6LIyZCGARYZAoOSMoA7dNhZAxt55uhALE0wLviNiff6t3cYwDTGNYvv1o14WVZCxfsZCY";
		def userId = "516504490";
		when: "call service to get long access token"
		
		def isValid = service.isValidToken(input_token, userId);
		
		then: "we should see a valid response"
		log.debug isValid;
		
		//sample
		//{"data":{"app_id":735981529745433,"is_valid":true,"application":"nawdevelopment",
		//"user_id":516504490,"expires_at":1411801200,"scopes":["public_profile","basic_info","email","user_friends"]}}
	}
}
