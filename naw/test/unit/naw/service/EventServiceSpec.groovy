package naw.service



import naw.Event;
import naw.EventService;
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.*

/**
 *
 */
@TestFor(EventService)
@Mock(Event)
class EventServiceSpec extends Specification {


	def "test save method event service"(){
		given: "dummy event object"
		def event= new Event()
		event.name = "Dummy Event"
		event.capacity= "200"
		event.address1= "1 Infinite Loop"
		event.address2= ""
		event.city="Cupertino"
		event.postalcode="95014"
		event.state= "CA"
		event.country= "US"
		event.price= 15.00
		event.startTime= new Date()
		event.endTime= new Date()
		
		when: "call save event"
		Event savedEvent=service.saveEvent(event, null)
		
		then: "we should able to find event"
		Event resultEvent=Event.get(savedEvent.id)
		resultEvent.name=="Dummy Event"
	}
	
	def "get events ordered by location"(){
		given: "geo location coordinates"
		def latitude = 41.9073196
		def longitude = -87.71182109999999

		when: "call get events"
		def events = service.getEventsByGeoLocation(latitude, longitude)

		then: "we should able to find event"
		log.debug(events)
	}
}
