package naw.service

import naw.MeetupService;
import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import grails.plugins.rest.client.RestBuilder

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(MeetupService)
class MeetupServiceSpec extends Specification {

	
//	def "create event"() {
//		given: "mock service"
//		
//		when: "call event method"
//		def params = [:]
//		params.put("groupId", "11231592")
//		params.put("group_urlname", "http://www.meetup.com/Network-After-Work-Fort-Worth-Business-Networking-Events/")
//		params.put("name", "UNIT TESTING MEETUP INTEGRATION")
//		params.put("publishStatus", "draft")
//		
//		def groups = service.createEvent(params)
//		
//		then: "we should see OK response in logs"
//	}
	
	def "get rsvps"() {
		given: "mock service"
		
		when: "call method"
		def groups = service.getRsvps("157167662", "37381e10473c5c58564c454970a2320")
		
		then: "we should see OK response in logs"
	}
}
