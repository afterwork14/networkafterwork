package naw.service



import spock.lang.*

import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock
import naw.OrderService
import naw.PaypalService
import naw.EmailDirectService
/**
 *
 */
@TestFor(OrderService)
@Mock([PaypalService, EmailDirectService])
class OrderServiceSpec extends Specification {

   def "submit transaction test"() {
		given: "mock service"
		
		when: "call method"
		
		def groups = service.submitTransaction()
		
		then: "we should see OK response in logs"
	}
}
