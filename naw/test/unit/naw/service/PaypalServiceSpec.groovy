package naw.service

import naw.PaypalService;
import paypal.payflow.*
import grails.test.mixin.TestFor
import spock.lang.Specification
import grails.test.mixin.Mock

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PaypalService)
class PaypalServiceSpec extends Specification {

    def "submit transaction test"() {
		given: "mock service"
		
		when: "call method"
		
		def groups = service.submitTransaction()
		
		then: "we should see OK response in logs"
	}
}
