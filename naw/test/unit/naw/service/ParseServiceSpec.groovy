package naw.service

import naw.ParseService;
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ParseService)
class ParseServiceSpec extends Specification {

	def "send push notification"(){
		given: "new event"
		when: "new event is created"
		def postResponse = service.sendNewEventPushNotification()
		then: "push notification should be sent"
	}
}
