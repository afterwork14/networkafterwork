package naw.service

import spock.lang.*
import grails.test.mixin.TestFor
import naw.EmailDirectService
import grails.test.mixin.Mock
/**
 *
 */
@TestFor(EmailDirectService)
class EmailDirectServiceSpec extends Specification {

	def "test add new list"(){
		given: "new city"
		when: "call add list"
		def postResponse = service.addList("testcity")
		then: "we should able to find event"
		log.debug(postResponse)
	}

	def "test send profile update email"(){
		given: "existing user"
		when: "updates profile"
		def postResponse = service.sendProfileUpdateEmail("purvin.84@gmail.com", "testbody")
		then: "we should able to see email"
	}
}
