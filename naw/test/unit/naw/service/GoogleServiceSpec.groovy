package naw.service

import naw.GoogleService;
import naw.Venue
import com.vividsolutions.jts.geom.Coordinate
import com.vividsolutions.jts.geom.GeometryFactory
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(GoogleService)
class GoogleServiceSpec extends Specification {
	def geometryFactory
	
	def "test google geo service"(){

		given: "address"
		def address = "1600 Amphitheatre Parkway, Mountain View, CA, 94043, US "

		when: "call get point"
		def geoLocation = service.getPoint(address)

		then: "we should able to see coordinates"
		def lat = (geoLocation.json.results.geometry.location.lat).get(0)
		def lng = (geoLocation.json.results.geometry.location.lng).get(0)
		def point = new GeometryFactory().createPoint(new Coordinate(lat,lng))
		assert point != null
	}
}
