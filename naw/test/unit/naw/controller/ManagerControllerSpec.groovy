package naw.controller

import grails.test.mixin.TestFor
import spock.lang.Specification
import naw.ManagerController

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ManagerController)
class ManagerControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
