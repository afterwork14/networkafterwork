package naw.controller



import naw.Order;
import naw.OrderController;

import org.junit.*

import grails.test.mixin.*

@TestFor(OrderController)
@Mock(Order)
class OrderControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/order_/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.order_InstanceList.size() == 0
        assert model.order_InstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.order_Instance != null
    }

    void testSave() {
        controller.save()

        assert model.order_Instance != null
        assert view == '/order_/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/order_/show/1'
        assert controller.flash.message != null
        assert Order.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/order_/list'

        populateValidParams(params)
        def order_ = new Order(params)

        assert order_.save() != null

        params.id = order_.id

        def model = controller.show()

        assert model.order_Instance == order_
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/order_/list'

        populateValidParams(params)
        def order_ = new Order(params)

        assert order_.save() != null

        params.id = order_.id

        def model = controller.edit()

        assert model.order_Instance == order_
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/order_/list'

        response.reset()

        populateValidParams(params)
        def order_ = new Order(params)

        assert order_.save() != null

        // test invalid parameters in update
        params.id = order_.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/order_/edit"
        assert model.order_Instance != null

        order_.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/order_/show/$order_.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        order_.clearErrors()

        populateValidParams(params)
        params.id = order_.id
        params.version = -1
        controller.update()

        assert view == "/order_/edit"
        assert model.order_Instance != null
        assert model.order_Instance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/order_/list'

        response.reset()

        populateValidParams(params)
        def order_ = new Order(params)

        assert order_.save() != null
        assert Order.count() == 1

        params.id = order_.id

        controller.delete()

        assert Order.count() == 0
        assert Order.get(order_.id) == null
        assert response.redirectedUrl == '/order_/list'
    }
}
