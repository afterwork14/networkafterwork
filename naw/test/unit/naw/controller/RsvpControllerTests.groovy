package naw.controller



import naw.Rsvp;
import naw.RsvpController;

import org.junit.*

import grails.test.mixin.*

@TestFor(RsvpController)
@Mock(Rsvp)
class RsvpControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/rsvp/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.rsvpInstanceList.size() == 0
        assert model.rsvpInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.rsvpInstance != null
    }

    void testSave() {
        controller.save()

        assert model.rsvpInstance != null
        assert view == '/rsvp/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/rsvp/show/1'
        assert controller.flash.message != null
        assert Rsvp.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/rsvp/list'

        populateValidParams(params)
        def rsvp = new Rsvp(params)

        assert rsvp.save() != null

        params.id = rsvp.id

        def model = controller.show()

        assert model.rsvpInstance == rsvp
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/rsvp/list'

        populateValidParams(params)
        def rsvp = new Rsvp(params)

        assert rsvp.save() != null

        params.id = rsvp.id

        def model = controller.edit()

        assert model.rsvpInstance == rsvp
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/rsvp/list'

        response.reset()

        populateValidParams(params)
        def rsvp = new Rsvp(params)

        assert rsvp.save() != null

        // test invalid parameters in update
        params.id = rsvp.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/rsvp/edit"
        assert model.rsvpInstance != null

        rsvp.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/rsvp/show/$rsvp.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        rsvp.clearErrors()

        populateValidParams(params)
        params.id = rsvp.id
        params.version = -1
        controller.update()

        assert view == "/rsvp/edit"
        assert model.rsvpInstance != null
        assert model.rsvpInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/rsvp/list'

        response.reset()

        populateValidParams(params)
        def rsvp = new Rsvp(params)

        assert rsvp.save() != null
        assert Rsvp.count() == 1

        params.id = rsvp.id

        controller.delete()

        assert Rsvp.count() == 0
        assert Rsvp.get(rsvp.id) == null
        assert response.redirectedUrl == '/rsvp/list'
    }
}
