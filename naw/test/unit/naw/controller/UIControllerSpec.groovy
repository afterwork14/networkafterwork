package naw.controller

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
class UIControllerSpec extends Specification {

    def "save facebook user"(){
		
		given: "valid facebook user"
		def params = [:];
		
		when: "call service to get app access token"
		
		then: "we should see a valid response"
	}
}
